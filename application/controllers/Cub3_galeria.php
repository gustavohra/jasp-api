<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 10/08/2016 às 13:48:30
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_galeria extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{

	}

		/*
	* [Método]: inserirGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function inserirGaleria()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model"); 
		$dados 					= json_decode(file_get_contents('php://input'),true);
		$dados["galImagens"]	= json_encode($dados["galImagens"]);
 
		if($dados != null) {
			$dados["usuCodigo"] = $this->session->userdata('s_usuCodigo');

			if($this->padrao_model->inserir('cub3_galeria', $dados) > 0)
				retornarJson(null, array('resposta' => true, 'galCodigo' => $this->db->insert_id() ));
			else
				retornarJson(false);
		}
		else {
			retornarJson(false);
		}

	}
	
		/*
	* [Método]: editarGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function editarGaleria()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model"); 
		$dados 					= json_decode(file_get_contents('php://input'),true);
		$dados["galImagens"]	= json_encode($dados["galImagens"]);
 
		if($dados != null) {
			$dados["usuCodigo"] = $this->session->userdata('s_usuCodigo');
			$condicao 			= array('galCodigo' => $dados["galCodigo"]);

			if($this->padrao_model->alterar('cub3_galeria', $dados, $condicao) > 0)
				retornarJson(null, array('resposta' => true));
			else
				retornarJson(false);
		}
		else {
			retornarJson(false);
		}
	}
	
		/*
	* [Método]: excluirGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function excluirGaleria()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model"); 
		$galCodigo 				= urldecode(decifracub3($this->input->get("galCodigo")));

		if($galCodigo != '') {
			$dados["galStatus"] = "EXCLUIDO";
			$condicao 			= array('galCodigo' => $galCodigo);

			if($this->padrao_model->alterar('cub3_galeria', $dados, $condicao) > 0)
				retornarJson(null, array('resposta' => true));
			else
				retornarJson(false);
		}
		else {
			retornarJson(false);
		}
	}
	
		/*
	* [Método]: destacarGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function destacarGaleria()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model"); 
		$galCodigo 					= urldecode(decifracub3($this->input->get("galCodigo")));
		$galDestaque 				= urldecode(decifracub3($this->input->get("galDestaque")));

		if($galCodigo != '') {
			$dados["galDestaque"] 	= $galDestaque;
			$condicao 				= array('galCodigo' => $galCodigo);

			if($this->padrao_model->alterar('cub3_galeria', $dados, $condicao) > 0)
				retornarJson(null, array('resposta' => true));
			else
				retornarJson(false);
		}
		else {
			retornarJson(false);
		}
	}
	/*
	* [Método]: getGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function getGaleria()
	{ 
		$this->load->model("cub3_galeria_model"); 

		$galCodigo 				= urldecode(decifracub3($this->input->get("galCodigo")));

		if($galCodigo != ""){
			$dadosGaleria		= $this->cub3_galeria_model->galeriaPorCodigo($galCodigo)->row_array();
			retornarJson(null, $dadosGaleria);
		}
		else
			retornarJson(false);
	}


	/*
	* [Método]: getGalerias
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_galeria.php
	* @param 
	* @return 
	*/
	public function getGaleriasJson()
	{
		verificarStatusSessao();
		$this->load->model("cub3_galeria_model"); 
		$dadosGalerias 			= $this->cub3_galeria_model->galeriasListar();

		$retorno		 		= $dadosGalerias->result_array();
 
		if(is_null($retorno))
           retornarJson(false);
       else
			retornarJson(null, $retorno);

	}



}
