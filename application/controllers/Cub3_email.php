<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 13/05/2016 às 19:14:09
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_email extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarAdministrador(); 
		$config = Array( 
		    'smtp_host' => $this->config->item("smtp_host", "cub3_config_email"),
		    'smtp_port' => $this->config->item("smtp_port", "cub3_config_email"),
		    'smtp_user' => $this->config->item("smtp_user", "cub3_config_email"),
		    'smtp_pass' => $this->config->item("smtp_pass", "cub3_config_email"), 
		    'charset'   => $this->config->item("charset", "cub3_config_email")
		);

		$dados["configEmail"]   = cifracub3(json_encode($config)); 
		$dados["titulo"] 		= "E-mails";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-envelope-o"></i> Gerenciamento / '.$dados["titulo"], 'cub3_email');


		$dados["view"]	= "cub3_email/emailsListar";
		carregarTema($dados);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function emailsCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastro de modelo";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-envelope-o"></i> Gerenciamento / '.$dados["titulo"], 'cub3_email');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> E-mail / '.$dados["titulo"], 'cub3_email/emailsCadastrar');

		$dados["view"]	= "cub3_email/emailModelo";
		$dados["formularioSlug"]	= "email-sistema";
		carregarTema($dados);

	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function emailsVisualizar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Edição de modelo";
		$emtCodigo 			= urldecode(decifracub3($this->input->get("emtCodigo")));
		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-envelope-o"></i> Gerenciamento / '.$dados["titulo"], 'cub3_email');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> E-mail / '.$dados["titulo"], 'cub3_email/emailsCadastrar');

		$dados["view"]	= "cub3_email/emailModelo";
		$dados["formularioSlug"]	= "email-sistema";
		$dados["emtCodigo"] 		= $emtCodigo;
		carregarTema($dados);

	} 
	/*
	* [Método]: getEmailsJson
	* [Descrição]:
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package
	* @param 
	* @return 
	*/
	public function getEmailsJson()
	{
		verificarStatusSessao();
		$this->load->model("cub3_email_model");
		$emtCodigo 			= urldecode(decifracub3($this->input->get("emtCodigo")));

		if($emtCodigo == '')
			retornarJson(null, $this->cub3_email_model->emailsListar()->result_array());
		else 
			retornarJson(null, $this->cub3_email_model->emailListarPorCodigo($emtCodigo)->row_array());
	}
	/*
	* [Método]: emailInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Propostas.php
	* @param 
	* @return 
	*/
	public function emailInserir()
	{
		verificarStatusSessao();
		verificarAdministrador();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados 										= array();
		$dados 										= json_decode(file_get_contents('php://input'),true);
		 
 	 
		$dados["emtHorario"] 					= date("Y-m-d H:i:s"); 
		$dados["usuCodigo"]						= $this->session->userdata('s_usuCodigo');

		if(!isset($dados["emtCodigo"])){
			if($this->padrao_model->inserir('cub3_email', $dados) > 0){
				$resposta = array('mensagem' => 'Modelo de e-mail inserido com sucesso!', 'url' => 'cub3_email' );
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}
		else{
			$condicao = array('emtCodigo' => $dados["emtCodigo"] );

			if($this->padrao_model->alterar('cub3_email', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Modelo de e-mail editado com sucesso!', 'url' => 'cub3_email' );
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}

	} 
		/*
	* [Método]: excluirEmail
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_email.php
	* @param 
	* @return 
	*/
	public function excluirEmail()
	{
		$this->load->model("padrao_model");
		verificarAdministrador();
			$emtCodigo 				= urldecode(decifracub3($this->input->get("emtCodigo")));
			$dados["emtCodigo"]		= $emtCodigo;
			$dados["emtStatus"] 	= "EXCLUIDO";

			$condicao 				= array('emtCodigo' => $emtCodigo);

			if($this->padrao_model->alterar('cub3_email', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Modelo de e-mail excluído com sucesso!');
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
	}


	/*
	* [Método]: setCub3Config
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	public function setCub3Config()
	{
			verificarStatusSessao();
			verificarAdministrador();
			$arquivo 								= "<?php defined('BASEPATH') OR exit('No direct script access allowed'); \n \n"; 
			$campo 									= urldecode(decifracub3($this->input->get("campo")));
			$valor 									= urldecode(decifracub3($this->input->get("valor")));

			$dados = Array( 
			    'smtp_host' => $this->config->item("smtp_host", "cub3_config_email"),
			    'smtp_port' => $this->config->item("smtp_port", "cub3_config_email"),
			    'smtp_user' => $this->config->item("smtp_user", "cub3_config_email"),
			    'smtp_pass' => $this->config->item("smtp_pass", "cub3_config_email"), 
			    'charset'   => $this->config->item("charset", "cub3_config_email")
			); 
			$dados[$campo] 					= $campo == "smtp_pass" ? cifracub3($valor) : $valor;
			$configuracoes 					= getConfigProjeto();
			$configuracoes["config_email"] = $dados;
			if(setConfigProjeto($configuracoes))
				retornarJson(true);
			else
				retornarJson(false);
	} 
		/*
	* [Método]: testarEnvioEmail
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_email.php
	* @param 
	* @return 
	*/
	public function testarEnvioEmail()
	{
		verificarStatusSessao();
		$destinatario 				= urldecode(decifracub3($this->input->get("destinatario")));
		$modelo 					= urldecode(decifracub3($this->input->get("emtSlug")));

		if($destinatario != "" && $modelo != ""){
			$dadosEmail 				= array();
			$dadosEmail["destinatario"] = $destinatario;
			$dadosEmail["assunto"]		= "[CUB3] Boas vindas!";
			$dadosEmail["mensagem"] 	= "<center><b>Mensagem</b> de teste, favor não responder.</center>";

			if(enviarEmail($dadosEmail, $modelo))
				retornarJson(true, array('mensagem' => "Teste realizado com sucesso!"));
			else
				retornarJson(false, array('mensagem' => "Ocorreu um erro! Por favor, tente novamente."));
		}
		else {
				retornarJson(false, array('mensagem' => "Ocorreu um erro! Por favor, confira os valores e tente novamente."));
		}
	}



}
