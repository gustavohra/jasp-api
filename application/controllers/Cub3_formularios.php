<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Base de gerenciamento dos formulários
	* [Criação]: 29/04/2016 às 19:11:22
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_formularios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("cub3_formularios_model");
		$this->load->model("padrao_model");
	}

	
	public function formularioMontarCampos($array, $campos = null, $forCodigo = null)
	{
		return formularioMontarCampos($array, $campos, $forCodigo);
	}
	public function getFormularioDados() {
		$id 			= urldecode(decifracub3($this->input->get("id")));
		$verificar 		= $this->cub3_formularios_model->formularioVerificar($id)->row_array(); 

		if($verificar == null)
			retornarJson(false);
		else
			retornarJson(true, $verificar);
	} 
	public function criarCache()
	{
		verificarAdministrador(); 
		$forCodigo 		= urldecode(decifracub3($this->input->get("forCodigo")));
		return criarCache($forCodigo);
	} 
	public function criarCacheGeral()
	{
		verificarAdministrador(); 
		return criarCacheGeral();
	} 
	public function exportarFormulario()
	{
		verificarAdministrador();   
		$dados 				= (file_get_contents('php://input'));

		return exportarFormulario($dados);
	}
	public function getTabelasNomes()
	{
		verificarStatusSessao();
		return getTabelasNomes();
	}
 	public function inserirDadosEmFormulario()
 	{
		verificarAdministrador(); 
 		$forCodigo 	= urldecode(decifracub3($this->input->get("forCodigo")));
		$dados 		= json_decode(file_get_contents('php://input'),true);

 		return inserirDadosEmFormulario($dados, $forCodigo);
 	}
	/*
	* [Método]: index
	* [Descrição]: Listagem de formulários no escopo administrativo 
	*/
	public function index()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Formulários";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-check-square-o"></i> Gerenciamento / '.$dados["titulo"], 'formularios');

		$dados["view"]	= "cub3_formularios/formulariosListar";
		carregarTema($dados);
	}
	/*
	* [Método]: visualizar
	* [Descrição]: Exibe uma visualização genérica do formulário a partir do segment
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	public function visualizar()
	{
		verificarStatusSessao();
		$dados["forSlug"]			= $this->uri->segment(3);
		$codigo 		 			= urldecode(decifracub3($this->input->get("codigo")));

		$formulario 				= $this->cub3_formularios_model->formularioListarPorSlug($dados["forSlug"])->row_array();
		if($formulario != null){
			$dados["titulo"] 			= $formulario["forTitulo"];
			$dados["view"]				= "cub3_formularios/formularioPadrao";
			// Breadcrumbs
			$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-tasks"></i>'.$dados["titulo"], 'cub3_formularios/visualizar/'.$dados["forSlug"]);

			if($codigo == ''){
				$dados["forTitulo"]			= "<i class='fa fa-plus'></i> ".$dados["titulo"];
			}
			else { 
				if($formulario["forBancoDeDados"] != null && $formulario["forBancoDeDadosKey"] != '' && $formulario["forBancoDeDadosKey"] != 'undefined'){
					$model 			= $this->padrao_model->buscar("SELECT * FROM ". $formulario["forBancoDeDados"] . " WHERE ". $formulario["forBancoDeDadosKey"] . " = ".$codigo)->row_array();
					if($model != null)
						$dados["modelDados"] = cifracub3(json_encode($model));
				}
				else{
					$dados["mensagem"]	= "Formulário com configurações inválidas. Erro: F0DB";
					$dados["view"] 		= "cub3_erros/erro404";
				}
			}
		}
		else
			$dados["view"] 				= "cub3_erros/erro404";

		carregarTema($dados);	
	}


	/*
	* [Método]: formularioEditar
	* [Descrição]: Exibe o formulário de edição  
	*/
	public function formularioEditar()
	{
		verificarStatusSessao();

		$dados["titulo"] 		= "Editar formulário";
		$forCodigo 				= decifracub3($this->input->get("forCodigo"));

		$formulario 			= $this->cub3_formularios_model->formularioListarPorCodigo($forCodigo)->row_array();

		if($formulario != null):
			// Breadcrumbs
			$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Formulários / Formulários', 'cub3_formularios');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Formulário / '.$dados["titulo"], 'formularioEditar');

			$dados["paginaSubmit"]	= "formularioEditar(formularioDados)";
			$dados["forCodigo"]		= cifracub3($formulario["forCodigo"]);
			$dados["controller"]	= "FormularioEdicaoCtrl"; 
			$dados["view"]			= "cub3_formularios/formulario";

				carregarTema($dados);
		else:
           redirect('erro404');			
       	endif;
	}

	/*
	* [Método]: getFormularioJson
	* [Descrição]:
	*/
	public function getFormulariosJson()
	{
		verificarStatusSessao();
		$forCodigo 			= urldecode(decifracub3($this->input->get("forCodigo")));

		if($forCodigo == '')
			retornarJson(null, $this->cub3_formularios_model->formulariosListar()->result_array());
		else 
			retornarJson(null, $this->cub3_formularios_model->formularioListarPorCodigo($forCodigo)->row_array());
	}

	/*
	* [Método]: getFormularioCamposJson
	* [Descrição]:
	*/
	public function getFormularioCamposJson()
	{ 
		$forCodigo 			= urldecode(decifracub3($this->input->get("forCodigo")));
		$cache 				= "./application/cache/formularios/formulario-".$forCodigo.".json";
		switch (file_exists($cache)) {
			case true:
				retornarJson(null, json_decode(file_get_contents($cache), true));
				break;
			default:
				retornarJson(null, getFormularioCamposJson($forCodigo));
				break;
		}
	}
	/*
	* [Método]: getOptionsUrlPorKey
	* [Descrição]:  
	*/
	public function getOptionsUrlPorKey()
	{
		header('Content-type: application/json');
 
		$key 			= urldecode(decifracub3($this->input->get("key")));  
		$dados      = $this->cub3_formularios_model->formularioCampoOptionsUrl($key)->row_array(); 
		retornarJson(null, $dados);
	}


	/*
	* [Método]: getFormularioCamposBrutoJson
	* [Descrição]: Retorna os campos sem a formatação do Formly
	*/
	public function getFormularioCamposBrutoJson()
	{
		verificarStatusSessao();
		$forCodigo 			= urldecode(decifracub3($this->input->get("forCodigo")));
		
		if($forCodigo == '')
			retornarJson(false);
		else {
			$campos  	=  $this->cub3_formularios_model->formularioListarCampos($forCodigo)->result_array();
			$arrayPai 	= array();
			$retorno 	= array();

			for ($i=0; $i < count($campos); $i++) { 
				$aux 		= $campos[$i];

				switch ($aux["fctAgrupamento"]) {
					case 1:
							// Monta o array pai que irá agrupar os campos
							$arrayPai["className"] 				= $aux["className"];
							$arrayPai["fieldGroup"]				= array();
							break;
					case 0: 
								$auxArray 					= $this->formularioMontarCampos($aux, array_keys($aux), $forCodigo);

								if(!isset($arrayPai["className"])){
									$arrayPai["className"]			= "row";
									$arrayPai["fieldGroup"]			= array();
								}  
								array_push($arrayPai["fieldGroup"], $auxArray);
						break;
					default:
						
						break;
				}
			}

			array_push($retorno, $arrayPai);

			retornarJson(null,$retorno);
		}
	} 
	/*
	* [Método]: formularioInserir
	* [Descrição]: Recebe um post via JSON e insere no banco
	*/
	public function formularioInserir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		$dados["usuCodigo"] = $this->session->userdata('s_usuCodigo');
  	
  		if(!isset($dados["forCodigo"])){
			if($this->padrao_model->inserir('cub3_formulario', $dados) > 0)
				retornarJson(true, array( 'url' => 'cub3_formularios/formularioEditar/?forCodigo='.cifracub3($this->db->insert_id())));
			else
				retornarJson(false);
  		}
  		else {
  			$condicao = array('forCodigo' => $dados["forCodigo"]);
			if($this->padrao_model->alterar('cub3_formulario', $dados, $condicao) > 0)
				retornarJson(null, array('resposta' => true, 'mensagem' => "Formulário alterado com sucesso!" ));
			else
				retornarJson(false);  			
  		}
	}
 
	/*
	* [Método]: formularioExcluir
	* [Descrição]:  Recebe o código e realiza a exclusão do conteúdo
	*/
	public function formularioExcluir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$forCodigo 		= urldecode(decifracub3($this->input->get("forCodigo")));
  
		$dados 			= array();
		$dados 			= json_decode(file_get_contents('php://input'),true);
		$auxContagem 	= 0;

		if($dados != null && Count($dados) > 0){
			for ($i=0; $i < Count($dados); $i++) { 
				$aux 		= array("forCodigo" => $dados[$i]["forCodigo"]);
				if($this->padrao_model->excluir('cub3_formulario_campo', $aux)){
					if($this->padrao_model->excluir('cub3_formulario', $aux))
						$auxContagem++;
				}
			}		
			if($auxContagem > 0)
				retornarJson(true);
			else
				retornarJson(false);
		}
		else{
	  		// Excluir campos do formulário
			$condicao 		= array("forCodigo" => $forCodigo);

			if($this->padrao_model->excluir('cub3_formulario_campo', $condicao)){
				if($this->padrao_model->excluir('cub3_formulario', $condicao))
					retornarJson(true);
				else	
					retornarJson(false);	
			}
			else
				retornarJson(false);
		}	
	}

	/*
	* [Método]: formularioCampoDuplicar
	* [Descrição]: Recebe o código via GET e realiza a duplicação no banco de dados
	*/
	public function formularioCampoDuplicar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');


		$fctCodigo 		= urldecode(decifracub3($this->input->get("fctCodigo")));
		$dados 			= $this->cub3_formularios_model->formularioListarCampoPorCodigo($fctCodigo)->row_array();
		unset($dados["fctCodigo"]);
 
  
		if($this->padrao_model->inserir('cub3_formulario_campo', $dados) > 0)
			retornarJson(true);
		else
			retornarJson(false);
	}
 
	/*
	* [Método]: formularioCampoExportar
	* [Descrição]: Recebe o código via GET e realiza a duplicação no banco de dados
	*/
	public function formularioCampoExportar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$fctCodigo = array();
		$fctCodigo = json_decode(file_get_contents('php://input'),true);
		$dados     = $this->cub3_formularios_model->formularioListarCampoPorCodigo($fctCodigo)->row_array();


		$forCodigo 		= urldecode(decifracub3($this->input->get("forCodigo"))); 
 
		unset($dados["fctCodigo"]);
		$dados["forCodigo"] = $forCodigo;
 
  
		if($this->padrao_model->inserir('cub3_formulario_campo', $dados) > 0)
			retornarJson(true);
		else
			retornarJson(false);
	}
	/*
	* [Método]: formularioDuplicar
	* [Descrição]: Recebe o código via GET e realiza a duplicação no banco de dados
	*/
	public function formularioDuplicar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');
		$this->load->helper('date');


		$forCodigo 			= urldecode(decifracub3($this->input->get("forCodigo")));
		$dadosFormulario    = $this->cub3_formularios_model->formularioListarPorCodigoUnico($forCodigo)->row_array();
		$campos 			= $this->cub3_formularios_model->formularioListarCampos($forCodigo)->result_array();
 		
 		unset($dadosFormulario["forCodigo"]);
 		$dadosFormulario["forTitulo"] = "Cópia de " . $dadosFormulario["forTitulo"];
 		$dadosFormulario["forHorario"] = date("Y-m-d H:i:s");
 		$dadosFormulario["forSlug"]   = "copia-de-" . $dadosFormulario["forSlug"] . now(); 
  	
		if($this->padrao_model->inserir('cub3_formulario', $dadosFormulario) > 0){
			 $forCodigoNovo = $this->db->insert_id();

	 		for ($i=0; $i < Count($campos); $i++) { 
	 			unset($campos[$i]["fctCodigo"]);
	 			$campos[$i]["forCodigo"] = $forCodigoNovo;


				$this->padrao_model->inserir('cub3_formulario_campo', $campos[$i]);
	 		}

				retornarJson(true); 
		}
		else
			retornarJson(false);
	}
	/*
	* [Método]: formularioCampoExcluir
	* [Descrição]:  Recebe o código e realiza a exclusão do conteúdo
	*/
	public function formularioCampoExcluir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$fctCodigo 		= urldecode(decifracub3($this->input->get("fctCodigo")));
		$forCodigo 		= urldecode(decifracub3($this->input->get("forCodigo")));

		$dados["fctStatus"] = 'EXCLUÍDO';

		$condicao = array("fctCodigo" => $fctCodigo);

		if($this->padrao_model->alterar('cub3_formulario_campo', $dados, $condicao)){
			criarCache($forCodigo, false);
			retornarJson(true);
		}
		else	
			retornarJson(false);		
	}

	/*
	* [Método]: formularioCampoAlterar
	* [Descrição]: 
	*/
	public function formularioCampoAlterar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);
		unset($dados["msg"]);
		if(isset($dados["fctCodigo"])){
			$fctCodigo 		= $dados["fctCodigo"];

			$condicao = array("fctCodigo" => $fctCodigo);
 

			if($this->padrao_model->alterar('cub3_formulario_campo', $dados, $condicao)){
				criarCache($dados["forCodigo"], false);
				retornarJson(true);
			}
			else	
				retornarJson(false);		
		}
		else{
			if($this->padrao_model->inserir('cub3_formulario_campo', $dados)){
				criarCache($dados["forCodigo"], false);
				retornarJson(true);
			}
			else	
				retornarJson(false);		

		}
	}

	/*
	* [Método]: formularioCampoAlterar
	* [Descrição]: 
	*/
	public function setFormulario()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$forCodigo 									= urldecode(decifracub3($this->input->get("forCodigo"))); 

		$dados = array();
		$campo 										= urldecode(decifracub3($this->input->get("campo")));
		$valor 			 							= urldecode($this->input->get("valor"));

		if($campo != "undefined" && $valor != "undefined"){
		$dados[$campo] 								= $valor; 
		$condicao = array("forCodigo" => $forCodigo);

			if($this->padrao_model->alterar('cub3_formulario', $dados, $condicao)){
				criarCache($forCodigo, false);
				retornarJson(true, array('url' => "cub3_formularios" ));
			}
			else	
				retornarJson(false);		
		}
		else
				retornarJson(false);		
	}

		/*
	* [Método]: formularioCamposInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	public function formularioImportar()
	{
			verificarStatusSessao();
			$this->load->model("padrao_model");
			$this->load->helper("date");

			$nomeTabela 							= urldecode(decifracub3($this->input->get("nomeTabela")));
			$titulo 								= urldecode(($this->input->get("titulo")));
			$slug 									= urldecode(decifracub3($this->input->get("slug")));

			$dados 									= json_decode(file_get_contents('php://input'));
			$camposTabela 							= $dados == null ? getTabelaCampos($nomeTabela) : getTabelaCampos($nomeTabela, $dados->dados);

			$dadosFormulario 						= array();
			$dadosFormulario["forTitulo"]   		= $titulo == '' ? "Formulário da tabela ".$nomeTabela : $titulo;
			$dadosFormulario["forSlug"]				= $slug == '' ? $nomeTabela . now() : validarSlug($slug);
			$dadosFormulario["forHorario"]  		= date("Y-m-d H:i:s");
			$dadosFormulario["usuCodigo"]   		= $this->session->userdata('s_usuCodigo');
	 		$dadosFormulario["forBancoDeDados"]		= $nomeTabela;
	 		$dadosFormulario["forAutomatico"]		= $dados != null ? 1 : 0;
	 		$dadosFormulario["forUrlRedirect"]		= isset($dados->forUrlRedirect) ? $dados->forUrlRedirect : "";
			$dadosFormulario["forBancoDeDadosKey"] 	= isset($dados->primaryKey) ? $dados->primaryKey : '';

			if($this->padrao_model->inserir('cub3_formulario', $dadosFormulario))
			{
				$forCodigo = $this->db->insert_id(); 
			 		// $primeiraLinha 						= array();
			 		// $primeiraLinha["template"] 		= 		"<h4 class=\"titulo\"><i class=\"fa fa-users\"></i> Título do formulário</i></h4>".
					// 								  			"<blockquote><small>Breve descrição com informações sobre o formulário, orientações, etc..</small></blockquote>";
					// $primeiraLinha["forCodigo"]			= $forCodigo;
					// $primeiraLinha["fctOrdem"] 			= 0;
					// $primeiraLinha["className"] 		= "section-label";
					// $this->padrao_model->inserir('cub3_formulario_campo', $primeiraLinha);

				foreach ($camposTabela as $campo) {
					$campo["forCodigo"]  = $forCodigo;
					$this->padrao_model->inserir('cub3_formulario_campo', $campo);
				}
				retornarJson(true, array('mensagem' => 'Formulário importado com sucesso!', 'url' => 'cub3_formularios/formularioEditar/?forCodigo='.cifracub3($forCodigo), 'slug' => $dadosFormulario["forSlug"], 'forTitulo'=> $dadosFormulario["forTitulo"], 'forCodigo' => $forCodigo ));
			}
			else
				retornarJson(false, array('mensagem' => 'Ocorreu um erro! Por favor, tente novamente.'));

	}

		/*
	* [Método]: getColunasDb
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	public function validarFormulario()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);
		$formCampos = $dados["campos"];
		if($formCampos != null && isset($formCampos)):
			$colunas 	= $this->db->list_fields($dados["forBancoDeDados"]);
			$formCamposValidos = array();
 
			for ($i=0; $i < Count($formCampos) ; $i++) { 
				$aux = $formCampos[$i]["fieldGroup"];

				for ($j=0; $j < Count($aux); $j++) { 
				$auxCampo = $aux[$j]["key"];

					if(is_string($auxCampo))
						array_push($formCamposValidos, $auxCampo);
				} 
			}
			$camposValidados = array();
	 		$camposValidados = $this->compararCampos($formCamposValidos, $colunas);

	 		retornarJson(null, $camposValidados);
 		else:
 			retornarJson(false);
 		endif;
		
	}
	/*
	* [Método]: compararColunas
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	private function compararCampos($forCampos, $dbCampos)
	{
		$camposValidados = array();
 
		for ($i=0; $i < Count($forCampos); $i++) { 
			$j 			= 0;
			$verificar 	= false;
			$auxForm 	= $forCampos[$i];

			while ($j < Count($dbCampos)) {
				$auxDb = $dbCampos[$j]; 
				if($auxForm == $auxDb){
					$camposValidados[$i]["nome"] 		= $auxForm;
					$camposValidados[$i]["resultado"] 	= "<i class='fa fa-check' style='color:green'></i> Conferido e validado";
					$verificar = true;
					$j++;
					break;
				}
				$j++;
			}

			if($verificar == false) {
					$camposValidados[$i]["nome"] 		= $auxForm;
					$camposValidados[$i]["resultado"] 	= "<i class='fa fa-close' style='color:red'></i> Campo não confere. Favor verificar!";
			}
		}

		return $camposValidados;
	}

	/*
	* [Método]: formularioCamposOrdenar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_formularios.php
	* @param 
	* @return 
	*/
	public function formularioCamposOrdenar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);
		

		for ($i=0; $i < count($dados); $i++) { 
			$auxDados 						= array();
			$aux 							= $dados[$i];
			$auxDados["fctOrdem"] 			= $i;

			$condicao = array("fctCodigo" => $aux["fctCodigo"]);
			if($this->padrao_model->alterar('cub3_formulario_campo', $auxDados, $condicao))
				criarCache($aux["forCodigo"], false);
		}
		retornarJson(true);
	}
 
	public function getIcones()
	{
		$this->load->model("cub3_menu_model");
		$dados = $this->cub3_menu_model->icones()->result_array();
		$retorno = array();
		$aux     = 0;
		foreach ($dados as $key => $value) {
			$retorno[$aux]["name"]	= "<span><i class='fa fa ".$value["icone"]."'></i> ".$value["icone"]."</span>";
			$retorno[$aux]["value"]	= "fa " .$value["icone"];
			$retorno[$aux]["group"] = "Ícones";
			$aux++;
		}
		retornarJson(null, $retorno);
	}



}
