<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 13/05/2016 às 19:14:09
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_configuracoes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarAdministrador();  
		$dados["titulo"] 			= "Configuração do Sistema";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-wrench"></i> Gerenciamento / '.$dados["titulo"], 'cub3_email');

		if($this->input->get("opcao") != '')
			$dados["aba"]	= $this->input->get("opcao");

		$dados["view"]	= "cub3_configuracoes/formulario";
		carregarTema($dados);
	}
 		/*
	* [Método]: montarAngularJs
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function montarAngularJs()
	{
		$origem = $this->uri->segment(3);
 
		echo "var app = angular.module('". $this->config->item("nome_app")."', [".carregarDependenciasAngular($origem)."]);".
        	"app.constant('BASE_URL', '". base_url()."');". 
        	"app.constant('FACEBOOK_URL', '". $this->config->item("Facebook", "redes_sociais"). "');". 
        	"app.constant('YOUTUBE_URL', '". $this->config->item("Youtube", "redes_sociais"). "');". 
        	"app.constant('TWITTER_URL', '". $this->config->item("Twitter", "redes_sociais"). "');". 
        	"app.constant('INSTAGRAM_URL', '". $this->config->item("Instagram", "redes_sociais")."');";
	}


		/*
	* [Método]: getConfigProjeto
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function getConfigProjeto()
	{
		verificarAdministrador();  
		retornarJson(null, getConfigProjeto());
	}
		/*
	* [Método]: getPluginsAngular
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function getPluginsAngular()
	{
		verificarAdministrador();  
		$config 	= getConfigProjeto();
		$plugins 	= $config["plugins"];
		retornarJson(null, $plugins);
	}
		/*
	* [Método]: getEmailConfig
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function getEmailConfig()
	{
		verificarAdministrador();  
		$config 		= getConfigProjeto();
		$configEmail 	= $config["config_email"];
		retornarJson(null, $configEmail);
	}
		/*
	* [Método]: verificarPlugin
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function verificarPlugin()
	{
		verificarAdministrador();  
		$nomePlugin = urldecode(decifracub3($this->input->get("nome")));
		$config 	= getConfigProjeto();
		$plugins 	= $config["dep_angular"];
		$retorno 	= false;

		foreach ($plugins as $plugin) {
			if($plugin == $nomePlugin){
				$retorno = true;
				break;
			}
		}
		retornarJson(null,array('resposta' => $retorno));
	}


		/*
	* [Método]: setConfigProjeto
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function setConfigProjeto($campo = null, $valor = null, $usarHeader = true)
	{
			verificarAdministrador();  
			$campo 									= $campo == null ? urldecode(decifracub3($this->input->get("campo"))) : $campo;
			$valor 									= $valor == null ? urldecode(($this->input->get("valor"))) : $valor; 
			$configuracoes 							= getConfigProjeto();

			$configuracoes[$campo] 					= $valor; 
 
			if(setConfigProjeto($configuracoes))
				retornarJson(true, null, $usarHeader);
			else
				retornarJson(false, null, $usarHeader); 
	} 
		/*
	* [Método]: setCor
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function setCor($campo = null, $valor = null)
	{
			verificarAdministrador();  
			$campo 												= $campo == null ? urldecode(decifracub3($this->input->get("campo"))) : $campo;
			$valor 												= $valor == null ? urldecode(decifracub3($this->input->get("valor"))) : $valor; 
			$configuracoes 										= getConfigProjeto();
			$resposta 											= false;
			$arrayAux 											= array();

			for ($i=0; $i < Count($configuracoes["cores"]) ; $i++) { 
				$aux 			= $configuracoes["cores"][$i];
				$nome 			= explode("-", $campo);
				if($aux["id"] 	== $nome[1]){
					$configuracoes["cores"][$i]["valor"] = $valor;
					$resposta 							 = true;
					break;
				}
			} 
			if(!$resposta) {
				$arrayAux[$campo] 						= $valor;
				array_push($configuracoes["cores"], $arrayAux);
			}

			if(setConfigProjeto($configuracoes))
				retornarJson(true);
			else
				retornarJson(false); 
	}
		/*
	* [Método]: alterarRotas
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function alterarRotas()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true);
			$this->setConfigProjeto("rotas", $post);
	}

		/*
	* [Método]: alterarLib
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function alterarLib()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true);

			$this->setConfigProjeto("libraries", $post);
	}

		/*
	* [Método]: alterarHelpers
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function alterarHelper()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true);
 
			$this->setConfigProjeto("helper", $post);
	}


		/*
	* [Método]: alterarPlugins
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function alterarPlugins()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true);
 
			$this->setConfigProjeto("dep_angular", $post);
	}
		/*
	* [Método]: importarConfig
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function importarConfig()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true); 
			if(setConfigProjeto($post))
				retornarJson(true);
			else
				retornarJson(false); 
	}
		/*
	* [Método]: importarConfig
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function importarSql()
	{
			verificarAdministrador();  
			$post		= json_decode(file_get_contents('php://input'), true); 
			if ( ! $this->db->simple_query($post))
			{
			        $error = $this->db->error();
			        retornarJson(false, array('mensagem' => "Ocorreu um erro: ".$error));
			}
			else
			        retornarJson(false, array('mensagem' => "Importação realizada com sucesso!"));
	}

		/*
	* [Método]: exportarConfig
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function exportarConfig()
	{
			verificarAdministrador();  
			$config 		= "./projeto.json";

			if (file_exists($config)) {
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename="'.$this->config->item('portal_nome').'.config"');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize($config));
			    readfile($config);
		    exit;
			}
	}
		/*
	* [Método]: atualizarSistema
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function atualizarSistema()
	{
			verificarAdministrador();   

			$post		= json_decode(file_get_contents('php://input'), true);   
			if($this->baixarAtualizacao($post)){
				try {
					exec('unzip -o '.getcwd().'/atualizacao'.$post["verCodigo"].'.zip'); 
				} catch (Exception $e) {
					retornarJson(null, array('resposta' => false, 'mensagem' => "Ocorreu um erro: ".$e), false);
				} finally {
					$this->setConfigProjeto('versao', $post["verCodigo"], false);
				}
			} 
	}

	private function baixarAtualizacao($post) {
	    $rh = fopen("http://cub3.com.br/others/uploads/versoes/".$post["verUrl"], 'rb');
	    $wh = fopen('./atualizacao'.$post["verCodigo"].'.zip', 'w+b');
	    
	    if (!$rh || !$wh) {
	        return false;
	    }

	    while (!feof($rh)) {
	        if (fwrite($wh, fread($rh, 4096)) === FALSE) {
	            return false;
	        } 
	        flush();
	    }

	    fclose($rh);
	    fclose($wh);

	    return true;
	}
	/*
	* [Método]: gerarChaveCriptografada
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function gerarChaveCriptografada($len = 32)
	{ 
			$chars = array(
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
				'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
				'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
			);
			shuffle($chars);
			$num_chars = count($chars) - 1;
			$token = '';
			// Create random token at the specified length.
			for ($i = 0; $i < $len; $i++)
			{
				$token .= $chars[mt_rand(0, $num_chars)];
			}
			retornarJson(null, array('token' => cifracub3($token)));
	}

	/*
	* [Método]: getTabelasDb
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getTabelasDb()
	{
		verificarAdministrador();
		$tabelas 		= $this->db->list_tables();
		retornarJson(null, $tabelas);
	}
	/*
	* [Método]: getColunasDb
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getColunasDb()
	{
		verificarStatusSessao();
		$nome 			= urldecode(decifracub3($this->input->get("nome")));

		retornarJson(null, $this->db->field_data($nome));
	}
	/*
	* [Método]: getTabelaDados
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getTabelaDados()
	{
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$nome 			= urldecode(decifracub3($this->input->get("nome")));

		retornarJson(null, $this->padrao_model->buscar("SELECT * FROM `".$nome."`")->result_array());
	}
	/*
	* [Método]: linhaExcluir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function linhaExcluir()
	{
		verificarAdministrador();
		$this->load->model("padrao_model");
		$tabela 			= urldecode(decifracub3($this->input->get("tabela")));
		$key 				= urldecode(decifracub3($this->input->get("key")));
		$dados 				= json_decode(file_get_contents('php://input'), true)[0]; 

		$condicao 			= array($key => $dados[$key]);
		retornarJson(null, $this->padrao_model->excluir($tabela, $condicao));
	}
	/*
	* [Método]: ferramentas
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function ferramentas()
	{
		verificarAdministrador();  
		$dados["titulo"] 			= "Ferramentas";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-wrench"></i> Gerenciamento / '.$dados["titulo"], 'cub3_configuracoes/ferramentas');
 
		$dados["view"]	= "cub3_configuracoes/ferramentas";
		carregarTema($dados);		
	}
	/*
	* [Método]: executarQuery
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function executarQuery()
	{
		verificarAdministrador();  
		$post		= json_decode(file_get_contents('php://input'), true); 
		if ( ! $this->db->simple_query($post["comando"]))
		{
		        $error = $this->db->error();
		        retornarJson(false, array('mensagem' => "Ocorreu um erro: ".implode(", ", $error)));
		}
		else
		        retornarJson(false, array('mensagem' => "Comando realizado com sucesso!"));
	}


	/*
	* [Método]: assistente
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_configuracoes.php
	* @param 
	* @return 
	*/
	public function assistente()
	{
		verificarAdministrador();

		$dados["view"]	= "cub3_configuracoes/assistente";
		carregarTema($dados);	

	}

	/*
	* [Método]: inserirVersao 
	* [Descrição]: Insere ou edita um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador e $tabela
	* [Comentários]: Caso o código identificador seja passado, ele realiza a alteração e caso contrário, a inserção.
	*/
	public function inserirVersao ($campoIdentificador = 'verOrdem', $tabela = 'cub3_versao')
	{
		$this->load->model('padrao_model');
		$dados 		= json_decode(file_get_contents('php://input'), true); 
		$urlRetorno = "cub3_listagem/visualizar/versoes-do-sistema";

		$dados 		= tratarAnexo($dados, "verUrl");
		if(!isset($dados[$campoIdentificador])){
			$mensagemRetorno 		= 'Versão inserida com sucesso!';
			if($this->padrao_model->inserir($tabela, $dados) > 0){
				$this->postarMensagemSlack($dados);
				retornarJson(null, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => $mensagemRetorno));
			}
			else
				retornarJson(false);	
		}	
		else {		
			$mensagemRetorno 		= 'Versão alterada com sucesso!';
			$condicao 				= array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(false);
		}	
	}
	private function postarMensagemSlack($dados){  
 
				$this->load->helper('cub3_slack_helper');
				// Envia msg de commit via Slack
				$token 				= "xoxp-6097219367-6100081090-13934983844-0992f23074";
				$canal 				= "C2CSFPQ0K";
				$nomeUsuario 		= $this->session->userdata('s_usuNome');
				$texto 				= "Nova versão do sistema";
				$urlImagem 			= "http://cub3.com.br/others/uploads/avatar/".$this->session->userdata('s_usuImagem');
				$log 				= array(
										array(
						                    "title" => "Log",
						                    "value" => isset($dados["verLog"]) ? $dados["verLog"] : "Nenhum log de alteração informado.",
						                    "short" => true
											)
										);

				$anexo = array(
					"fallback" 		=> "Novo release",
					"color"			=> "#4fc500",
					"pretext"		=> "Versão: ".$dados["verCodigo"],
					"author_name"	=> $this->session->userdata('s_usuNome'),
					"author_link"	=> "http://cub3.com.br",
					"author_icon"	=> $urlImagem,
					"title"			=> "Nova versão do sistema adicionada à CUB3",
					"title_link"	=> "http://cub3.com.br/",
					"text"			=> 'A versão '.$dados["verCodigo"]." foi adicionada. Por favor, atualizem os sistemas que utilizem a plataforma.",
			        "fields" 		=> $log,	
					"icon_url"		=> "http://cub3.com.br/assets/img/favicon.png"
					);
				$anexo = array(
					"attachments" => $anexo
					);
				postarMensagemSlack($token, $canal, $texto, $nomeUsuario, $anexo, $urlImagem);		
	}

}