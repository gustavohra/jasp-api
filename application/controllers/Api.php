<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 03/10/2016 às 21:25:08
	*
	* @author gustavoaguiar
	* @package None
	*/
class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
 

	// 	/*
	// * [Método]: getAreasAtuacao
	// * [Descrição]: 
	// * [Comentários]: 
	// * 
	// * @author gustavoaguiar
	// * @package /Volumes/Desenvolvimento/htdocs/jasp/application/controllers/Api.php
	// * @param 
	// * @return 
	// */
	// public function getAreasAtuacao()
	// {
	// 	$this->load->helper("jasp_areas_atuacao_helper");
	// 	getAreasAtuacao();
	// }
	// /*
	// * [Método]: getProfissionais
	// * [Descrição]: 
	// * [Comentários]: 
	// * 
	// * @author gustavo.aguiar
	// * @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Api.php
	// * @param 
	// * @return 
	// */
	// public function getProfissionaisPesquisa()
	// {
	// 	$this->load->helper("jasp_profissionais_helper");
	// 	getProfissionais();
	// }
	// /*
	// * [Método]: getPacotes
	// * [Descrição]: 
	// * [Comentários]: 
	// * 
	// * @author gustavo.aguiar
	// * @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Api.php
	// * @param 
	// * @return 
	// */
	// public function getPacotes()
	// {
	// 	$this->load->helper("jasp_pacotes_helper");
	// 	getPacotes();
	// }

	// /*
	// * [Método]: getServicos
	// * [Descrição]: 
	// * [Comentários]: 
	// * 
	// * @author gustavo.aguiar
	// * @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Api.php
	// * @param 
	// * @return 
	// */
	// public function getServicos()
	// {
	// 	$this->load->helper("jasp_servicos_helper");
	// 	getServicos();
	// }

	/*
	* [Método]: realizarLogin
	* [Descrição]:  Recebe um post de solicitação pelo App. Caso o login solicitado seja pelo Facebook, realiza a validação e se necessário, o cadastro.
	*/
	public function realizarLogin()
	{ 
		$this->load->helper("jasp_login_helper");

		$dados = null;
		$dados = json_decode(file_get_contents('php://input'),true);

		if($dados != null){
			if(isset($dados["pacIdFacebook"]) && !is_null($dados["pacIdFacebook"]))
				pacienteRealizarLoginFacebook($dados);
			else {
				if(isset($dados["login"]) && !filter_var($dados["login"], FILTER_VALIDATE_EMAIL))
					profissionalRealizarLogin($dados);
				else
					pacienteRealizarLogin($dados);				
			}
		}
		else
			retornarJson(null, array("resposta" => false, "mensagem" => "Informações inválidas, por favor, tente novamente."));
	}
	/*
	* [Método]: realizarCadastro
	* [Descrição]:  Recebe um post de solicitação pelo App. 
	*/
	public function realizarCadastro()
	{ 
		$this->load->helper("jasp_login_helper");

		$dados = null;
		$dados = json_decode(file_get_contents('php://input'),true);

		if($dados != null){ 
				pacienteRealizarCadastro($dados); 
		}
		else
			retornarJson(null, array("resposta" => false, "mensagem" => "Informações inválidas, por favor, tente novamente."));
	}
 
 	/*
	* [Método]: requisicao
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar 
	* @param 
	* @return 
	*/
	public function requisicao()
	{
		// Recebe a postagem
		$dados 		= $this->input->post("dados") != null ? $this->input->post("dados") : $this->input->get("dados");

		if($dados == null)
			$dados 		= (file_get_contents('php://input'));

		$authKey 	= $this->input->post("authKey") != null ? $this->input->post("authKey") : $this->input->get("authKey");
		$helper 	= $this->input->post("helper") != null ? $this->input->post("helper") : $this->input->get("helper");
		$metodo 	= $this->input->post("metodo") != null ? $this->input->post("metodo") : $this->input->get("metodo");

		// Verifica se o token de acesso é válido e se a postagem é válida

		if(!is_null($helper) && file_exists('application/helpers/'.$helper.".php")){
			// Carrega o helper solicitado
			$this->load->helper($helper);

			// Carrega a função solicitada, caso a função não exista, ou seja inválida, retorna um erro
			if(is_callable($metodo)){
				// Caso um array de valores ou objeto seja enviado na postagem, executa a função passando o método como parâmetro. Caso contrário, executa sem parâmetros
				if(!is_null($dados))
					call_user_func_array($metodo, array($dados));
				else
					call_user_func($metodo);
			}
			else
				retornarJson(false, array('mensagem' => 'Método inválido'));
		}
		else
			retornarJson(false, array('mensagem' => "Helper inválido")); 
	}


}
