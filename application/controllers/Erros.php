<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 03/05/2016 às 14:14:54
	*
	* @author gustavo.aguiar
	* @package None
	*/
class Erros extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{

	}
	/*
	* [Método]: erro403
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package None
	* @param 
	* @return 
	*/
	public function erro403()
	{
		$data["view"]		= "cub3_erros/403";
		$this->load->view("template/sistema/principal",$data);
	}
	/*
	* [Método]: erro404
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package None
	* @param 
	* @return 
	*/
	public function erro404()
	{
		$data["view"]		= "cub3_erros/404";
		$this->load->view("template/sistema/principal",$data);
	}

	/*
	* [Método]: personalizado
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package None
	* @param 
	* @return 
	*/
	public function personalizado()
	{
		$data["view"]		= "cub3_erros/personalizado";
		$data["erro"]		= $this->session->flashdata('erro');
		$this->load->view("template/sistema/principal",$data);
	}



}
