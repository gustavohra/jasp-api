<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_geral extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function uploadFlow()
	{
		ini_set('display_errors',0);
		$this->load->helper('manipulacaoArquivos');
		$this->load->helper('string');

		if(!is_null($this->input->post('dir')) && !empty($this->input->post('dir')))
			$dir = $this->input->post('dir');
		else  
			$dir = $this->input->get("dir");

		if(is_null($dir) || empty($dir))
			$dir = 'conteudo/';

		switch ($dir) {
			case 'profissionais':
				$dir .= '/fotos/';
				break;
			default:
				$dir = 'conteudo/';
				break;
		}
		
		$tempDir = $dir == null ? 'others/temp/'.$this->session->userdata("session_id") : "others/uploads/".$dir;

		if (!file_exists($tempDir)) {
			mkdir($tempDir, 0755, true);
		}
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {
			$chunkDir = $tempDir . DIRECTORY_SEPARATOR . $_GET['flowIdentifier'];
			$chunkFile = $chunkDir.'/chunk.part'.$_GET['flowChunkNumber'];
			if (file_exists($chunkFile)) {
				header("HTTP/1.0 200 Ok");
			} else {
				header("HTTP/1.1 204 No Content");
			}
		}


		//Cria o arquivo no diretório
		$retorno = array(
			'success' => true,
		    'files' => $_FILES,
		    'get' => $_GET,
		    'post' => $_POST,
		    //optional
		    'flowTotalSize' => isset($_FILES['file']) ? $_FILES['file']['size'] : $_GET['flowTotalSize'],
		    'flowIdentifier' => isset($_FILES['file']) ? $_FILES['file']['name'] . '-' . $_FILES['file']['size'] : $_GET['flowIdentifier'],
		    'flowFilename' => isset($_FILES['file']) ? $_FILES['file']['name'] : $_GET['flowFilename'],
		    'flowExtension'	=> pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION),
		    'flowRelativePath' => isset($_FILES['file']) ? $_FILES['file']['tmp_name'] : $_GET['flowRelativePath']
		);

		$arquivo 		= file_get_contents($retorno["flowRelativePath"]);

		$fileName = $retorno["flowFilename"];

		$nomeArquivo 	= random_string(16) . "." . $retorno["flowExtension"];
		$fp = fopen($tempDir."/".$nomeArquivo, "w+");
 		fwrite($fp, $arquivo);

 		$retorno["arquivo"] = $nomeArquivo;

		// Just imitate that the file was uploaded and stored.
		sleep(2);

		die(json_encode($retorno));
	}

	public function cidadesPorEstado(){
		$post 			= json_decode(file_get_contents('php://input'));
		$this->load->model("Geral_model");
		$query = $this->Geral_model->cidadesListar($post->id);
		die(json_encode($query->result()));
	}

}

/* End of file Geral.php */
/* Location: ./application/controllers/Geral.php */