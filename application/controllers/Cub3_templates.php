<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 17/05/2016 às 21:58:10
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_templates extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();  
		$dados["titulo"] 		= "Templates";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-paint-brush"></i> Gerenciamento / '.$dados["titulo"], 'cub3_templates');


		$dados["view"]	= "cub3_templates/templatesListar";
		carregarTema($dados);

	}
	/*
	* [Método]: getTemplatesJson
	* [Descrição]:
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package
	* @param 
	* @return 
	*/
	public function getTemplatesJson()
	{
		verificarStatusSessao();
		$this->load->model("cub3_template_model");
		$tplCodigo 			= urldecode(decifracub3($this->input->get("tplCodigo")));

		if($tplCodigo == '')
			retornarJson(null, $this->cub3_template_model->templatesListar()->result_array());
		else 
			retornarJson(null, $this->cub3_template_model->templateListarPorCodigo($tplCodigo)->row_array());
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function templatesCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastro de modelo";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-envelope-o"></i> Gerenciamento / '.$dados["titulo"], 'cub3_templates');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> E-mail / '.$dados["titulo"], 'cub3_templates/templatesCadastrar');

		$dados["view"]	= "cub3_templates/template";
		$dados["formularioSlug"]	= "templates-sistema";
		carregarTema($dados);

	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function templatesVisualizar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Edição de template";
		$tplCodigo 			= urldecode(decifracub3($this->input->get("tplCodigo")));
		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-envelope-o"></i> Gerenciamento / '.$dados["titulo"], 'cub3_templates');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> E-mail / '.$dados["titulo"], 'cub3_templates/templatesCadastrar');

		$dados["view"]	= "cub3_templates/template";
		$dados["formularioSlug"]	= "templates-sistema";
		$dados["tplCodigo"] 		= $tplCodigo;
		carregarTema($dados);

	} 
	/*
	* [Método]: templateInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Propostas.php
	* @param 
	* @return 
	*/
	public function templateInserir()
	{
		verificarStatusSessao();
		verificarAdministrador();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados 										= array();
		$dados 										= json_decode(file_get_contents('php://input'),true);
		 
 	 
		$dados["tplHorario"] 					= date("Y-m-d H:i:s"); 
		$dados["usuCodigo"]						= $this->session->userdata('s_usuCodigo');

		if(!isset($dados["tplCodigo"])){
			if($this->padrao_model->inserir('cub3_template', $dados) > 0){
				$resposta = array('mensagem' => 'Template inserido com sucesso!', 'url' => 'cub3_templates' );

				$this->gravarTemplate($dados);
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}
		else{
			$condicao = array('tplCodigo' => $dados["tplCodigo"] );

			if($this->padrao_model->alterar('cub3_template', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Modelo de template editado com sucesso!', 'url' => 'cub3_templates' );

				$this->gravarTemplate($dados);
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}

	}
		/*
	* [Método]: excluirTemplate
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_email.php
	* @param 
	* @return 
	*/
	public function excluirTemplate()
	{
		$this->load->model("padrao_model");
		verificarAdministrador();
			$emtCodigo 				= urldecode(decifracub3($this->input->get("tplCodigo")));
			$dados["tplCodigo"]		= $emtCodigo;
			$dados["tplStatus"] 	= "EXCLUIDO";

			$condicao 				= array('tplCodigo' => $emtCodigo);

			if($this->padrao_model->alterar('cub3_template', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Modelo de template excluído com sucesso!');
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
	}

		/*
	* [Método]: gravarTemplate
	* [Descrição]: Recebe os valores do template e realiza a gravação no servidor
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Cub3_templates.php
	* @param array $dados
	* @return bool
	*/

	private function gravarTemplate($dados) {
			$diretorio	= dirname($dados["tplCaminho"]);
			
			if(!is_dir($diretorio))
				mkdir($diretorio, 0755, true);

			$tpl 		= fopen($dados["tplCaminho"], 'a+');

			try{
				fwrite($tpl, $dados["tplConteudo"]);
				fclose($tpl);
				return true;
			} catch ( Exception $e ) {
		      	return false;
		    } 
	}
}
