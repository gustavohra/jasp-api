<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Classe destinada a rotinas de backup da apliação.
	* [Criação]: 22/04/2016 às 18:10:54
	*
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	*/
class Cub3_backups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: index
	* [Descrição]: Listagem de backups do banco de dados.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$data["view"]	= "cub3_backups/backupsListar";
		carregarTema($data);
	}
	/*
	* [Método]: getBackups
	* [Descrição]: Busca todos os backups da base de dados armazenados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	public function getBackups()
	{
		$this->load->model("cub3_backups_model");
		$dados = $this->cub3_backups_model->backups();
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}


	/*
	* [Método]: gerarBackup
	* [Descrição]: Executa todas as rotinas de backup da aplicação.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	public function gerarBackup()
	{
		$this->gerarBackupBanco();
		$this->gerarLogErros();
	}
	/*
	* [Método]: gerarBackupBanco
	* [Descrição]: Realizar o backup de todas as tabelas do banco de dados.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	private function gerarBackupBanco()
	{
		$this->load->dbutil();
		$backup = $this->dbutil->backup(); 
		$this->load->helper('file');
		$arquivo  = $_SERVER['HTTP_HOST']."-".date("Y-m-d-H-i-s").".gz";

		if(write_file('./others/bkp/'.$arquivo, $backup)){
			$this->load->model("padrao_model");
			$dados = array(
				"bkpNome"						=> $arquivo,
				"bkpHorario"					=> date("Y-m-d H:i:s"),
				"bkpStatusSincronizacao"		=> "PENDENTE",
				"bkpStatusArquivo"				=> "ATIVO"
			);

			$bkpCodigo = $this->padrao_model->inserir("cub3_backup",$dados);

			$url = base_url('others/bkp/'.$arquivo);
			$this->solicitarSincronizacaoBanco($bkpCodigo,$url);
		}; 
	}
	/*
	* [Método]: solicitarSincronizacao
	* [Descrição]: Realiza a inclusão de uma solicitação de sincronização do backup realizado
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	private function solicitarSincronizacaoBanco($bkpCodigo,$bkpUrl)
	{
		$fields_string = "";
		$url = 'http://cub3.com.br/rotinas/backupRegistrarSolicitacao';
		$fields = array(
			'bkpCodigo'		=> urlencode($bkpCodigo),
			'bkpUrl' 		=> urlencode($bkpUrl)
		);
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);
	}
	/*
	* [Método]: gerarLogErros
	* [Descrição]: Realiza a leitura do arquivo de log de erros
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	private function gerarLogErros()
	{
		$arquivo = file("./error_log");
		$retorno = array();
		for($i = 0; $i < count($arquivo); $i++) {
			if (!in_array(substr($arquivo[$i],23), $retorno)) { 
			    array_push($retorno,substr($arquivo[$i],23));
			}
		}

		$this->solicitarSincronizacaoLog($this->config->item("projeto_repositorio"), $retorno);
	}
	/*
	* [Método]: solicitarSincronizacaoLog
	* [Descrição]: Sincroniza os dados com o servidor da CUB3
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/controllers/Backup.php
	* @param 
	* @return 
	*/
	private function solicitarSincronizacaoLog($repositorio, $retorno)
	{
		$fields_string = "";
		$url = 'http://cub3.com.br/rotinas/logRegistrarSolicitacao';

		foreach ($retorno as $key => $value) {
			$fields = array(
				'repositorio'		=> urlencode($repositorio),
				'log' 				=> urlencode($value)
			);
			foreach($fields as $key2=>$value2) { $fields_string .= $key2.'='.$value2.'&'; }
			rtrim($fields_string, '&');
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			$result = curl_exec($ch);
			curl_close($ch);
		}
	}

		/*
	* [Método]: baixarBackupBanco
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_backups.php
	* @param 
	* @return 
	*/
	public function baixarBackupBanco()
	{
		verificarAdministrador();
		$this->load->dbutil();
		$backup = $this->dbutil->backup(); 
		$this->load->helper('download');
		force_download('banco-de-dados-'.$this->config->item("portal_nome").'.gz', $backup);
	}
		/*
	* [Método]: baixarBackupSistema
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_backups.php
	* @param 
	* @return 
	*/
	public function baixarBackupSistema()
	{
		verificarAdministrador();
	    $this->load->library('zip'); 
	    $path = './'; 
	    $time = time();
	    $this->zip->read_dir($path);  
	    $result = $this->zip->download('backup.'.$time.'.zip');  
		force_download('banco-de-dados-'.$this->config->item("portal_nome").'zip', $result);
	}








}