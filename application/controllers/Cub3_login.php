<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 
class Cub3_login extends CI_Controller {

	public function __construct(){
        parent::__construct();
		$this->load->library('session'); 
        $this->load->helper("auth");

	}
	public function index()
	{ 
		if($this->session->userdata('s_usuCodigo') != null)
			redirect("painel");
		
		$dados["view"]				= 	"template/login/index";
		$dados["exibirSidebar"]		= false;
		$dados["exibirHeader"]		= false;
		$this->load->view($dados["view"], $dados);
	}
	public function loginValidarAcesso(){
		$post		= json_decode(file_get_contents('php://input'));

		$this->load->model("cub3_usuario_model"); 
		$usuEmail 		= $post->usuEmail;
		$usuSenha		= $post->usuSenha;
		$dadosUsuario			= $this->cub3_usuario_model->usuarioPorEmail($usuEmail)->row(); 
		$retorno 		= array();
 
				$usuSenhaMatch = ($usuSenha == $this->loginDecifrar($dadosUsuario->usuSenha))?true:false;

				if($usuSenhaMatch && $dadosUsuario->usuStatus == "ATIVO"):
					$retorno["TIPO"] 		= 1;
					$retorno["urlRetorno"]	= "painel";
					 		
					//Monta o menu a ser acessado de acordo com opções de acesso setadas pelo administrador
					$menusPorLogin = $this->loginCriarMenu($dadosUsuario, true);
					$dadosUsuario->tipo 	= 'Cliente';
					//Cria a sessão do usuário
					$retorno['TOKEN'] 		= $this->loginCriarSessao($dadosUsuario,$menusPorLogin, true);
					$retorno['USUARIO'] 	= $dadosUsuario;
					$retorno['MENU'] 		= $menusPorLogin;
					$retorno['CLIENTE'] 	= false;

				elseif($dadosUsuario->usuStatus == "INATIVO"):
					$retorno["TIPO"] 	= 2;
				else:
					$retorno["TIPO"] 	= 3;
				endif;    
		//Insere log de tentativa de acesso
		$this->logLogin($usuEmail, $retorno["TIPO"]);
		//Retorno com status para a tela de login
		retornarJson(null, $retorno);
	}
	/*
	* [Método]: logLogin
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Login.php
	* @param 
	* @return 
	*/
	private function logLogin($usuEmail,$retorno){
		$this->load->model("padrao_model");

		switch ($retorno) {
			case '1':
				$retorno = "LIBERADO";
				break;
			case '2':
				$retorno = "BLOQUEADO";
				break;
			case '3':
				$retorno = "NEGADO";
				break;
		}
		$dados = array(
			"usuEmail"	=> $usuEmail,
			"usuStatus"	=> $retorno,
			"horario"	=> date("Y-m-d H:i:s"),
			"ip"		=> $this->input->ip_address(),
			"agente"	=> $this->input->user_agent()
		);

		$this->padrao_model->inserir("cub3_loginlog",$dados);

	}
	private function loginDecifrar($usuSenha)
	{
		$this->load->library('encryption');	
		return $this->encryption->decrypt($usuSenha);	
	}
	private function loginCriarMenu($usuario, $cliente = null){
		$this->load->model("cub3_menu_model");
		$this->load->model("cub3_grupo_model");
 
			$gruCodigo	= $this->cub3_grupo_model->grupoPorUsuario($usuario->usuCodigo);  

		if($gruCodigo != null && $gruCodigo->num_rows() >  0):
			$gruCodigo 	= $gruCodigo->row()->gruCodigo;
			
			$dados 	= ($gruCodigo != 1)?$this->cub3_menu_model->menuListarPorGrupo($gruCodigo):$this->cub3_menu_model->menuListarTodos();
			$menus = $dados->result_array();

			for ($i=0; $i < Count($menus); $i++) { 
				$menus[$i]["menuItens"] = $this->cub3_menu_model->menuSubmenuListarTodos($menus[$i]["menCodigo"])->result_array();
			}

			$menu 	= ($dados->num_rows() == 0)?json_encode(null):json_encode($menus);
			return $menu;
		else:
			return false;
		endif;
	}
	private function loginCriarSessao($usuario,$menusPorLogin, $cliente = null){
 
		$sessao = array(
					"session_id"		=> date('YmdHis'),
					"s_usuCodigo"		=> $usuario->usuCodigo,
					"s_usuEmail"		=> $usuario->usuEmail,
					"s_usuNome"			=> $usuario->usuNome,
					"s_usuSobrenome"	=> $usuario->usuSobrenome,
					"s_usuImagem"		=> $usuario->usuImagem,
					"s_menu"			=> $menusPorLogin 
				);
 
		$this->session->set_userdata($sessao);
        
        $sess = token_get($sessao);

		return $sess;
	}
	public function loginDestruir(){
		$this->session->sess_destroy();
		redirect(base_url("login"));
	}
	public function cadastrarSenha(){
		$urtToken 	= $this->uri->segment(3);
		//$urtToken 	= decifracub3($urtToken);
		//$urtToken 	= explode("_", $urtToken);
		// $usuCodigo 	= $urtToken[0];
		// $urtToken 	= $urtToken[1];

		$this->load->model("cub3_perfil_model");
		
		$dadosToken 	= $this->cub3_perfil_model->perfilSelecionarToken($urtToken);
		

		if($dadosToken->num_rows() > 0){
			$dadosUsuario 		= $this->cub3_perfil_model->perfilSelecionarCodigo($dadosToken->row()->usuCodigo);
			$data["usuNome"] 	= $dadosUsuario->row()->usuNome;
			$data["usuCodigo"] 	= $dadosUsuario->row()->usuCodigo;
			$data["view"] 		= "perfil/cadastrarSenha";
		}else{
			$data["view"] = "perfil/tokenInvalido";

		}

		$this->load->view("template/externo/externo",$data);

	}
	public function atualizarSenha(){
		$this->load->library('encrypt');

		$usuSenha = $this->input->post("usuSenha");
		$usuSenha = $this->encrypt->sha1($usuSenha);
		$usuCodigo = $this->input->post("usuCodigo");

		$dados = array("usuSenha"=>$usuSenha);
		$this->load->model("padrao_model");
		$codicao = array("usuCodigo" => $usuCodigo);
		$this->padrao_model->alterar("cub3_usuario",$dados,$codicao);


		$this->session->set_flashdata('msgStatus', mensagemStatus("sucesso","Sua nova senha foi devidamente configurada e o acesso já pode ser realizado."));
		redirect(base_url());
	}
	public function recuperarAcesso(){
		$data["view"] = "perfil/perfilRecuperarSenha";
		$this->load->view("template/externo/externo",$data);
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */