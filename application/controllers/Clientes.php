<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 28/12/2016 às 14:06:55
	*
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Clientes.php
	*/
class Clientes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Clientes.php
	* @param 
	* @return 
	*/
	public function clienteDescontoEditar()
	{
		$this->load->model("financeiro_model");
		$dados["titulo"] 					= "Edição de Desconto";
		$codigo 							= decifracub3($this->input->get("desCodigo"));
		if($codigo != '')
			$dados["dados"] 				= cifracub3(json_encode($this->financeiro_model->descontoPorCodigo($codigo)->row_array())); 

		// Breadcrumbs/
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-heartbeat"></i> Descontos', 'profissionais/');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-cube"></i>Cadastro', 'profissionais/profissionalEditar');

		$dados["controller"]		= "DescontosEditarCtrl";
		$dados["view"]				= "cub3_formularios/formularioPadrao";
		$dados["forSlug"]			= "clientes-desconto-formulario";
		carregarTema($dados);
	}
	/*
	* [Método]: clienteDescontoAlterar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Clientes.php
	* @param 
	* @return 
	*/
	public function clienteDescontoAlterar($campoIdentificador = "desCodigo", $tabela = "fin_desconto")
	{
		$this->load->model("padrao_model");
		$post 	= json_decode(file_get_contents('php://input'), true);
		$dados 	= $post;

		if(!isset($dados[$campoIdentificador])){
			if($this->padrao_model->inserir($tabela, $dados) > 0){
				retornarJson(null, array("resposta" => true, "url" => "cub3_listagem/visualizar/clientes-descontos", "mensagem" => "Desconto cadastrado com sucesso."));
			}
			else
				retornarJson(false);			
		}	
		else {			
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array("resposta" => true, "url" => "cub3_listagem/visualizar/clientes-descontos", "mensagem" => "Desconto modificado com sucesso.")); 
			else
				retornarJson(false);
		}
	}


}
