<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_unidade extends CI_Controller {

	/*
	* [Método]: index
	* [Descrição]: Abre a listagem de unidades
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$data["view"]		= "cub3_unidade/unidadeListar";
		carregarTema($data);
	}
	/*
	* [Método]: getUnidades
	* [Descrição]: Busca todas as unidades cadastradas
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function getUnidades()
	{
		$this->load->model("cub3_unidade_model");
		$dados = $this->cub3_unidade_model->unidadeListarTodos();
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}
	/*
	* [Método]: getUnidadePorCodigo
	* [Descrição]: Busca uma unidade por seu código.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function getUnidadePorCodigo()
	{
		$this->load->model("cub3_unidade_model");

		$post		= json_decode(file_get_contents('php://input'));

		$dados = $this->cub3_unidade_model->unidadeSelecionarCodigo(decifracub3($post->uniCodigo));
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}
	/*
	* [Método]: unidadeCadastrar
	* [Descrição]: Abre o formulário para cadastro de uma nova unidade
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function unidadeCadastrar(){
		verificarStatusSessao();
		$this->load->model("cub3_unidade_model");

		$data["view"]		= "cub3_unidade/unidadeCadastrar";
		carregarTema($data);
	}
	/*
	* [Método]: unidadeInserir
	* [Descrição]: Realiza a inserção da unidade no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function unidadeInserir(){
		verificarStatusSessao();
		$post		= json_decode(file_get_contents('php://input'));

		$dados = array(
			"uniDescricao"	=> $post->uniDescricao,
			"uniStatus"		=> $post->uniStatus
		);

		$this->load->model("padrao_model");
		die(json_encode($this->padrao_model->inserir("cub3_unidade",$dados)));
	}
	/*
	* [Método]: unidadeEditar
	* [Descrição]: Abre o formulário para edição de uma unidade
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function unidadeEditar(){
		verificarStatusSessao();
		$data["view"]			= "cub3_unidade/unidadeEditar";
		carregarTema($data);
	}
	/*
	* [Método]: unidadeAlterar
	* [Descrição]: Realiza a alteração da unidade no banco de dados.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function unidadeAlterar(){
		verificarStatusSessao();
		$post		= json_decode(file_get_contents('php://input'));

		$dados = array(
			"uniCodigo"		=> $post->uniCodigo,
			"uniDescricao"	=> $post->uniDescricao,
			"uniStatus"		=> $post->uniStatus
		);

		$this->load->model("padrao_model");
		$condicao = array("uniCodigo" => $post->uniCodigo);
		die(json_encode($this->padrao_model->alterar("cub3_unidade",$dados,$condicao)));
	}
	/*
	* [Método]: unidadeExcluir
	* [Descrição]: Realiza a exclusão da unidade no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Unidade.php
	* @param 
	* @return 
	*/
	public function unidadeExcluir(){
		verificarStatusSessao();
		$uniCodigo = $this->uri->segment(3);
		$uniCodigo = decifracub3($uniCodigo);
		
		$this->load->model("padrao_model");
		$this->load->model("grupo_model");

		//Exclui menus e usuários associados
		$dados = $this->grupo_model->grupoPorPorUnidade($uniCodigo);
		foreach ($dados->result() as $dado) {
			$condicao = array("gruCodigo" => $dado->gruCodigo);
			$this->padrao_model->excluir("cub3_grupo_has_menu",$condicao);
			$this->padrao_model->excluir("cub3_grupo_has_usuario",$condicao);
		}
		//Exclui grupos e unidades.
		$condicao = array("uniCodigo" => $uniCodigo);
		$this->padrao_model->excluir("cub3_grupo",$condicao);
		$this->padrao_model->excluir("cub3_unidade",$condicao);

		redirect(base_url("unidade"));
	}

}

/* End of file unidade.php */
/* Location: ./application/controllers/unidade.php */