<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 29/07/2016 às 21:01:10
	*
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/site/Inicio.php
	*/
class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/site/Inicio.php
	* @param 
	* @return 
	*/
	public function index()
	{
		// redirect("painel"); 
		$this->load->view('site/home');
	}
}
