<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 16/09/2016 às 19:29:02
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_videos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("cub3_videos_model");
	}

	/*
	* [Método]: getVideosJson
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	public function getVideosJson()
	{
		$dados 		= $this->cub3_videos_model->listarVideos()->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: excluirVideo 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	public function excluirVideo ($campoIdentificador = 'vidCodigo', $campoStatus = 'vidStatus', $tabela = 'cub3_video')
	{
		$this->load->model('padrao_model');
		$dados 								= array(); 
		$dados[$campoStatus]	 			= 'EXCLUIDO';
		$dados[$campoIdentificador]			= urldecode(decifracub3($this->input->get("codigo")));
		$mensagemRetorno 					= 'Vídeo excluído com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção do vídeo.';
  
		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}


}
