<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 14/05/2016 às 10:43:13
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_listagem extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
 		$this->load->model('cub3_listagem_model');
		$this->load->model("padrao_model");
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarAdministrador();  
		$dados["titulo"] 		= "Listagem de dados";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-bars"></i> '.$dados["titulo"], 'cub3_listagem');


		$dados["view"]	= "cub3_listagem/listagem";
		carregarTema($dados);
	}
	/*
	* [Método]: visualizar
	* [Descrição]: Gera uma listagem genérica a partir do slug fornecido via uri segment ou input
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function visualizar()
	{
		verificarStatusSessao();
		$dados["titulo"] 			= "Listagem";
		$slug 						= $this->uri->segment(3) == null ? $this->input->get("listagem") : $this->uri->segment(3);

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-bars"></i> '.$dados["titulo"], 'cub3_listagem/visualizar/'.$slug); 

		$dados["view"]				= "cub3_listagem/listagemPadrao";
		$dados["lisCodigo"] 		= $slug;
		carregarTema($dados);

	} 
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function listagemVisualizar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Listagem";
		$lisCodigo 				= urldecode(decifracub3($this->input->get("lisCodigo")));
		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-bars"></i> '.$dados["titulo"], 'cub3_listagem');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> '.$dados["titulo"], 'cub3_listagem/listagemVisualizar');

		$dados["view"]				= "cub3_listagem/listagemVisualizar";
		$dados["formularioSlug"]	= "listagem-sistema";
		$dados["lisCodigo"] 		= $lisCodigo;
		carregarTema($dados);
	} 
	/*
	* [Método]: listagemCadastrar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function listagemCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastro de listagem";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-bars"></i> Listagem', 'cub3_listagem');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> '.$dados["titulo"], 'cub3_listagem/cadastroVisualizar');

		$dados["view"]	= "cub3_listagem/listagemVisualizar";
		$dados["formularioSlug"]	= "listagem-sistema";
		carregarTema($dados);

	} 

	/*
	* [Método]: getListagensJson
	* [Descrição]:
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package
	* @param 
	* @return 
	*/
	public function getListagensJson()
	{  
		$lisCodigo 			= urldecode(decifracub3($this->input->get("lisCodigo")));

		if($lisCodigo == '')
			retornarJson(null, $this->cub3_listagem_model->listagens()->result_array());
		else 
			retornarJson(null, $this->cub3_listagem_model->listagemPorCodigoOuSlug($lisCodigo)->row_array());
	} 
		/*
	* [Método]: getDados
	* [Descrição]: Método genérico para buscar dados de uma listagem
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getDados()
	{ 

		$lisCodigo 			= urldecode(decifracub3($this->input->get("lisCodigo")));
		$dados 				= json_decode(file_get_contents('php://input'),true); 
		$dados 				= (($dados["dados"]));
 		$retorno 			= array();

		if(!isset($dados["lisBancoDeDadosKeyStatus"])){ 
			$retorno 			= $this->padrao_model->buscar("SELECT * FROM `".$dados["lisBancoDeDados"]."`")->result_array();
		}
		else{
			if(($dados["lisBancoDeDadosKeyStatus"] != '' && $dados["lisBancoDeDadosKeyStatus"] != null))
				$retorno 			= $this->padrao_model->buscar("SELECT * FROM `".$dados["lisBancoDeDados"]."` WHERE ".$dados["lisBancoDeDadosKeyStatus"]." = 'ATIVO'")->result_array();
			else
				$retorno 			= $this->padrao_model->buscar("SELECT * FROM `".$dados["lisBancoDeDados"]."`")->result_array();
		}

		retornarJson(null, $retorno);
	}


	/*
	* [Método]: listagemInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package  
	* @param 
	* @return 
	*/
	public function listagemInserir()
	{
		verificarStatusSessao();
		verificarAdministrador();
		header('Content-type: application/json');

		$dados 										= array();
		$dados 										= json_decode(file_get_contents('php://input'),true);

		if(isset($dados["lisPlugin"])){
			if(isset($dados["lisPlugin"]["value"]))
	 	 		$dados["lisPlugin"]							= $dados["lisPlugin"]["value"];
	 	 	else
	 	 		$dados["lisPlugin"]							= $dados["lisPlugin"];	
		}
		if(isset($dados["lisIcone"])) {
			if(isset($dados["lisIcone"]["value"]))
 	 			$dados["lisIcone"]							= $dados["lisIcone"]["value"];
	 	 	else
	 	 		$dados["lisIcone"]							= $dados["lisIcone"];
		}
		
 	 	if(isset($dados["lisAcoes"]))
 	 		$dados["lisAcoes"] 						= json_encode($dados["lisAcoes"]);

 	 	if(isset($dados["lisTitulos"]))
 	 		$dados["lisTitulos"] 					= json_encode($dados["lisTitulos"]);
 	 		
 		if(isset($dados["lisHeader"])){
 				$dados["lisHeader"] 					= json_encode($dados["lisHeader"]);
	 		if($dados["lisHeader"] != "")
	 			unset($dados["lisHeader"]);
 		}
 		
 	 	if(isset($dados["lisBotoes"])){
 	 		$dados["lisHeader"]["lisBotoes"] 		= json_encode($dados["lisBotoes"]); 
 	 		$dados["lisHeader"] 					= json_encode($dados["lisHeader"]);
 	 		unset($dados["lisBotoes"]);
 	 	}
		$dados["lisHorario"] 					= date("Y-m-d H:i:s"); 
		$dados["usuCodigo"]						= $this->session->userdata('s_usuCodigo');

		if(!isset($dados["lisCodigo"])){
			if($this->padrao_model->inserir('cub3_listagem', $dados) > 0){
				$resposta = array('mensagem' => 'Modelo de lista inserido com sucesso!', 'url' => 'cub3_listagem' );
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}
		else{
			$condicao = array('lisCodigo' => $dados["lisCodigo"] );

			if($this->padrao_model->alterar('cub3_listagem', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Modelo de lista editado com sucesso!', 'url' => 'cub3_listagem' );
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}

	} 

		/*
	* [Método]: excluirListagem 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	public function excluirListagem ($campoIdentificador = 'lisCodigo', $campoStatus = 'lisStatus', $tabela = 'cub3_listagem')
	{
		$this->load->model('padrao_model');
		$dados 								= array();  
		$dados[$campoIdentificador]			= urldecode(decifracub3($this->input->get($campoIdentificador)));
		$mensagemRetorno 					= 'Linha excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da linha.';
  
		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->excluir($tabela, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}

	/*
	* [Método]: getTabelasDb
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getTabelasDb()
	{
		verificarStatusSessao();
		$select 		= $this->input->get("select");
		$tabelas 		= $this->db->list_tables();
		$retorno 		= array();
		if($select != '' && $select){
			for ($i=0; $i < Count($tabelas); $i++) { 
				$aux = array('name' => $tabelas[$i], 'value' => $tabelas[$i] );
				array_push($retorno, $aux);
			}
		}
		else
			$retorno 	= $tabelas;

		retornarJson(null, $retorno);
	}
	/*
	* [Método]: getCamposDb
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_listagem.php
	* @param 
	* @return 
	*/
	public function getColunasDb()
	{
		verificarStatusSessao();
		$nome 			= urldecode(decifracub3($this->input->get("nome")));

		retornarJson(null, $this->db->field_data($nome));
	}

	public function colunasImportar()
	{
			verificarStatusSessao();
			header('Content-type: application/json');

			$this->load->model("padrao_model");
			$dados 										= array();
			$dados 										= json_decode(file_get_contents('php://input'),true);
			$this->load->helper("date");

			$nomeTabela 								= urldecode(decifracub3($this->input->get("nomeTabela")));
			$slug 										= urldecode(decifracub3($this->input->get("slug")));

			$camposTabela 								= $dados["coluna"];

			$dadosListagem 							= array();
			$dadosListagem["lisTitulo"]   			= "Listagem da tabela ".$nomeTabela;
			$dadosListagem["lisSlug"]				= $slug == '' ? $nomeTabela . now() : $this->validarSlug($slug);
			$dadosListagem["lisHorario"]  			= date("Y-m-d H:i:s");
			$dadosListagem["usuCodigo"]   			= $this->session->userdata('s_usuCodigo');
	 		$dadosListagem["lisBancoDeDados"]		= $nomeTabela;
		 	$dadosListagem["lisBancoDeDadosKey"]  	= isset($dados["primaryKey"]) ? $dados["primaryKey"] : '';
	 		$titulos 								= array();

	 		if($camposTabela != null && Count($camposTabela) > 0){
		 		for ($i=0; $i < Count($camposTabela); $i++) {  
		 			$titulos[$i]["campo"]					= !isset($camposTabela[$i]["campo"]) ? $camposTabela[$i] : $camposTabela[$i]["campo"];
		 			$titulos[$i]["nome"]					= !isset($camposTabela[$i]["nome"]) ? $camposTabela[$i] : $camposTabela[$i]["nome"];
		 			$titulos[$i]["tipo"]					= "";
		 			$titulos[$i]["group"]					= "Fixo";
		 		}
		 		$dadosListagem["lisTitulos"] 			= json_encode($titulos);
	 		}

			if($this->padrao_model->inserir('cub3_listagem', $dadosListagem))
			{
				$lisCodigo = $this->db->insert_id();   
				retornarJson(true, array('mensagem' => 'A listagem foi importada com sucesso!', 'url' => 'cub3_listagem/listagemVisualizar/?lisCodigo='.cifracub3($lisCodigo), 'slug' => $dadosListagem["lisSlug"],'lisTitulo' => $dadosListagem["lisTitulo"], "lisCodigo" => $lisCodigo ));
			}
			else
				retornarJson(false, array('mensagem' => 'Ocorreu um erro! Por favor, tente novamente.'));

	}

	function validarSlug($slug)
	{   
 		$listagem 		=	$this->cub3_listagem_model->listagemPorCodigoOuSlug($slug);

 		if($listagem->num_rows() != null && $listagem->num_rows() > 0)
 			return $slug . $listagem->num_rows()+1;
 		else
 			return $slug;
	}
		/*
	* [Método]: excluirPadrao 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	public function excluirPadrao ()
	{
		verificarStatusSessao();
		$this->load->model('padrao_model');
		$dados 								= array(); 
		$lisCodigo 							= urldecode(decifracub3($this->input->get("lisCodigo")));

		$listagem 							=  $this->cub3_listagem_model->listagemPorCodigo($lisCodigo)->row_array();
		$codigo 							= urldecode(decifracub3($this->input->get("codigo")));
		$mensagemRetorno 					= 'Linha excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da linha.';
		$campoIdentificador 				= $listagem["lisBancoDeDadosKey"];
		$tabela 							= $listagem["lisBancoDeDados"];
		$campoStatus 						= $listagem["lisBancoDeDadosKeyStatus"];

		$dados[$campoIdentificador]			= $codigo;
    
		$post 			= array();
		$post 			= json_decode(file_get_contents('php://input'),true);
		$auxContagem 	= 0;

		if($post != null && Count($post) > 0){
			for ($i=0; $i < Count($post); $i++) { 
				$aux 		= array($campoIdentificador => $post[$i][$campoIdentificador]);
				if($this->padrao_model->excluir($tabela, $aux))
						$auxContagem++;
			}		
			if($auxContagem > 0)
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));			
		}
		else{
				if(isset($dados[$campoIdentificador])) { 		
					if(isset($campoStatus) && $campoStatus != null && $campoStatus != ''){
						$dados[$campoStatus]	 			= 'EXCLUIDO';
						$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
						if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
							retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
						else
							retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));				
					}
					else {
						$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
						if($this->padrao_model->excluir($tabela, $condicao) > 0) 
							retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
						else
							retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));							
					}
				}
				else
						retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}	


	}

}
