<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_permissoes extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
    }
	public function index()
	{
		verificarStatusSessao();
		$this->load->model("cub3_menu_model");
		$this->load->model("cub3_grupo_model");

		$data["menus"]	= $this->cub3_menu_model->menuListarTodos();
		$data["grupos"]	= $this->cub3_grupo_model->grupoListarTodos();
		$data["view"]	= "cub3_permissoes/permissoesListar";
		carregarTema($data);
	}
	public function permissaoAssociar(){
		$menCodigo	= $this->input->post("menCodigo");
		$gruCodigo	= $this->input->post("gruCodigo");
		$status		= $this->input->post("status");

		$this->load->model("padrao_model");

		if($status == "true"):

			$dados = array(
				"menCodigo"	=> $menCodigo,
				"gruCodigo"	=> $gruCodigo
			);

			$this->load->model("padrao_model");
			$this->padrao_model->inserir("cub3_grupo_has_menu",$dados);
		else:
			$condicao = array("gruCodigo" => $gruCodigo, "menCodigo" => $menCodigo);
			$this->padrao_model->excluir("cub3_grupo_has_menu",$condicao);
		endif;

		die(json_encode(true));
	}
}

/* End of file permissoes.php */
/* Location: ./application/controllers/permissoes.php */