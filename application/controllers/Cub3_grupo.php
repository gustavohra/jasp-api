<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_grupo extends CI_Controller {

	/*
	* [Método]: index
	* [Descrição]: Lista todos os grupos cadastrados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$data["view"]		= "cub3_grupos/grupoListar";
		carregarTema($data);
	}
	/*
	* [Método]: getGrupos
	* [Descrição]: Busca todos os grupos no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function getGrupos()
	{
		$this->load->model("cub3_grupo_model");
		$dados = $this->cub3_grupo_model->grupoListarTodos()->result_array(); 
		retornarJson(null, $dados);
	}
	/*
	* [Método]: getGrupoPorCodigo
	* [Descrição]: Busca dados de um grupo a partir de seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function getGrupoPorCodigo()
	{
		$this->load->model("cub3_grupo_model");

		$post		= json_decode(file_get_contents('php://input'));

		$dados = $this->cub3_grupo_model->grupoSelecionarCodigo(decifracub3($post->gruCodigo))->row_array(); 
		retornarJson(null, $dados);
	}
		/*
	* [Método]: getGruposPorUnidade
	* [Descrição]: Busca todos os grupos associados a uma unidade
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function getGruposPorUnidade()
	{
		$this->load->model("cub3_grupo_model");
		$post		= json_decode(file_get_contents('php://input'));
		$dados = $this->cub3_grupo_model->grupoPorPorUnidade($post->uniCodigo);

		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}
	/*
	* [Método]: getGrupoPorUsuario
	* [Descrição]: Busca o grupo de um usuário por seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function getGrupoPorUsuario()
	{
		$this->load->model("cub3_grupo_model");
		$post		= json_decode(file_get_contents('php://input'));
		$dados 		= $this->cub3_grupo_model->grupoPorUsuario(decifracub3($post->usuCodigo));

		$retorno 	= array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}
	/*
	* [Método]: grupoCadastrar
	* [Descrição]: Abre o formulário de cadastro de grupos
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function grupoCadastrar(){
		verificarStatusSessao();
		$this->load->model("cub3_unidade_model");

		$data["unidades"] 	= $this->cub3_unidade_model->unidadeListarTodos();
		$data["view"]		= "cub3_grupos/grupoCadastrar";
		carregarTema($data);
	}
	/*
	* [Método]: grupoInserir
	* [Descrição]: Realiza a inserção de um grupo no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function grupoInserir(){
		verificarStatusSessao();

		$post		= json_decode(file_get_contents('php://input'));
		$this->load->model("padrao_model");

		$dados = array(
			"gruDescricao"	=> $post->gruDescricao,
			"uniCodigo"		=> $post->uniCodigo,
			"gruStatus"		=> $post->gruStatus
		);
		die(json_encode($this->padrao_model->inserir("cub3_grupo",$dados)));
	}
	/*
	* [Método]: grupoEditar
	* [Descrição]: Abre o formulário para edição de um grupo
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function grupoEditar(){
		verificarStatusSessao();
		$data["gruCodigo"]		= urldecode(decifracub3($this->input->get("gruCodigo")));
		$data["view"]			= "cub3_grupos/grupoEditar";
		carregarTema($data);

	}
	/*
	* [Método]: grupoAlterar
	* [Descrição]: Realiza a alteração de dados de um grupo no banco de dados.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function grupoAlterar(){
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$post = json_decode(file_get_contents('php://input'));

		$dados = array(
			"uniCodigo"		=> $post->uniCodigo,
			"gruDescricao"	=> $post->gruDescricao,
			"gruStatus"		=> $post->gruStatus
		);

		$condicao = array("gruCodigo" => $post->gruCodigo);
		die(json_encode($this->padrao_model->alterar("cub3_grupo",$dados,$condicao)));
	}
	/*
	* [Método]: grupoExcluir
	* [Descrição]: Exclui um grupo
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Grupo.php
	* @param 
	* @return 
	*/
	public function grupoExcluir(){
		verificarStatusSessao();
		$gruCodigo = urldecode(decifracub3($this->input->get("gruCodigo")));
		
		$this->load->model("padrao_model");
		$codicao = array("gruCodigo" => $gruCodigo);

		$this->padrao_model->excluir("cub3_grupo_has_menu",$codicao);
		$this->padrao_model->excluir("cub3_grupo_has_usuario",$codicao);
		if($this->padrao_model->excluir("cub3_grupo",$codicao))
			retornarJson(true);
		else
			retornarJson(false);

	}

}

/* End of file grupo.php */
/* Location: ./application/controllers/grupo.php */