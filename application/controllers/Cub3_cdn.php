<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 03/06/2016 às 17:48:27
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_cdn extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$dados = array();

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '/admin');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-book"></i> Repositório / CDN', 'cub3_cdn/');

		$dados["titulo"]		=	"Repositórios";
		$dados["descricao"]		=	"Área destinada à listagem de componentes adicionados ao CDN.";		
		$dados["view"]			= 	"cub3_cdn/repositorios";

		carregarTema($dados);
	}
	/*
	* [Método]: getPluginsJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/CUB3Site/application/controllers/Conhecimento.php
	* @param 
	* @return 
	*/
	public function getPluginsJson()
	{
		verificarAdministrador();
		$this->load->model("cub3_cdn_model"); 
 		$tipo 					= urldecode(decifracub3($this->input->get("tipo"))); 
 		$retorno 				= $this->cub3_cdn_model->listarPlugins($tipo)->result_array(); 
 
		retornarJson(null, $retorno); 
	}
	/*
	* [Método]: getPluginsSublimeJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/CUB3Site/application/controllers/Conhecimento.php
	* @param 
	* @return 
	*/
	public function getPluginsSublimeJson()
	{ 
		$this->load->model("cub3_cdn_model"); 
 		$tipo 					= $this->input->get("tipo"); 
 		$retorno 				= $this->cub3_cdn_model->listarPluginsSublime($tipo)->result_array(); 
 		$dados 					= array();

 		for ($i=0; $i < Count($retorno); $i++) { 

 			$auxMain = json_decode($retorno[$i]["main"], true); 
 			$dados[$i]["name"] 		= $retorno[$i]["name"];
 			$dados[$i]["conteudo"]	= ""; 
	 		switch ($tipo) {
	 			case 'bower':
	 				$diretorio      = "http://cdn.cub3.com.br/bower_components/".$dados[$i]["name"];
	 				break;
	 			case "npm":
	 				$diretorio      = "http://cdn.cub3.com.br/node_modules/".$dados[$i]["name"];
	 				break;
	 		}


 			if(is_array($auxMain)) { 
		 				for ($k=0; $k < Count($auxMain); $k++) { 
			 				$auxExplode = explode(".", $auxMain[$k]);  
			 				if($auxExplode[1] == 'css' || $auxExplode[1] == 'js') { 
			 					if($auxExplode[1] == 'css')
			 						$dados[$i]["conteudo"] .= '<link rel="stylesheet" type="text/css" href="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].'"> ';
			 					elseif($auxExplode[1] == 'js')
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].'"></script>';
			 					elseif($auxExplode[1] == 'min')
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].".".$auxExplode[2].'"></script>';
			 				} 
			 				elseif (isset($auxExplode[2])) { 
			 					if($auxExplode[2] == 'css')
			 						$dados[$i]["conteudo"] .= '<link rel="stylesheet" type="text/css" href="'.$diretorio."/".$auxExplode[1].".".$auxExplode[2].'">';
			 					elseif($auxExplode[2] == 'js')
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].".".$auxExplode[2].'"></script>';
			 					elseif($auxExplode[2] == 'min')
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].".".$auxExplode[2].".".$auxExplode[3].'"></script>';
			 				}  
		 				}

 			} 
 			else { 
		 			if($auxMain != null && substr($auxMain[0], 0, 1) == "."){ 
			 				$auxExplode = explode(".", $auxMain); 
			 				switch ($auxExplode[2]) {
			 					case 'css':
			 						$dados[$i]["conteudo"] .= '<link rel="stylesheet" type="text/css" href="'.$diretorio.$auxExplode[1].".".$auxExplode[2].'">';
			 						break;
			 					case 'js':
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio.$auxExplode[1].".".$auxExplode[2].'"></script>';
			 						break;
			 					case 'min':
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio.$auxExplode[1].".".$auxExplode[2].".".$auxExplode[3].'"></script>';
			 						break;

			 				}  
		 			}
		 			elseif($auxMain != null & substr($auxMain[0], 0, 1) != ".") {
 
			 				$auxExplode = explode(".", $auxMain); 
			 				switch ($auxExplode[1]) {
			 					case 'css':
			 						$dados[$i]["conteudo"] .= '<link rel="stylesheet" type="text/css" href="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].'">';
			 						break;
			 					
			 					case 'js':
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].'"></script>';
			 						break;
			 					case 'min':
			 						$dados[$i]["conteudo"] .= '<script type="text/javascript" src="'.$diretorio."/".$auxExplode[0].".".$auxExplode[1].".".$auxExplode[2].'"></script>';
			 						break;
			 				}  
		 			} 
 			}
 		}
		retornarJson(null, $dados); 
	}
	/*
	* [Método]: getCategoriasJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/CUB3Site/application/controllers/Conhecimento.php
	* @param 
	* @return 
	*/
	public function getCategoriasJson()
	{
		verificarStatusSessao();
		$this->load->model("cub3_cdn_model"); 
 
			retornarJson(null, $this->cub3_cdn_model->listarCategorias()->result_array()); 
	}

		/*
	* [Método]: versaoSistema
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/CUB3Site/application/controllers/Cub3_cdn.php
	* @param 
	* @return 
	*/
	public function versaoSistema()
	{
		$this->load->model("cub3_cdn_model"); 
		$versao = $this->cub3_cdn_model->versaoSistema()->row_array();
		retornarJson(null, $versao);
	}

		/*
	* [Método]: urlSistema
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/CUB3Site/application/controllers/Cub3_cdn.php
	* @param 
	* @return 
	*/
	public function urlSistema()
	{
		verificarAdministrador(); 
		$this->load->model("cub3_cdn_model"); 
		$versao = $this->cub3_cdn_model->versaoSistema()->row_array();
		retornarJson(null, array('url' => 'http://cdn.cub3.com.br/CUB3Painel/others/versoes/'.$versao["verCodigo"]."/"."master.zip"));
	}




}
