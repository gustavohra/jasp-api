<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 19/02/2017 às 14:30:57
	*
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Pagamentos.php
	*/
class Pagamentos extends CI_Controller {
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Pagamentos.php
	* @param 
	* @return 
	*/
	public function retorno()
	{
		//Cria log
		$dados = array("iugId" => $_POST["data"]["id"],"iugStatus" => $_POST["data"]["status"],"iugTipo" => $_POST["event"], "iugHorario" => date("Y-m-d H:i:s"));
		$this->padrao_model->inserir("sys_logIugu",$dados);
	}
}
