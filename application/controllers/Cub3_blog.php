<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Classe referente ao gerenciamento de blogs e seus conteúdos
	* [Criação]: 28/04/2016 às 23:52:50
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model("cub3_blog_model");
		$this->load->model("padrao_model");
		$this->load->helper("cub3_blog_helper");
	}

	// Métodos de ajuda
	public function getCategoriasJson()
	{
		return getCategoriasJson();
	}
	public function getPostsJson()
	{
		return getPostsJson();
	} 
	public function getCategoriasSelect()
	{
		return getCategoriasSelect();
	}
	public function getSubCategorias()
	{
 		$bcaCodigo 							= urldecode(decifracub3($this->input->get("bcaCodigo")));
 		return getSubCategorias($bcaCodigo);
	}
	public function getSlidersJson()
	{
		return getSlidersJson();
	} 
	public function getPostJson()
	{
 		$slug 				= urldecode(decifracub3($this->input->get("slug")));

 		return getPostJson($slug);
	}
	public function getNoticiasMaisVisualizadas()
	{
 		return getNoticiasMaisVisualizadas();
	}
 
	public function getPostPorCategoria()
	{
 		$categoria			= urldecode(decifracub3($this->input->get("categoria")));
 		$qtd 				= urldecode(decifracub3($this->input->get("qtd")));

 		return getPostPorCategoria($qtd, $categoria);
	} 
	public function getTags()
	{ 
		return getTags();
	} 
	public function getBlogJson()
	{
		$bpoCodigo 			= urldecode(decifracub3($this->input->get("bpoCodigo")));
		return getBlogJson($bpoCodigo);
	} 
	public function getBlogPorSlugJson()
	{
		$bpoSlug 			= urldecode(decifracub3($this->input->get("bpoSlug")));
		return getBlogJson($bpoSlug);
	} 

	public function getPostCategoriasJson()
	{
		$bpoCodigo 			= urldecode(decifracub3($this->input->get("bpoCodigo")));
		return getPostCategoriasJson($bpoCodigo);
	} 

	public function getPostVisualizacoesJson()
	{
		$bpoCodigo 			= urldecode(decifracub3($this->input->get("bpoCodigo")));
		return getPostVisualizacoesJson($bpoCodigo);
	} 
	public function getBlogCategoriaPorCodigo()
	{
		$bcaCodigo 							= urldecode(decifracub3($this->input->get("bcaCodigo"))); 
		return getBlogCategoriaPorCodigo($bcaCodigo);
	}
	private function blogValidarSlug($bpoSlug = '')
	{
		return blogValidarSlug($bpoSlug);
	} 
	private function blogCategoriaValidarSlug($bpoSlug = '')
	{
		return blogCategoriaValidarSlug($bpoSlug);
	}
	public function tratarCategorias($categorias, $bpoCodigo)
	{
		return tratarCategorias($categorias, $bpoCodigo);
	}
	public function adicionarVisualizacaoPost()
	{
		$bpoCodigo 			= urldecode(decifracub3($this->input->get("bpoCodigo")));

		if(adicionarVisualizacaoPost($bpoCodigo))
			retornarJson(true);
		else
			retornarJson(false);
	}
	public function criarCachePostsGeral()
	{
		verificarAdministrador(); 
		return criarCachePostsGeral();
	} 

	/*
	* [Método]: index
	* [Descrição]: Listagem de blogs no escopo administrativo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Postagens";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-comment-o"></i> Blog / '.$dados["titulo"], 'blog');

		$dados["view"]	= "cub3_blog/blogListar";
		carregarTema($dados);
	}
	/*
	* [Método]: blogCategoriasListar
	* [Descrição]: Listagem de blogs no escopo administrativo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function blogCategoriasListar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Categorias";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-folde-open-o"></i> Blog / '.$dados["titulo"], 'blog');

		$dados["view"]	= "cub3_blog/blogCategoriasListar";
		carregarTema($dados);
	}
	/*
	* [Método]: blogCadastrar
	* [Descrição]: Exibe o formulário de cadastro de uma blog
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function postCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastrar Postagem";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Blog / Notícias', 'blog');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Blog / '.$dados["titulo"], 'blogCadastrar');

		$dados["paginaSubmit"]	= "blogCadastrar(blogDados)";
		$dados["controller"]	= "Cub3BlogCtrl";
		$dados["idFormulario"]	= "blog-cadastro";
		$dados["view"]			= "cub3_blog/blog";
		
		carregarTema($dados);
	}
	/*
	* [Método]: postEditar
	* [Descrição]: Exibe o formulário de edição de uma blog
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function postEditar()
	{
		verificarStatusSessao();

		$dados["titulo"] 	= "Editar post";
		$bpoCodigo 			= decifracub3($this->input->get("bpoCodigo"));

		$blog 			= $this->cub3_blog_model->blogListarPorCodigo($bpoCodigo)->row_array();

		if($blog != null):
			// Breadcrumbs
			$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Blog / Blog', 'blog');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Blog / '.$dados["titulo"], 'blogEditar');

			$dados["paginaSubmit"]	= "blogEditar(blogDados)";
			$dados["bpoCodigo"]		= cifracub3($blog["bpoCodigo"]);
			$dados["controller"]	= "Cub3BlogCtrl";
			$dados["idFormulario"]	= "blog-cadastro";
			$dados["view"]			= "cub3_blog/blog";
			carregarTema($dados);
		else:
           redirect('erro404');			
       	endif;
	}

	/*
	* [Método]: categoriaCadastrar
	* [Descrição]: Exibe o formulário de cadastro de uma categoria
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function categoriaCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastrar Categoria";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Blog / Categoria', 'blog');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Blog / '.$dados["titulo"], 'categoriaCadastrar');

		$dados["paginaSubmit"]	= "blogCadastrar(blogDados)";
		$dados["controller"]	= "Cub3BlogCategoriaCtrl";
		$dados["idFormulario"]	= "categoria-cadastro";
		$dados["view"]			= "cub3_blog/categoria";
		
		carregarTema($dados);
	}
	/*
	* [Método]: categoriaEditar
	* [Descrição]: Exibe o formulário de edição de uma categoria
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function categoriaEditar()
	{
		verificarStatusSessao();

		$dados["titulo"] 	= "Editar categoria";
		$bcaCodigo 			= decifracub3($this->input->get("bcaCodigo")); 

		$categoria 			= $this->cub3_blog_model->blogListarCategoriaPorCodigo($bcaCodigo)->row_array();

		if($categoria != null):
			// Breadcrumbs
			$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Blog / Categoria', 'blog');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Blog / '.$dados["titulo"], 'categoriaEditar');

			$dados["paginaSubmit"]	= "categoriaEditar(blogDados)";
			$dados["bcaCodigo"]		= cifracub3($bcaCodigo);
			$dados["controller"]	= "Cub3BlogCategoriaCtrl";
			$dados["idFormulario"]	= "categoria-cadastro";
			$dados["view"]			= "cub3_blog/categoria";
			carregarTema($dados);
		else:
           redirect('erro404');			
       	endif;
	}
 
	public function blogInserir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		$dados["usuCodigo"] = $this->session->userdata('s_usuCodigo');
		$categorias 		= null;

		if(isset($dados["anexo"])){
			$dados["bpoThumbnail"] = $dados["anexo"];
			unset($dados["anexo"]);
		}

		// Verifica se o slug é válido
		$dados["bpoSlug"] 				= $this->blogValidarSlug($dados["bpoSlug"]);

		if(isset($dados["bpoTags"])) 
			$dados["bpoTags"] 			= json_encode($dados["bpoTags"]);
		else
			unset($dados["bpoTags"]);
		
		if(isset($dados["bpoCategorias"])) {
			$categorias 				= $dados["bpoCategorias"];
			unset($dados["bpoCategorias"]);
		}

		if(!isset($dados["bpoCodigo"])){
			if($this->padrao_model->inserir('cub3_blog_posts', $dados) > 0){
				$bpoCodigo 				= $this->db->insert_id();

				if(!is_null($categorias)) {
					$this->cub3_blog_model->limparPostCategorias($bpoCodigo);
					$categorias 				= $this->tratarCategorias($categorias, $bpoCodigo);

					foreach ($categorias as $categoria) {
						$this->padrao_model->inserir('cub3_blog_posts_has_cub3_blog_categoria',$categoria);
					}
				}
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);
		}
		else {
			$condicao = array('bpoCodigo' => $dados["bpoCodigo"]);
			if($this->padrao_model->alterar('cub3_blog_posts', $dados, $condicao) > 0){

				if(isset($categorias)){
					$this->cub3_blog_model->limparPostCategorias($dados["bpoCodigo"]);
					$categorias 				= $this->tratarCategorias($categorias,  $dados["bpoCodigo"]);

					foreach ($categorias as $categoria) {
						$this->padrao_model->inserir('cub3_blog_posts_has_cub3_blog_categoria',$categoria);
					} 
				}
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);			
		}
	}  
 



		/*
	* [Método]: blogCategoriaInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_blog.php
	* @param 
	* @return 
	*/
	public function blogCategoriaInserir()
	{
		verificarStatusSessao();
		$post 				= json_decode(file_get_contents('php://input'), true);  
		$post["usuCodigo"]	= $this->session->userdata('s_usuCodigo');
		$post["bcaSlug"] 	= $this->blogCategoriaValidarSlug($post["bcaSlug"]);

		$subCategorias		= null; 

		if(isset($post["subCategorias"])){
			$subCategorias 		= $post["subCategorias"];
			unset($post["subCategorias"]);			
		}

		if(!isset($post["bcaCodigo"])){
			if($this->padrao_model->inserir("cub3_blog_categoria", $post)){
				$bcaCodigo = $this->db->insert_id();

				if(!is_null($subCategorias)){
					for ($i=0; $i < Count($subCategorias) ; $i++) { 
						$subCat 					= $subCategorias[$i];
						$subCat["bcaSlug"] 			= $this->blogCategoriaValidarSlug($post["bcaSlug"]);
						$subCat["usuCodigo"]		= $this->session->userdata('s_usuCodigo');
						$subCat["bcaCodigoPai"] 	= $bcaCodigo; 

						$this->padrao_model->inserir("cub3_blog_categoria",$subCat); 
					}
				}
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);
		}
		else {
			$condicao = array("bcaCodigo" => $post["bcaCodigo"]);
			if($this->padrao_model->alterar("cub3_blog_categoria",$post,$condicao)) {
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);

		}

	}
 
		/*
	* [Método]: subCategoriaInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_blog.php
	* @param 
	* @return 
	*/
	public function subCategoriaInserir()
	{
		verificarStatusSessao();
		$post 					= json_decode(file_get_contents('php://input'), true);  
		$post["usuCodigo"]		= $this->session->userdata('s_usuCodigo');
		$post["bcaSlug"] 		= $this->blogCategoriaValidarSlug($post["bcaSlug"]); 

		$subCategorias		= null; 

		if(isset($post["subCategorias"])){
			$subCategorias 		= $post["subCategorias"];
			unset($post["subCategorias"]);			
		}

		if(!isset($post["bcaCodigo"])){
			if($this->padrao_model->inserir("cub3_blog_categoria", $post)){
				$bcaCodigo = $this->db->insert_id();

				if(!is_null($subCategorias)){
					for ($i=0; $i < Count($subCategorias) ; $i++) { 
						$subCat 					= $subCategorias[$i];
						$subCat["bcaSlug"] 			= $this->blogCategoriaValidarSlug($post["bcaSlug"]);
						$subCat["usuCodigo"]		= $this->session->userdata('s_usuCodigo');
						$subCat["bcaCodigoPai"] 	= $bcaCodigo; 

						$this->padrao_model->inserir("cub3_blog_categoria",$subCat); 
					}
				}
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);
		}
		else {
			$condicao = array("bcaCodigo" => $post["bcaCodigo"]);
			if($this->padrao_model->alterar("cub3_blog_categoria",$post,$condicao)) {
				criarCachePostsGeral();
				retornarJson(true);
			}
			else
				retornarJson(false);

		}

	}
 


		/*
	* [Método]: subCategoriaAlterar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_menu.php
	* @param 
	* @return 
	*/
	public function subCategoriaAlterar()
	{
		verificarStatusSessao();

			$this->load->model("padrao_model");
			$bcaCodigo			 	= urldecode(decifracub3($this->input->get("bcaCodigo")));
			$campo 					= urldecode(decifracub3($this->input->get("campo")));
			$valor 					= urldecode(($this->input->get("valor")));

			$dados[$campo] 			= $valor;
			$condicao 				= array("bcaCodigo" => $bcaCodigo);

			if($this->padrao_model->alterar("cub3_blog_categoria",$dados,$condicao))
				retornarJson(true);
			else
				retornarJson(false);
	}

	/*
	* [Método]: blogAlterarJson
	* [Descrição]: Recebe um post via JSON e realiza a alteração do conteúdo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function blogAlterar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		$condicao = array("bloCodigo" => $dados["bloCodigo"]);

		if(isset($dados["anexo"])){
			$dados["bloImagem"] = $dados["anexo"]; 
		}

		unset($dados["anexo"]);

		if($this->padrao_model->alterar('cub3_blog', $dados, $condicao)){
			criarCachePostsGeral();
			retornarJson(true);
		}
		else	
			retornarJson(false);
	}
	/*
	* [Método]: blogExcluir
	* [Descrição]:  Recebe o código e realiza a exclusão do conteúdo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function blogExcluir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$bpoCodigo 			= urldecode(decifracub3($this->input->get("bpoCodigo")));

		$dados["bpoStatus"] = 'EXCLUIDO';

		$condicao = array("bpoCodigo" => $bpoCodigo);

		if($this->padrao_model->alterar('cub3_blog_posts', $dados, $condicao)){
			criarCachePostsGeral();
			retornarJson(true);
		}
		else	
			retornarJson(false);		
	}
	/*
	* [Método]: blogCategoriaExcluir
	* [Descrição]:  Recebe o código e realiza a exclusão do conteúdo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Blog.php
	* @param 
	* @return 
	*/
	public function blogCategoriaExcluir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$bcaCodigo 			= urldecode(decifracub3($this->input->get("bcaCodigo")));

		$dados["bcaStatus"] = 'EXCLUIDO';

		$condicao = array("bcaCodigo" => $bcaCodigo);

		if($this->padrao_model->alterar('cub3_blog_categoria', $dados, $condicao)){
			$subCategorias = $this->cub3_blog_model->blogListarSubCategorias($bcaCodigo)->result_array();

			// Exclui as subCategorias
			for ($i=0; $i < Count($subCategorias); $i++) { 
				$condicao 			= array("bcaCodigo" => $subCategorias[$i]["bcaCodigo"]);
				$this->padrao_model->alterar('cub3_blog_categoria', $dados, $condicao);
			}
			retornarJson(true);
		}
		else	
			retornarJson(false);		
	}


	/*
	* [Método]: sliders
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/controllers/Cub3_blog.php
	* @param 
	* @return 
	*/
	public function sliders()
	{
		$dados["titulo"] 	= "Sliders";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-photo"></i>'.$dados["titulo"], 'cub3_blog/sliders');

		$dados["view"]				= "cub3_listagem/listagemPadrao";
		$dados["lisCodigo"]			= "11";
		carregarTema($dados);		
	}
	/*
	* [Método]: sliderCadastrar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/controllers/Areas.php
	* @param 
	* @return 
	*/
	public function sliderCadastrar()
	{
		$dados["titulo"] 	= "Cadastrar Slider";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-photo"></i> Sliders', 'cub3_blog/sliders/');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i>'.$dados["titulo"], 'cub3_blog/sliderCadastrar');

		$dados["view"]				= "cub3_formularios/formularioPadrao";
		$dados["forTitulo"]			= "<i class='fa fa-plus'></i> Cadastro de slider";
		$dados["forSlug"]			= "slider";
		
		carregarTema($dados);		

	}

	/*
	* [Método]: sliderInserir 
	* [Descrição]: 
	* [Comentários]: 
	*  
	* @param 
	* @return 
	*/
	public function sliderInserir ($campoIdentificador = "blsCodigo", $tabela = "cub3_blog_slider")
	{
		$this->load->model("padrao_model");
		$post 	= json_decode(file_get_contents('php://input'), true);
		$dados 	= $post;
  		$dados 	= tratarAnexo($dados, "blsImagem");
 
		if(!isset($dados[$campoIdentificador])){
			if($this->padrao_model->inserir($tabela, $dados) > 0){
				$this->montarSliders();
				retornarJson(null, array("resposta" => true, "url" => "cub3_blog/sliders/", "mensagem" => "Slider cadastrado com sucesso."));
			}
			else
				retornarJson(false);			
		}	
		else {			 
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) {
				$this->montarSliders();
				retornarJson(null, array("resposta" => true, "url" => "cub3_blog/sliders/", "mensagem" => "Slider modificado com sucesso.")); 
			}
			else
				retornarJson(false);
		}	
	}
	/*
	* [Método]: sliderEditar
	* [Descrição]: 
	* [Comentários]: 
	*  
	* @param 
	* @return 
	*/
	public function sliderEditar()
	{
		$dados["titulo"] 							= "Edição de slider";
		$codigo 									= decifracub3($this->input->get("blsCodigo"));
		if($codigo != ''){
			$slider 								= $this->cub3_blog_model->sliderPorCodigo($codigo)->row_array();  

			$dados["modelDados"] 						= cifracub3(json_encode($slider)); 
		} 
		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-photo"></i> Slider '.$dados["titulo"], 'cub3_blog/sliders');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-edit"></i> Edição', 'cub3_blog/sliderEditar');

		$dados["view"]				= "cub3_formularios/formularioPadrao";
		$dados["forSlug"]			= "slider";
		carregarTema($dados);
	}
	/*
	* [Método]: sliderExcluir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/controllers/Sintomas.php
	* @param 
	* @return 
	*/
	public function sliderExcluir($campoIdentificador = "blsCodigo", $campoStatus = "blsStatus", $banco = "cub3_blog_slider")
	{
		verificarStatusSessao(); 
		$this->load->model("padrao_model");
		$dados 								= array(); 
		$dados[$campoStatus]	 			= "EXCLUIDO";
		$dados[$campoIdentificador]			= urldecode(decifracub3($this->input->get($campoIdentificador)));

		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($banco, $dados, $condicao) > 0) {
				$this->montarSliders();
				retornarJson(null, array("resposta" => true, "url" => "cub3_blog/sliders/", "mensagem" => "Slider removido com sucesso.")); 
			}
			else
				retornarJson(null, array("resposta" => false, "url" => "cub3_blog/sliders/", "mensagem" => "Não foi possível realizar a remoção do Slider."));
		}
		else
				retornarJson(null, array("resposta" => false, "url" => "cub3_blog/sliders/", "mensagem" => "Não foi possível realizar a remoção do Slider."));
	}

	/*
	* [Método]: sliderDestacar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/controllers/Sintomas.php
	* @param 
	* @return 
	*/
	public function sliderDestacar($campoIdentificador = "blsCodigo", $campoStatus = "blsDestaque", $banco = "cub3_blog_slider")
	{
		verificarStatusSessao(); 
		$this->load->model("padrao_model");
		$dados 								= array(); 
		$dados[$campoStatus]	 			= 1;
		$dados[$campoIdentificador]			= urldecode(decifracub3($this->input->get($campoIdentificador)));

		if(isset($dados[$campoIdentificador]) && $this->sliderRemoverDestaque()){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($banco, $dados, $condicao) > 0) {
				$this->montarSliders();
				retornarJson(null, array("resposta" => true, "url" => "cub3_blog/sliders/", "mensagem" => "Slider destacado com sucesso.")); 
			}
			else
				retornarJson(null, array("resposta" => false, "url" => "cub3_blog/sliders/", "mensagem" => "Não foi possível realizar o destaque do Slider."));
		}
		else
				retornarJson(null, array("resposta" => false, "url" => "cub3_blog/sliders/", "mensagem" => "Não foi possível realizar o destaque do Slider."));
	}
		/*
	* [Método]: sliderRemoverDestaque
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/oentusiasta/application/controllers/Cub3_blog.php
	* @param 
	* @return 
	*/
	private function sliderRemoverDestaque()
	{
			$condicao = array("blsDestaque" => 1);
			$dados 	  = array('blsDestaque' => 0);
			$banco 	  = "cub3_blog_slider";

			if($this->padrao_model->alterar($banco, $dados, $condicao) > 0){
				$this->montarSliders();
				return true;
			}
			else
				return false;
	}
	// Métodos de ajuda
	public function montarSliders()
	{  
		$sliders 							= getSlidersJson(false);   
		for ($i=0; $i < Count($sliders); $i++) {   
		 		$bcaCodigo 					= $sliders[$i]["bcaCodigo"]; 
		 		if($bcaCodigo != '')
		 			$sliders[$i]["categoria"] 	= getBlogCategoriaPorCodigo($bcaCodigo, false);
		}
		$this->salvarSlidersJson($sliders);
	} 

		/*
	* [Método]: salvarMenuJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/oentusiasta/application/controllers/Entusiasta_menu.php
	* @param 
	* @return 
	*/
	private function salvarSlidersJson($dados)
	{
		if($dados != null){
			$dados 			= (json_encode($dados, JSON_PRETTY_PRINT));

			if(!is_dir("./cache"))
				mkdir("./cache", 0755, true);

			$arquivo 		= fopen("./cache/sliders.json", "w");
			
			if($dados != false){
				try {
					fwrite($arquivo, $dados);
					fclose($arquivo);
				} catch (Exception $e) {
					return false;
				}	finally {
				   return true;
				}
			}
			else
				return false;
		}
		else
			return false;
	}

	/*
	* [Método]: compartilharPorEmail
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/Desenvolvimento/htdocs/oentusiasta/application/controllers/Cub3_blog.php
	* @param 
	* @return 
	*/
	public function compartilharPorEmail()
	{
		$dados = json_decode(file_get_contents('php://input'),true);
		$post  = getBlogJson($dados["bpoCodigo"], false); 
		$conteudo 	= array(
					'nome'				=> $dados["nome"],
					'bpoTitulo' 		=> $post["bpoTitulo"],
					'bpoResumo'			=> $post["bpoResumo"],
					'bpoThumbnail' 		=> $post["bpoThumbnail"],
					'url'				=> base_url()."post/".$post["bpoSlug"],
					'destinatario'		=> $dados["email"],
					'assunto'			=> $dados["nomeAutor"] .' compartilhou uma matéria de '.$this->config->item("portal_nome"));

		if(enviarEmail($conteudo, 'compartilhar-post'))
			retornarJson(true);
		else
			retornarJson(false);

	}


}
