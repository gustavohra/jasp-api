<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 08/08/2016 às 17:10:46
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_social extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: getFotosFanPage
	* [Descrição]: Retorna em formato JSON, o conteúdo publicado pela FanPage configurada no projeto
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function facebookGetFotosFanPage()
	{
		retornarJson($this->cub3_facebook->get_fanpage());
	}
	/*
	* [Método]: getFotosFanPage
	* [Descrição]: Retorna em formato JSON, o conteúdo publicado pela FanPage configurada no projeto
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function facebookGetFotoPorId()
	{
		$fotoId 		= urldecode(decifracub3($this->input->get("id")));
		retornarJson($this->cub3_facebook->get_foto_por_id($fotoId));
	}

}