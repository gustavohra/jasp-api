<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_perfil extends CI_Controller {

	public function index()
	{
		verificarStatusSessao("admin");
		$data["view"]		= "cub3_perfil/perfilListar";
		carregarTema($data);
	}
	/*
	* [Método]: getPerfis
	* [Descrição]: Busca todos os perfis cadastrados.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function getPerfis()
	{
		$this->load->model("cub3_perfil_model");
		$dados = $this->cub3_perfil_model->perfilListarTodos()->result_array();
		retornarJson(null, $dados);
	}
	/*
	* [Método]: getPerfilPorEmail
	* [Descrição]: Busca um usuário na base de dados por seu e-mail
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function getPerfilPorEmail()
	{
		$this->load->model("cub3_perfil_model");

		$post		= json_decode(file_get_contents('php://input'));

		$dados = $this->cub3_perfil_model->perfilSelecionarEmail($post->usuEmail);
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}
	/*
	* [Método]: perfilAssociarSenha
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function perfilAssociarSenha()
	{
		verificarStatusSessao();
		$data["view"]		= "cub3_perfil/perfilCriarSenha";
		$this->load->view("template/externo/externoDialog",$data);
	
	}

	/*
	* [Método]: resetSenha
	* [Descrição]: Realiza o reset de senha a partir da listagem de usuários
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	/*
	* [Método]: criarSenhaAlterar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function criarSenhaAlterar()
	{
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$this->load->model("cub3_perfil_model");
		$this->load->library("encryption");

		//Altera a senha no banco de dados
		$post 		= json_decode(file_get_contents('php://input'));
		$usuCodigo 	= decifracub3($post->usuCodigo);
		$dados 		= array("usuSenha"		=> $this->encryption->encrypt($post->usuSenha));
		$condicao 	= array("usuCodigo" => $usuCodigo);
		$this->padrao_model->alterar("cub3_usuario",$dados,$condicao);
		

		//Envia um e-mail para o usuário com a nova senha
		$dados = $this->cub3_perfil_model->perfilSelecionarCodigo($usuCodigo)->row();
		$dados = array(
			"usuEmail"	=> $dados->usuEmail,
			"usuSenha"	=> $post->usuSenha,
			"usuNome"	=> $dados->usuNome
		);
		$this->perfilEnviarNovaSenha($dados);

		die(json_encode(true));
	}
	public function resetSenha()
	{
		$this->load->model("padrao_model");

		$post		= json_decode(file_get_contents('php://input'));

		if($post->usuStatus == "INATIVO"){
			$dados 		= array("usuStatus" => "ATIVO");
			$condicao 	= array("usuCodigo" => $post->usuCodigo);
			$this->padrao_model->alterar("cub3_usuario",$dados,$condicao);
		}

		//Enviar uma solicitação para criação de senha ao usuário
		$dadosReset = array(
			"usuNome"		=> $post->usuNome,
			"usuCodigo"		=> $post->usuCodigo,
			"usuEmail"		=> $post->usuEmail,
			"usuToken"		=> cifracub3($post->usuCodigo."_".date("Ymdhis"))
		);
		$this->perfilCriarSenha($dadosReset);
		$this->perfilEmailCadastrarSenha($dadosReset);

		die(json_encode(true));
	}
	/*
	* [Método]: getPerfilPorCodigo
	* [Descrição]: Seleciona um perfil de usuário por seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function getPerfilPorCodigo()
	{
		$this->load->model("cub3_perfil_model");

		$post		= json_decode(file_get_contents('php://input'));

		$dados = $this->cub3_perfil_model->perfilSelecionarCodigo(decifracub3($post->usuCodigo));
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}
	public function meuPerfil(){
		verificarStatusSessao();
		$usuCodigo = $this->session->userdata("s_usuCodigo");

		$this->load->model("cub3_perfil_model");
		$this->load->model("cub3_grupo_model");
		
		$dados = $this->cub3_perfil_model->perfilSelecionarCodigo($usuCodigo);

		$data["grupos"]			= $this->cub3_perfil_model->perfilSelecionarGruposAssociados($usuCodigo);
		//$data["areas"]			= $this->cub3_grupo_model->grupoSelecionarAreasAssociadas($usuCodigo);
		$data["usuNome"]		= $dados->row()->usuNome;
		$data["usuSobrenome"]	= $dados->row()->usuSobrenome;
		$data["usuEmail"]		= $dados->row()->usuEmail;
		$data["view"]		= "cub3_perfil/meuPerfil";
		carregarTema($data);
	}
	/*
	* [Método]: meuPerfilJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function meuPerfilJson(){
		verificarStatusSessao();
		$usuCodigo = $this->session->userdata("s_usuCodigo");

		$this->load->model("cub3_perfil_model");
		$this->load->model("cub3_grupo_model");
		
		$dados = $this->cub3_perfil_model->perfilSelecionarCodigo($usuCodigo);

	//	$data["grupos"]			= $this->perfil_model->perfilSelecionarGruposAssociados($usuCodigo);
	//	$data["areas"]			= $this->grupo_model->grupoSelecionarAreasAssociadas($usuCodigo);
		$data["usuario"]		= $dados->row_array();

		die(json_encode($data));
	}
	/*
	* [Método]: meuPerfilAlterar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function meuPerfilAlterar(){
		verificarStatusSessao();
		$usuCodigo 		= $this->session->userdata("s_usuCodigo");
		$usuNome 		= $this->input->post("usuNome");
		$usuSobrenome 	= $this->input->post("usuSobrenome");
		$usuEmail 		= $this->input->post("usuEmail");

		$dados = array(
			"usuNome"		=> $usuNome,
			"usuSobrenome"	=> $usuSobrenome,
			"usuEmail"		=> $usuEmail
		);

		$this->load->model("padrao_model");
		$codicao = array("usuCodigo" => $usuCodigo);
		if($this->padrao_model->alterar("cub3_usuario",$dados,$codicao))
			retornarJson(true);
		else
			retornarJson(false);
	}
	/*
	* [Método]: meuPerfilAlterarJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function meuPerfilAlterarJson(){
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);
		$usuCodigo 		= $this->session->userdata("s_usuCodigo");

		$condicao = array("usuCodigo" => $usuCodigo);
		if(isset($dados["trocarSenha"]))
		{
			$dados["usuSenha"] = cifracub3_256($dados["usuNovaSenha"]);
			unset($dados["trocarSenha"]);
			unset($dados["usuNovaSenha"]);
		}

		if($this->padrao_model->alterar('cub3_usuario', $dados, $condicao) > 0)
			die(json_encode(true));
		else
			die(json_encode(false));
	}
	public function listarPerfis(){
		//Bootgrid - Variáveis do Plugin
		$limiteInicial 		= $this->input->post("current");
		$limiteFinal 		= $this->input->post("rowCount");
		$ordernacao 		= $this->input->post("sort");
		$ordernacaoCampo 	= array_keys($ordernacao);
		$ordernacaoOrdem 	= array_values($ordernacao);
		$palavraChave		= $this->input->post("searchPhrase");

		$this->load->model("cub3_perfil_model");
		$dadosGerais 	= $this->cub3_perfil_model->perfilListarTodos(); 
		$dados 			= $this->cub3_perfil_model->perfilListarTodosAjax($limiteInicial-1,$limiteFinal,$ordernacaoCampo[0],$ordernacaoOrdem[0],$palavraChave);
		
		$retorno = array();
		$retorno["current"] 	= $limiteInicial;
		$retorno["rowCount"] 	= $limiteFinal;
		$retorno["total"]		= $dadosGerais->num_rows();
		$retorno["rows"]		= $dados->result_array();

		die(json_encode($retorno));
	}

	public function perfilCadastrar(){
		verificarStatusSessao();
		$this->load->model("cub3_unidade_model");

		$data["unidades"] 	= $this->cub3_unidade_model->unidadeListarTodos();
		$data["view"]		= "cub3_perfil/perfilCadastrar";
		carregarTema($data);
	}

	/*
	* [Método]: perfilInserir
	* [Descrição]: Realiza a inserção de um perfil no banco de dados e dispara ações associadas
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function perfilInserir(){
		verificarStatusSessao();
		$post		= json_decode(file_get_contents('php://input'));

		//Cria Pergil
		$dados = array(
			"usuNome"		=> $post->usuNome,
			"usuSobrenome"	=> $post->usuSobrenome,
			"usuEmail"		=> $post->usuEmail,
			"usuStatus"		=> $post->usuStatus,
			"usuImagem"		=> 'default.jpg'
		);
		$this->load->model("padrao_model");
		$usuCodigo = $this->padrao_model->inserir("cub3_usuario",$dados);
		//Associa Grupos
		$dados = array(
				"gruCodigo" => $post->gruCodigo,
				"usuCodigo" => $usuCodigo
			);

		$this->padrao_model->inserir("cub3_grupo_has_usuario",$dados);
		//Enviar uma solicitação para criação de senha ao usuário
		$dadosReset = array(
			"usuNome"		=> $post->usuNome,
			"usuCodigo"		=> $usuCodigo,
			"usuEmail"		=> $post->usuEmail,
			"usuToken"		=> cifracub3($usuCodigo."_".date("Ymdhis"))
		);
		$this->perfilCriarSenha($dadosReset);
		$this->perfilEmailCadastrarSenha($dadosReset);

		die(json_encode(true));
	}
	/*
	* [Método]: perfilEditar
	* [Descrição]: Abre o formulário de edição de perfis
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	public function perfilEditar(){
		verificarStatusSessao();
		$usuCodigo = (decifracub3($this->input->get("usuCodigo")));

		$this->load->model("cub3_perfil_model");
		$dados = $this->cub3_perfil_model->perfilSelecionarCodigo($usuCodigo);

		$data["usuCodigo"]		= $dados->row()->usuCodigo;
		$data["usuNome"]		= $dados->row()->usuNome;
		$data["usuSobrenome"]	= $dados->row()->usuSobrenome;
		$data["usuStatus"]		= $dados->row()->usuStatus;
		$data["usuEmail"]		= $dados->row()->usuEmail;

		$data["view"]			= "cub3_perfil/perfilEditar";
		carregarTema($data);
	} 
	public function perfilAlterar(){
		verificarStatusSessao();
		$usuCodigo 		= $this->input->post("usuCodigo");
		$usuNome 		= $this->input->post("usuNome");
		$usuSobrenome 	= $this->input->post("usuSobrenome");
		$usuStatus 		= $this->input->post("usuStatus");
		$usuEmail 		= $this->input->post("usuEmail");

		$dados = array(
			"usuNome"		=> $usuNome,
			"usuSobrenome"	=> $usuSobrenome,
			"usuStatus"		=> $usuStatus,
			"usuEmail"		=> $usuEmail
		);

		$this->load->model("padrao_model");
		$codicao = array("usuCodigo" => $usuCodigo);
		$this->padrao_model->alterar("cub3_usuario",$dados,$codicao);


		$this->session->set_flashdata('msgStatus', mensagemstatus("sucesso","O usuário teve seus dados alterados com sucesso em nossa base de dados."));
		redirect(base_url("perfil"));
	}

	public function perfilExcluir(){
		verificarStatusSessao();
		$usuCodigo = $this->uri->segment(3);
		$usuCodigo = decifracub3($usuCodigo);
		
		$this->load->model("padrao_model");
		$codicao = array("usuCodigo" => $usuCodigo);

		if($this->padrao_model->excluir("cub3_usuario",$codicao))
			retornarJson(true);
		else
			retornarJson(false);
	}

	public function perfilAtualizarSessao()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		
		$this->session->set_userdata( $dados );

		die(json_encode(true));

	}

	private function perfilCriarSenha($dadosReset){
		$dados = array(
			"urtToken"		=> $dadosReset["usuToken"],
			"usuCodigo"		=> $dadosReset["usuCodigo"],
			"urtvalidade"	=> date("Y-m-d H:i:s", strtotime("+1 day")),
			"urtStatus"		=> "ATIVO"
		);

		$this->load->model("padrao_model");
		$this->padrao_model->inserir("cub3_usuarioresetsenha",$dados);
	}
	public function perfilRecuperarSenha(){
		$this->load->model("cub3_perfil_model");
		$usuEmail 	= urldecode(decifracub3($this->input->get("usuEmail")));
		$dados 		= $this->cub3_perfil_model->perfilSelecionarEmail($usuEmail);


		if($dados->num_rows() > 0):
			//Mensagem de reset de senha
			$dadosReset = array(
				"usuNome"		=> $dados->row()->usuNome,
				"usuCodigo"		=> $dados->row()->usuCodigo,
				"usuEmail"		=> $dados->row()->usuEmail,
				"usuToken"		=> cifracub3($dados->row()->usuCodigo."_".date("Ymdhis"))
			);
			//Cria registro de senha no histórico de recuperações de senha cub3_usuarioresetsenha
			$this->perfilCriarSenha($dadosReset);
			//Envia um e-mail com instruções ao usuário
			$this->perfilEmailCadastrarSenha($dadosReset); 
			$retorno = array("STATUS" => 1);
		else:
			$retorno = array("STATUS" => 2);
		endif;

		die(json_encode($retorno));
	}

	public function perfilCadastrarSenha(){
		$this->load->model("cub3_perfil_model");
		if(urldecode($this->input->get("token")) != '')
			$token 		= (urldecode($this->input->get("token")));
		else
			$token 		= (($this->uri->segment(2)));

		$dadosToken 	= $this->cub3_perfil_model->perfilSelecionarToken(($token)); 
		switch ($dadosToken->row()->urtStatus) {
			case 'Inativo':
					show_error("<p>Este token já foi utilizado ou expirou. Você deverá solicitar um novo cadastro de senha.", 503, $heading = 'Token Inválido');
				break;
			
			default:
					$data["view"] = "cub3_perfil/perfilNovaSenha";
					$data["exibirSidebar"]		= false;
					$data["exibirHeader"]		= false;
					$data["token"]				= $token;
					carregarTema($data);
				break;
		} 
	}
	public function perfilNovaSenhaInserir(){
		$this->load->model("cub3_perfil_model");
		$this->load->model("padrao_model");
		$this->load->library('encryption');	

		$usuSenha = $this->input->post("usuSenha");
		$usuToken = $this->input->post("usuToken");

		$dadosToken 	= $this->cub3_perfil_model->perfilSelecionarToken($usuToken);
		if(is_null($dadosToken->row())):
					show_error("<p>Este token já foi utilizado ou expirou. Você deverá solicitar um novo cadastro de senha.", 503, $heading = 'Token Inválido');
		else:
			$dadosUsuario	= $this->cub3_perfil_model->perfilSelecionarCodigo($dadosToken->row()->usuCodigo);
			//Altera o status do token utilizado
			$dados 		= array("urtStatus"	=> "INATIVO");
			$condicao 	= array("usuCodigo" => $dadosUsuario->row()->usuCodigo);
			$this->padrao_model->alterar("cub3_usuarioresetsenha",$dados,$condicao);
			//Altera a senha do usuário informado
			$dados 		= array("usuSenha"	=> $this->encryption->encrypt($usuSenha));
			$condicao 	= array("usuCodigo" => $dadosUsuario->row()->usuCodigo);
			if($this->padrao_model->alterar("cub3_usuario",$dados,$condicao)){
				$mensagem 	= cifracub3("Senha alterada com sucesso, por favor, realize o login abaixo:");
			}
			redirect(base_url()."login?mensagem=".$mensagem);			
		endif;

	}

	/*
	* [Método]: perfilEmailResetSenha
	* [Descrição]: Corpo do e-mail da solicitação para nova senha
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	private function perfilEmailResetSenha($dados){
		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["usuNome"].'</strong>! Você acaba de solicitar uma recuperação de senha em nosso sistema. Clique no botão abaixo para informar uma nova senha:<br /><br /></p>
							                        <p style="text-align:center"><a style="background-color: #fef37e; color: #30343F; text-decoration: none; text-transform: uppercase; padding: 10px; font-weight: bold;" href="'.base_url().'cadastrarSenha?token='.$dados["usuToken"].'">Cadastrar Nova Senha</a></p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["usuEmail"],
			'assunto'			=> 'Recuperação de Senha');

		return enviarEmail($conteudo, 'model-padrao');
	}
	/*
	* [Método]: perfilEnviarNovaSenha
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	private function perfilEnviarNovaSenha($dados){
		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá, <strong>'.$dados["usuNome"].'</strong>. Você provalmente teve problemas em receber o e-mail para cadastro de sua senha. Sendo assim, o administrador realizou um cadastro manual de sua senha. A nova senha gerada é: <strong style="color: #F44336">'.$dados["usuSenha"].'</strong>.</p>
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Você poderá acessar o sistema utilizando seu e-mail cadastrado e sua senha no endereço: '.base_url().'</p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["usuEmail"],
			'assunto'			=> 'Criação manual de senha');

		return enviarEmail($conteudo, 'model-padrao');
	}

	/*
	* [Método]: perfilEmailCadastrarSenha
	* [Descrição]: E-mail para cadastro de nova senha.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/controllers/Perfil.php
	* @param 
	* @return 
	*/
	private function perfilEmailCadastrarSenha($dados){
		$conteudo 				= array();
		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #37474F">Olá <strong>'.$dados["usuNome"].'</strong>! Seus dados foram cadastrados com sucesso no sistema de gestão do <strong>'. $this->config->item("portal_nome").'</strong>. Utilize o botão abaixo para criar uma senha de acesso:<br /><br /></p>
							                        <p style="text-align:center"><a style="background-color: #2b67ff;padding: 14px 28px 14px 28px;border-radius: 3px;line-height: 18px!important;letter-spacing: 0.125em;text-transform: uppercase;font-size: 13px;font-family: \'Open Sans\',Arial,sans-serif;font-weight: 400;color: #ffffff;text-decoration: none;display: inline-block;line-height: 18px!important;" href="'.base_url('criarsenha/?token='.$dados["usuToken"]).'">Criar minha senha de acesso</a><br /><br /></p>
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #37474F">Você poderá acessar o painel através do endereço <a style="color: #37474F">'.base_url().'</a> (salve o endereço em seus favoritos do navegador) ou clicando no botão abaixo:<br /><br /></p>
							                        <p style="text-align:center"><a style="background-color: #2b67ff;padding: 14px 28px 14px 28px;border-radius: 3px;line-height: 18px!important;letter-spacing: 0.125em;text-transform: uppercase;font-size: 13px;font-family: \'Open Sans\',Arial,sans-serif;font-weight: 400;color: #ffffff;text-decoration: none;display: inline-block;line-height: 18px!important;" href="'.base_url().'">Acessar o Sistema</a></p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["usuEmail"],
			'assunto'			=> 'Confirmação de Cadastro de Senha');

			return enviarEmail($conteudo, 'model-padrao');
	} 

}

/* End of file menu.php */
/* Location: ./application/controllers/menu.php */