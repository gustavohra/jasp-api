<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_menu extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper("cub3_menu_helper");
	}
	/*
	* [Método]: index
	* [Descrição]: Listagem de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$data["view"]	= "cub3_menu/menuListar";
		carregarTema($data);
	}
	/*
	* [Método]: getMenus
	* [Descrição]: Busca todos os menus cadastrados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getMenus()
	{
		$this->load->model("cub3_menu_model");
		$dados = $this->cub3_menu_model->menuListarTodos()->result_array();

		retornarJson(null, $dados);
	}
	/*
	* [Método]: getMenuPorCodigo
	* [Descrição]: Busca um menu por seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getMenuPorCodigo()
	{
		$this->load->model("cub3_menu_model");

		$menCodigo				= $this->input->get("menCodigo");

		$dados 					= $this->cub3_menu_model->menuSelecionarCodigo(decifracub3($menCodigo))->row_array();
		$dados["menusItens"] 	= $this->cub3_menu_model->menuSubmenuListarTodos($dados["menCodigo"])->result_array();
		retornarJson(null, $dados);
	}


	/*
	* [Método]: getIcones
	* [Descrição]: Busca os ícones do sistema
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getIcones()
	{
		$this->load->model("cub3_menu_model");
		$dados = $this->cub3_menu_model->icones();
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}
	/*
	* [Método]: menuInserir
	* [Descrição]: Inserção de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuInserir(){
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$post 			= json_decode(file_get_contents('php://input'), true); 
		$icone 			= $post["menIcone"]["value"];
		$subMenus 		= null;
		$post["menIcone"] = $icone;

		if(isset($post["menusItens"])){
			$subMenus 		= $post["menusItens"];
			unset($post["menusItens"]);			
		}

		if(!isset($post["menCodigo"])){
			if($this->padrao_model->inserir("cub3_menu", $post)){
				$menCodigo = $this->db->insert_id();

				if(!is_null($subMenus)){
					for ($i=0; $i < Count($subMenus) ; $i++) { 
						$menu 				= $subMenus[$i];
						$menu["menCodigo"] 	= $menCodigo;
						$menu["mitPosicao"] = $i;

						$this->padrao_model->inserir("cub3_menuitem",$menu); 
					}
				}
				retornarJson(true);
			}
			else
				retornarJson(false);
		}
		else {
			$condicao = array("menCodigo" => $post["menCodigo"]);
			if($this->padrao_model->alterar("cub3_menu",$post,$condicao)) {
				retornarJson(true);
			}
			else
				retornarJson(false);

		}

	}
		/*
	* [Método]: subMenuAlterar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_menu.php
	* @param 
	* @return 
	*/
	public function subMenuAlterar()
	{
		verificarStatusSessao();

			$this->load->model("padrao_model");
			$mitCodigo			 	= urldecode(decifracub3($this->input->get("mitCodigo")));
			$campo 					= urldecode(decifracub3($this->input->get("campo")));
			$valor 					= urldecode(($this->input->get("valor")));

			$dados[$campo] 			= $valor;
			$condicao 				= array("mitCodigo" => $mitCodigo);

			if($this->padrao_model->alterar("cub3_menuitem",$dados,$condicao))
				retornarJson(true);
			else
				retornarJson(false);
	}
	/*
	* [Método]: subMenuInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_menu.php
	* @param 
	* @return 
	*/
	public function subMenuInserir()
	{
		verificarStatusSessao();
				$this->load->model("padrao_model");
			$post 			= json_decode(file_get_contents('php://input'), true);

			if($this->padrao_model->inserir("cub3_menuitem",$post))
				retornarJson(true, array('mitCodigo' => $this->db->insert_id()));
			else
				retornarJson(false);
	}


	/*
	* [Método]: menuCadastrar
	* [Descrição]: Cadastro de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuCadastrar(){
		verificarStatusSessao();
		$data["view"]	= "cub3_menu/menu";
		carregarTema($data);
	}

	/*
	* [Método]: menuEditar
	* [Descrição]: Edição de menu
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuEditar(){
		verificarStatusSessao();

		$data["view"]					= "cub3_menu/menu";
		$data["menCodigoCifrado"]		= $this->input->get("menCodigo"); 
		carregarTema($data);
	}
	/*
	* [Método]: menuAlterar
	* [Descrição]: Altera os dados no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuAlterar(){
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$post	= json_decode(file_get_contents('php://input'));
		$dados = array(
			"menDescricao"	=> $post->menDescricao,
			"menStatus"		=> $post->menStatus,
			"menIcone"		=> $post->menIcone
		);

		$codicao = array("menCodigo" => $post->menCodigo);
		die(json_encode($this->padrao_model->alterar("cub3_menu",$dados,$codicao)));
	}

	/*
	* [Método]: menuExcluir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuExcluir(){
		verificarStatusSessao();
		$menCodigo = decifracub3($this->input->get("menCodigo"));
		
		$this->load->model("padrao_model");
		$codicao = array("menCodigo" => $menCodigo);
		$this->padrao_model->excluir("cub3_grupo_has_menu",$codicao);
		$this->padrao_model->excluir("cub3_menuitem",$codicao);
		
		if($this->padrao_model->excluir("cub3_menu",$codicao))
			retornarJson(true);
		else
			retornarJson(false);
		
	}
	/*
	* [Método]: menuSubmenu
	* [Descrição]: Lista os submenus de acordo com um código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenu(){
		verificarStatusSessao();
		$data["view"]						= "cub3_menu/menuSubmenuListar";
		$data["menCodigoCifrado"] 			= $this->input->get("menCodigo");
		carregarTema($data);
	}
	/*
	* [Método]: getSubMenus
	* [Descrição]: Busca todos os submenus de um menu
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getSubMenus()
	{
		verificarStatusSessao();
		$this->load->model("cub3_menu_model");

		$menCodigo 	= decifracub3($this->input->get("menCodigo")); 
		$dados 		= $this->cub3_menu_model->menuSubmenuListarTodos($menCodigo);
		$retorno 	= array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}
	/*
	* [Método]: getSubmenuMenuPorCodigo
	* [Descrição]: Busca um determinado submenu por seu código.
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getSubmenuMenuPorCodigo()
	{
		verificarStatusSessao();
		$this->load->model("cub3_menu_model");

		$mitCodigo 	= decifracub3($this->input->get("mitCodigo")); 

		$dados 		= $this->cub3_menu_model->menuSubmenuSelecionarCodigo($mitCodigo);
		$retorno 	= array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}


	/*
	* [Método]: menuSubmenuCadastrar
	* [Descrição]: Cadastro de submenus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenuCadastrar(){
		verificarStatusSessao();
		$data["menCodigoCifrado"]			= $this->input->get("menCodigo");
		$data["view"]				= "cub3_menu/menuSubmenuCadastrar";
		carregarTema($data);
	}
	/*
	* [Método]: menuSubmenuInserir
	* [Descrição]: Insere um submenu em uma linha de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenuInserir(){
		verificarStatusSessao();
		$post	= json_decode(file_get_contents('php://input'));
		$dados = array(
			"menCodigo"		=> decifracub3($post->menCodigoCifrado),
			"mitDescricao"	=> $post->mitDescricao,
			"mitLink"		=> $post->mitLink,
			"mitIcone"		=> $post->mitIcone,
			"mitStatus"		=> $post->mitStatus
		);

		$this->load->model("padrao_model");
		die(json_encode($this->padrao_model->inserir("cub3_menuitem",$dados)));
	}
	/*
	* [Método]: menuSubmenuExcluir
	* [Descrição]: Exclui um submenu e suas associações
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenuExcluir(){
		verificarStatusSessao();
		$mitCodigo =  decifracub3($this->input->get("mitCodigo"));
		
		$this->load->model("padrao_model");
		$this->load->model("cub3_menu_model");
		
		$menCodigo = $this->cub3_menu_model->menuSubmenuSelecionarCodigo($mitCodigo)->row()->menCodigo;
		$codicao = array("mitCodigo" => $mitCodigo);
		$this->padrao_model->excluir("cub3_menuitem",$codicao);
		redirect(base_url("menu/menuSubmenu/?menCodigo=".cifracub3($menCodigo)));
	}
	/*
	* [Método]: menuSubmenuEditar
	* [Descrição]: Abre o formulário de edição de um submenu
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenuEditar(){
		verificarStatusSessao();
		$data["mitCodigo"]		=  decifracub3($this->input->get("mitCodigo"));
		$data["menCodigo"]		=  decifracub3($this->input->get("menCodigo"));
		$data["mitIcone"]		=  decifracub3($this->input->get("mitIcone"));
		$data["view"]			= "cub3_menu/menuSubmenuEditar";
		carregarTema($data);
	}
	/*
	* [Método]: menuSubmenuAlterar
	* [Descrição]: Altera os dados de um submenu
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function menuSubmenuAlterar(){
		verificarStatusSessao();
		$post	= json_decode(file_get_contents('php://input'));
		$this->load->model("padrao_model");
		$this->load->model("cub3_menu_model");

		$dados = array(
			"mitDescricao"	=> $post->mitDescricao,
			"mitStatus"		=> $post->mitStatus,
			"mitIcone"		=> $post->mitIcone,
			"mitLink"		=> $post->mitLink
		);

		$condicao = array("mitCodigo" => $post->mitCodigo);
		die(json_encode($this->padrao_model->alterar("cub3_menuitem",$dados,$condicao)));
	}

		/*
	* [Método]: montarMenuJson
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_menu.php
	* @param 
	* @return 
	*/
	public function montarMenuJson()
	{
		if(verificarSessao()) {
			$recriarMenu = $this->input->get("recriar_menu");
			
			retornarJson(null, montarMenuJson($recriarMenu));			
		}
		else
			retornarJson(null);
	}



}

/* End of file menu.php */
/* Location: ./application/controllers/menu.php */