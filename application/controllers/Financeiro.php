<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 28/12/2016 às 17:44:16
	*
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Financeiro.php
	*/
class Financeiro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("financeiro_model");
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/controllers/Financeiro.php
	* @param 
	* @return 
	*/
	public function getDescontoPorCodigo()
	{
		$post 	= json_decode(file_get_contents('php://input'), true);
		$codigo = urldecode(decifracub3($this->input->get('desCodigo')));

		$dados 	= $this->financeiro_model->descontoPorCodigo($codigo);
		retornarJson(null, $dados->row_array()); 
	}
}
