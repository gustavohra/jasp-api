<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 26/05/2016 às 17:17:25
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_timeline extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		verificarStatusSessao();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{

	}
	/*
	* [Método]: getTimelineJson
	* [Descrição]:
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package
	* @param 
	* @return 
	*/
	public function getTimelineJson()
	{
		$this->load->model("cub3_timeline_model"); 
 
		retornarJson(null, $this->cub3_timeline_model->timelineListar()->result_array()); 
	}
	/*
	* [Método]: getTimelineComentariosJson
	* [Descrição]: 
	*/
	public function getTimelineComentariosJson()
	{
		$this->load->model("cub3_timeline_model"); 
 		$timCodigo 			= urldecode(decifracub3($this->input->get("timCodigo")));
		retornarJson(null, $this->cub3_timeline_model->timelineListarComentarios($timCodigo)->result_array()); 
	}
	/*
	* [Método]: timelineInserir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/controllers/Propostas.php
	* @param 
	* @return 
	*/
	public function timelineInserir()
	{
		verificarAdministrador();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados 										= array();
		$dados 										= json_decode(file_get_contents('php://input'),true);
		 
		$dados["usuCodigo"]						= $this->session->userdata('s_usuCodigo');
 
			if($this->padrao_model->inserir('cub3_timeline', $dados) > 0){
				$resposta = array('mensagem' => 'Postagem realizada com sucesso!');
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			}  
	} 
		/*
	* [Método]: excluir 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	public function excluir ($campoIdentificador = 'timCodigo', $campoStatus = 'timStatus', $tabela = 'cub3_timeline')
	{
		$this->load->model('padrao_model');
		$dados 								= array(); 
		$dados[$campoStatus]	 			= 'EXCLUIDO';
		$dados[$campoIdentificador]			= urldecode(decifracub3($this->input->get($campoIdentificador)));
		$mensagemRetorno 					= 'Postagem excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da postagem.';
  
		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->excluir($tabela, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}
	/*
	* [Método]: comentar 
	* [Descrição]: Insere ou edita um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador e $tabela
	* [Comentários]: Caso o código identificador seja passado, ele realiza a alteração e caso contrário, a inserção.
	*/
	public function comentar ($campoIdentificador = 'comCodigo', $tabela = 'cub3_timeline_comentario')
	{
		$this->load->model('padrao_model');
		$dados 					= json_decode(file_get_contents('php://input'), true); 
		$dados["usuCodigo"]		= $this->session->userdata('s_usuCodigo');
		if(!isset($dados[$campoIdentificador])){
			$mensagemRetorno 		= 'Comentário inserido com sucesso!';
			if($this->padrao_model->inserir($tabela, $dados) > 0)
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno, "comentario" => $dados));
			else
				retornarJson(false);	
		}	
		else {		
			$mensagemRetorno 		= 'Comentário alterado com sucesso!';
			$condicao 				= array($campoIdentificador => $dados[$campoIdentificador]);
			if($this->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno, "comentario" => $dados)); 
			else
				retornarJson(false);
		}	
	}

}
