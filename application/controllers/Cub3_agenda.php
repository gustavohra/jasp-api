<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 06/05/2016 às 11:10:23
	*
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsdbh55/application/controllers/Regionais.php
	*/
class Cub3_agenda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: index
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsdbh55/application/controllers/Regionais.php
	* @param 
	* @return 
	*/
	public function index()
	{
		$data["view"]	= "cub3_agenda/agendaListar";
		carregarTema($data);
	}
	/*
	* [Método]: getAgenda
	* [Descrição]: Busca todos os menus cadastrados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getAgenda()
	{
		$this->load->model("cub3_agenda_model");
		$dados = $this->cub3_agenda_model->agenda();
		$retorno = array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->result());
		die(json_encode($retorno));
	}
	/*
	* [Método]: getAgenda
	* [Descrição]: Busca todos os menus cadastrados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getAgendaJson()
	{
		$this->load->model("cub3_agenda_model");
		$ageCodigo 			= urldecode(decifracub3($this->input->get("ageCodigo")));
		$dados = $this->cub3_agenda_model->agendaEventoPorCodigo($ageCodigo); 
		die(json_encode($dados->row_array()));
	}
	/*
	* [Método]: getAgendaPorCodigo
	* [Descrição]: Busca um menu por seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function getAgendaPorCodigo()
	{
		$this->load->model("cub3_agenda_model");

		$post		= json_decode(file_get_contents('php://input'));
		$dados 		= $this->cub3_agenda_model->agendaPorCodigo(decifracub3($post->ageCodigo));
		$retorno	= array("count"	=>	$dados->num_rows(),	"data"	=>	$dados->row());
		die(json_encode($retorno));
	}
	/*
	* [Método]: agendaInserir
	* [Descrição]: Inserção de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function agendaInserir(){
		$dados 			= json_decode(file_get_contents('php://input'), true);
 
		$this->load->model("padrao_model"); 
		if(isset($dados["horario"]))
			unset($dados["horario"]);
		
		if(!isset($dados["ageCodigo"])){

			if($this->padrao_model->inserir('cam_agenda', $dados) > 0){
				$resposta = array('mensagem' => 'Cadastro realizado com sucesso!' );
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}
		else{
			$condicao = array('ageCodigo' => $dados["ageCodigo"] );

			if($this->padrao_model->alterar('cam_agenda', $dados, $condicao) > 0){
				$resposta = array('mensagem' => 'Edição realizada com sucesso!');
				retornarJson(true, $resposta);
			}
			else{
				$resposta = array('mensagem' => 'Por favor, tente novamente mais tarde.' );
				retornarJson(false, $resposta);
			} 
		}
	}
	/*
	* [Método]: agendaCadastrar
	* [Descrição]: Cadastro de menus
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function agendaCadastrar(){
		verificarStatusSessao();

		$dados["titulo"] 	= "Cadastrar compromisso";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-calendar"></i> Agenda', 'agenda');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> '.$dados["titulo"], 'agendaCadastrar');
 
		$dados["formularioSlug"]	= "cadastro-agenda";
		$dados["controller"]	= "AgendaCtrl";
		$dados["view"]			= "cub3_agenda/agendaCadastrar";
		
		carregarTema($dados); 
	}

	/*
	* [Método]: agendaEditar
	* [Descrição]: Edição de menu
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function agendaEditar(){
		verificarStatusSessao();
		$dados["titulo"] 				= "Editar compromisso";
		$dados["ageCodigo"] 			= urldecode(decifracub3($this->input->get("ageCodigo")));
		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-calendar"></i> Agenda', 'agenda');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-edit-o"></i> '.$dados["titulo"], 'agendaCadastrar');
 
		$dados["formularioSlug"]	= "cadastro-agenda";
		$dados["controller"]	= "AgendaCtrl";
		$dados["view"]			= "cub3_agenda/agendaCadastrar";
		
		carregarTema($dados); 
	}
	/*
	* [Método]: agendaAlterar
	* [Descrição]: Altera os dados no banco de dados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function agendaAlterar(){
		verificarStatusSessao();
		$this->load->model("padrao_model");
		$dados	= json_decode(file_get_contents('php://input')); 
		$codicao = array("ageCodigo" => $dadosage["ageCodigo"]);
		die(json_encode($this->padrao_model->alterar("cam_agenda",$dados,$codicao)));
	}

	/*
	* [Método]: agendaExcluir
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/controllers/Menu.php
	* @param 
	* @return 
	*/
	public function agendaExcluir(){
		verificarStatusSessao(); 
		$ageCodigo = $this->uri->segment(3);
		$ageCodigo = decifracub3($ageCodigo);
		
		$this->load->model("padrao_model");

		$dados = array(
			"canStatus"	=> "EXCLUIDO"
		);

		$codicao = array("ageCodigo" => $post->ageCodigo);
		$this->padrao_model->alterar("cam_agenda",$dados,$codicao);


		redirect(base_url("agenda"));
	}	
}
