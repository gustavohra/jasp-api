<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Classe referente ao gerenciamento de páginas e seus conteúdos
	* [Criação]: 28/04/2016 às 23:52:50
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_paginas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("cub3_pagina_model");
	}

	/*
	* [Método]: index
	* [Descrição]: Listagem de páginas no escopo administrativo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Páginas";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Página / '.$dados["titulo"], 'paginas');

		$dados["view"]	= "cub3_paginas/paginasListar";
		carregarTema($dados);
	}
		/*
	* [Método]: visualizar
	* [Descrição]: Método público para visualizar uma página 
	*
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_paginas.php
	* @param 
	* @return 
	*/
	public function visualizar()
	{
		$pagSlug 			= $this->uri->segment(3); 
		$dadosPagina 		= $this->cub3_pagina_model->paginaListarPorSlug($pagSlug)->row_array();

		if($dadosPagina != null){
			$dados["tema"]		= $dadosPagina["pagTemplate"];
			$dados["titulo"]	= $dadosPagina["pagTitulo"];
			$dados["conteudo"]	= $dadosPagina["pagConteudo"];
			$dados["pagCss"]	= $dadosPagina["pagCss"];
			$dados["pagImagem"]	= $dadosPagina["pagImagem"];
			$dados["view"]		= "cub3_paginas/paginaVisualizar";
			$dados["widgets"]	= json_decode($dadosPagina["pagEstrutura"]);	
		}
		else
			$dados["view"]		= "cub3_erros/erro404";
		  
		carregarTema($dados);
	}


	/*
	* [Método]: paginaCadastrar
	* [Descrição]: Exibe o formulário de cadastro de uma página
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaCadastrar()
	{
		verificarStatusSessao();
		$dados["titulo"] 	= "Cadastrar página";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Página / Páginas', 'paginas');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Página / '.$dados["titulo"], 'paginaCadastrar');

		$dados["paginaSubmit"]	= "paginaCadastrar(paginaDados)";
		$dados["controller"]	= "Cub3PaginasCtrl";
		$dados["idFormulario"]	= "paginas-cadastro";
		$dados["view"]			= "cub3_paginas/pagina";
		
		carregarTema($dados);

	}
	/*
	* [Método]: paginaEditar
	* [Descrição]: Exibe o formulário de edição de uma página
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaEditar()
	{
		verificarStatusSessao();
		

		$dados["titulo"] 	= "Editar página";
		$pagCodigo 			= urldecode(decifracub3($this->input->get("pagCodigo")));

		$pagina 			= $this->cub3_pagina_model->paginaListarPorCodigo($pagCodigo)->row_array(); 

		if($pagina != null):
			// Breadcrumbs
			$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-globe"></i> Página / Páginas', 'paginas');
			$this->breadcrumbs->push('<i class="fa fa-lg fa fa-plus"></i> Página / '.$dados["titulo"], 'paginaEditar');

			$dados["paginaSubmit"]	= "paginaEditar(paginaDados)";
			$dados["pagCodigo"]		= cifracub3($pagina["pagCodigo"]);
			$dados["controller"]	= "Cub3PaginasCtrl";
			$dados["idFormulario"]	= "paginas-cadastro";
			$dados["view"]			= "cub3_paginas/pagina";
			carregarTema($dados);
		else:
           redirect('erro404');			
       	endif;
	}


	/*
	* [Método]: getPaginasJson
	* [Descrição]: Recebe o código de um conteúdo por meio GET e retorna em formato JSON
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function getPaginasJson()
	{
		
 
		$dadosPagina 		= $this->cub3_pagina_model->paginaListar();
		$retorno		 	= $dadosPagina->result_array();

		// Caso o retorno seja inválido, exibe página não encontrada
		if(is_null($retorno))
           redirect('erro404');
       else
			retornarJson(null, $retorno);
	}

	/*
	* [Método]: getPaginaJson
	* [Descrição]: Recebe o código de um conteúdo por meio GET e retorna em formato JSON
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function getPaginaJson()
	{
		$pagCodigo 			= urldecode(decifracub3($this->input->get("pagCodigo")));
		$dadosPagina 		= $this->cub3_pagina_model->paginaListarPorCodigo($pagCodigo);

		$retorno		 	= $dadosPagina->row_array();

		// Caso o retorno seja inválido, exibe página não encontrada
		if(is_null($retorno))
           redirect('erro404');
       else
			retornarJson(null, $retorno);
	}
		/*
	* [Método]: getPaginaPorSlugJson
	* [Descrição]: Recebe o slug (URL amigável) de um conteúdo por meio GET e retorna em formato JSON
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function getPaginaPorSlugJson()
	{
		

		$pagSlug 			= urldecode(decifracub3($this->input->get("pagSlug")));
		$dadosPagina 		= $this->cub3_pagina_model->paginaListarPorSlug($pagSlug);

		$retorno		 	= $dadosPagina->row_array();

		// Caso o retorno seja inválido, exibe página não encontrada
		if(is_null($retorno))
           redirect('erro404');
       else 
			retornarJson(null, $retorno);
	}
 

	/*
	* [Método]: paginaInserir
	* [Descrição]: Recebe um post via JSON e insere no banco
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaInserir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		$dados["usuCodigo"] = $this->session->userdata('s_usuCodigo');

		if(isset($dados["anexo"])){
			$dados["pagImagem"] = $dados["anexo"];
			unset($dados["anexo"]);
		}

		// Verifica se o slug é válido
		$dados["pagSlug"] 		= $this->paginaValidarSlug($dados["pagSlug"]);

		if($this->padrao_model->inserir('cub3_pagina', $dados) > 0)
			retornarJson(true);
		else
			retornarJson(false);
	}

		/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaValidarSlug($pagSlug = '$pagSlug')
	{
		
		if($pagSlug != ''){
			$verificarSlug 		= $this->cub3_pagina_model->paginaListarPorSlug($pagSlug);

			if(Count($verificarSlug) > 0)
				return $pagSlug;
			else {
				// Caso o slug já exista, acrescenta um valor de timestamp unix
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $pagSlug."-".$unix;
			}
		}
		else{
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $unix;
		}
	}


	/*
	* [Método]: paginaAlterarJson
	* [Descrição]: Recebe um post via JSON e realiza a alteração do conteúdo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaAlterar()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$dados = array();
		$dados = json_decode(file_get_contents('php://input'),true);

		$condicao = array("pagCodigo" => $dados["pagCodigo"]);

		if(isset($dados["anexo"])){
			$dados["pagImagem"] = $dados["anexo"]; 
		}

		unset($dados["anexo"]);

		if($this->padrao_model->alterar('cub3_pagina', $dados, $condicao))
			retornarJson(true);
		else	
			retornarJson(false);
	}
	/*
	* [Método]: paginaExcluir
	* [Descrição]:  Recebe o código e realiza a exclusão do conteúdo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/controllers/Paginas.php
	* @param 
	* @return 
	*/
	public function paginaExcluir()
	{
		verificarStatusSessao();
		header('Content-type: application/json');

		$this->load->model("padrao_model");
		$pagCodigo 		= urldecode(decifracub3($this->input->get("pagCodigo")));

		$dados["pagStatus"] = 'EXCLUIDO';

		$condicao = array("pagCodigo" => $pagCodigo);

		if($this->padrao_model->alterar('cub3_pagina', $dados, $condicao))
			retornarJson(true);
		else	
			retornarJson(false);		
	}



}
