<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 29/07/2016 às 19:08:17
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_painel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{
		verificarStatusSessao();
		$dados = array();

		$dados["titulo"]			=	"Painel de controle";
		$dados["descricao"]			=	"Área destinada ao gerenciamento de todo o sistema <strong>".$this->config->item("template_tituloSistema")."</strong>";		
		$dados["view"]				= 	"admin/dashboard";
		$dados["estatisticas"]		= cifracub3(json_encode(null));
		carregarTema($dados);
	}
		/*
	* [Método]: verificarSessao
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/controllers/Cub3_painel.php
	* @param 
	* @return 
	*/
	public function verificarSessao()
	{ 
		if($this->session->userdata('s_usuCodigo') != null || $this->session->userdata('s_proCodigo') ){
			
			retornarJson(null, array('resposta' => true ));
		}
		else
			retornarJson(null, array('resposta' => false ));
	}


}
