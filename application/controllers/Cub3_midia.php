<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 14/08/2016 às 22:01:22
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_midia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package None
	* @param 
	* @return 
	*/
	public function index()
	{

	}
		/*
	* [Método]: imagens
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_midia.php
	* @param 
	* @return 
	*/
	public function imagens()
	{

		$dados["titulo"]		=	"Galeria de imagens";
		$dados["descricao"]		=	"Área destinada à listagem de imagens adicionadas ao site e sistema.";		
		$dados["view"]			= 	"cub3_midia/listagem";

		$dados["titulo"] 		= "Galerias";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-camera-retro-o"></i> Galeria de imagens', 'cub3_midia/imagens'); 

		carregarTema($dados);
	}
		/*
	* [Método]: criarGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_midia.php
	* @param 
	* @return 
	*/
	public function criarGaleria()
	{

		$dados["titulo"]		=	"Criação de Galeria de imagens";
		$dados["descricao"]		=	"Área destinada ao gerenciamento de uma galeria do sistema.";		
		$dados["view"]			= 	"cub3_midia/galeria";
		$dados["galCodigo"] 	= 	null;

		$dados["titulo"] 		= "Galeria";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-camera-retro-o"></i> Mídia', 'cub3_midia/imagens');
		$this->breadcrumbs->push($dados["titulo"], 'criarGaleria');

		carregarTema($dados);

	}
		/*
	* [Método]: criarGaleria
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/controllers/Cub3_midia.php
	* @param 
	* @return 
	*/
	public function editarGaleria()
	{

		$dados["titulo"]		=	"Edição de Galeria de imagens";
		$dados["descricao"]		=	"Área destinada ao gerenciamento de uma galeria do sistema.";		
		$dados["view"]			= 	"cub3_midia/galeria";

		$dados["titulo"] 		= "Galeria";

		// Breadcrumbs
		$this->breadcrumbs->push('<i class="fa fa-lg fa-home"></i> Home', '#');
		$this->breadcrumbs->push('<i class="fa fa-lg fa fa-camera-retro-o"></i> Mídia', 'cub3_midia/imagens');
		$this->breadcrumbs->push($dados["titulo"], 'editarGaleria');

		$galCodigo 				= (decifracub3($this->input->get("galCodigo")));

		if($galCodigo != ""){
			$dados["galCodigo"] = ($galCodigo);
			carregarTema($dados);
		}
		else
			redirect("erro404");
	}



}
