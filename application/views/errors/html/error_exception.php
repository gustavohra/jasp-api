 
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Ocorreu um erro interno</title>
<link href='https://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/flat-ui.css">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/estilos.css">  
<style type="text/css">
	ul.social {width: auto;display: inline-block;margin: auto}
	ul.social li {float: left;display:table-cell;vertical-align: middle;text-align: center;margin-right: 20px}
	p {font-size: 11px !important}
</style>
</head>
<body style="min-height:100%"> 
			<div class="conteudoInterno" style="display: table;  overflow: hidden;width:100%">  
					     <div style="display: table-cell; vertical-align: middle; text-align:center">
						       	<div style="text-align:center;width:90%;background-color:#fff;margin:auto;display:block;padding:10px;border-bottom:4px solid #eee;border-right:4px solid #eee">
									<section class="content"> 
										<h3><img ng-src="http://cub3.com.br/assets/img/logo.png" style="max-height: 3em" /></h3> 
										<h4>Ocorreu um erro interno / <b>PHP</b></h4>
										<blockquote >
											<small>
												Entre em contato com o <a href="mailto:contato@cub3.com.br" style="font-weight:600">suporte</a>.
												<?php if(ENVIRONMENT == 'development'): ?>
													<br /><br />
														<blockquote style="max-width:100%;overflow:auto;font-size:11px !important;text-align:left;border-color:#8d3741">
															<h4>Ocorreu um erro de exceção</h4>

															<p><b>Tipo:</b> <?php echo get_class($exception); ?></p>
															<p><b>Mensagem:</b>  <?php echo $message; ?></p>
															<p><b>Arquivo:</b> <?php echo $exception->getFile(); ?></p>
															<p><b>Linha:</b> <?php echo $exception->getLine(); ?></p>

															<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

																<p><b>Backtrace:</b></p>
																<?php foreach ($exception->getTrace() as $error): ?>

																	<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

																		<p style="margin-left:10px">
																		<b>Arquivo:</b> <?php echo $error['file'] ?><br />
																		<b>Line:</b> <?php echo $error['line'] ?><br />
																		<b>Function:</b> <?php echo $error['function'] ?>
																		</p>

																	<?php endif ?>

																<?php endforeach ?>

															<?php endif ?>
														</blockquote>
												<?php endif; ?>
											</small>
										</blockquote> 
									</section>
									<div class="row" style=" vertical-align: middle; text-align:center">
										<ul class="social">
											<li title="Portal CUB3">
												<a href="http://cub3.com.br"><i class="fa fa-globe"></i></a>
											</li>
											<li title="E-mail">
												<a href="mailto:contato@cub3.com.br"><i class="fa fa-envelope-o"></i></a>
											</li>
											<li title="Facebook">
												<a href="https://www.facebook.com/CUB3.Inteligencia.WEB"><i class="fa fa-facebook"></i></a>
											</li>
											<li title="Instagram">
												<a href="http://instagram.com/cub3web"><i class="fa fa-instagram"></i></a>
											</li>
										</ul>
									</div>
									</div> 
							</div>  
		</div>
</body>
</html>
 
		 