<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Página não encontrada</title>
<link href='https://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/flat-ui.css">
<link rel="stylesheet" type="text/css" href="https://cub3.com.br/lib/temas/flat/css/estilos.css">  
<style type="text/css">
	ul.social {width: auto;display: inline-block;margin: auto}
	ul.social li {float: left;display:table-cell;vertical-align: middle;text-align: center;margin-right: 20px}
</style>
</head>
<body style="overflow:hidden;min-height:100%"> 
			<div class="conteudoInterno" style="display: table; height: 100%; overflow: hidden;width:100%">  
			<a href="javascript:history.go(-1)" style="position:fixed;top:10px" title="Voltar"><i class="fa fa-2x fa-chevron-left" aria-hidden="true"></i></a>
					     <div style="display: table-cell; vertical-align: middle; text-align:center">
						       	<div style="text-align:center;width:32em;background-color:#fff;margin:auto;display:block;padding:10px;border-bottom:4px solid #eee;border-right:4px solid #eee">
									<section class="content"> 
										<h2><img ng-src="http://cub3.com.br/assets/img/logo.png" style="max-height: 3em" /></h2> 
										<h1>Página não encontrada / <b>404</b></h1>
										<blockquote>
											<small>
												A página solicitada não foi encontrada. Por favor, verifique o endereço digitado ou entre em contato com o <a href="mailto:contato@cub3.com.br" style="font-weight:600">suporte</a>, ou <a href="javascript:history.go(-1)" style="font-weight:600">clique aqui</a> para voltar.	
											</small>
										</blockquote> 
									</section>
									<div class="row" style=" vertical-align: middle; text-align:center">
										<ul class="social">
											<li title="Portal CUB3">
												<a href="http://cub3.com.br"><i class="fa fa-globe"></i></a>
											</li>
											<li title="E-mail">
												<a href="mailto:contato@cub3.com.br"><i class="fa fa-envelope-o"></i></a>
											</li>
											<li title="Facebook">
												<a href="https://www.facebook.com/CUB3.Inteligencia.WEB"><i class="fa fa-facebook"></i></a>
											</li>
											<li title="Instagram">
												<a href="http://instagram.com/cub3web"><i class="fa fa-instagram"></i></a>
											</li>
										</ul>
									</div>
									</div> 
							</div>  
		</div>
</body>
</html>