<?php

function solicitarCodigo($dados) {

		$CI =& get_instance();
		$CI->load->model("cub3_usuario_model");
		$CI->load->model("padrao_model");
		$CI->load->helper("cub3_email_helper");
   
		$dados = json_decode($dados, true); 
		$usuario 							= $CI->cub3_usuario_model->usuarioPorEmail($dados["email"])->row_array();
 
		if($usuario != null){
			$codigo 						= $usuario['usuCodigo'];
			$nome 							= $usuario['usuNome'];
			$email 							= $usuario['usuEmail'];
			$data 							= new DateTime();
			$dataValidade 					= new DateTime();
			$dataValidade->modify('+1 day');

			$tokenSenha 					= substr(mt_rand(100000,999999), 0, 6);
			$dadosReset 					= array('urtToken' => $tokenSenha, 'urtValidade' => $dataValidade->format('Y-m-d H:i:s'), 'id' => $codigo );
			$condicaoReset 					= array('id' => $codigo);
			$condicaoResetDados 			= array('urtStatus' => 'Inativo'); 
			$CI->padrao_model->alterar('reset_senha', $condicaoResetDados, $condicaoReset);

				if($CI->padrao_model->inserir('reset_senha', $dadosReset)){

					$html 		= "<center>
									<p>Uma solicitação de nova senha foi realizada para o seu login. Caso não tenha sido você, por favor, desconsidere este e-mail.</p>
									<p>O código de confirmação é:</p>
									<p><span style=\"margin-top:15px;width:200px;padding:10px 50px;text-align:center;background:none;background-color:#00a399;border:0px;font-weight:bold;color:#fff;\">{$tokenSenha}</span></p>
									</center>";
					$conteudo 	= array(
								'pacNome'			=> $nome,  
								'token' 			=> $tokenSenha,
								'resumo' 			=> 'Alteração de senha no JASP',
								'destinatario'		=> $email,
								'mensagem' 			=> $html,
								'assunto'			=> 'Alteração de senha no JASP');


					if(enviarEmail($conteudo ))
						retornarJson(null,  array('resposta' => true));
					else
						retornarJson(false);
				}
				else
					retornarJson(false); 
		}
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "E-mail não cadastrado!" ));
}


	function validarCodigo($dados) {

		$CI =& get_instance();
		$CI->load->model("cub3_usuario_model");
		$CI->load->model("padrao_model");
		$CI->load->library('encryption');	


		$dados = json_decode($dados,true);

		if($dados['codigo'] != null) {
			$dadosToken 	= $CI->cub3_usuario_model->usuarioValidarToken($dados['codigo'], $dados['email'])->result_array(); 
			if($dadosToken != null) { 
				if($dadosToken != null) {
					$dadosAlteracao 			= array();
					$dadosAlteracao["usuSenha"] = $CI->encryption->encrypt($dados['novaSenha']);

					$condicao 					= array('usuEmail' => $dados["email"]);
					if($CI->padrao_model->alterar("cub3_usuario", $dadosAlteracao, $condicao)) {
						retornarJson(null, array('resposta' => true));
					}
				}
				else
					retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}

