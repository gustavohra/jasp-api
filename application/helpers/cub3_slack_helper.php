<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * postarMensagem
 *
 * Posta uma mensagem em um canal
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('postarMensagemSlack'))
{
	function postarMensagemSlack($token, $canal, $texto, $nomeUsuario, $anexo, $urlImagem = 'http://cub3.com.br/assets/img/favicon.png')
	{
		$optional_headers = null;

		$urlApi = "https://slack.com/api/chat.postMessage?token=".$token."&channel=".$canal."&username=".$nomeUsuario."&text=".urlencode($texto)."&pretty=1&attachments=".urlencode(json_encode($anexo))."&icon_url=".urlencode($urlImagem);

		$params = array('http' => array(
		'method' => 'POST'
		));
		if ($optional_headers !== null) {
		$params['http']['header'] = $optional_headers;
		}
		$ctx = stream_context_create($params);
		$fp = @fopen($urlApi, 'rb', false, $ctx);
		if (!$fp) {
		return "Ocorreu um erro: Problem with $urlApi";
		}
		$response = @stream_get_contents($fp);
		if ($response === false) {
		return "Ocorreu um erro: Problem reading data from $urlApi";
		}

		return $response;
	}	
}