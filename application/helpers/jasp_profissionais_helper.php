<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function listarProfissionais()
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$profissionais 		= array();
 
		$tipo = $CI->input->post("tipo"); 
		$profissionais 		= $CI->profissionais_model->listarProfissionais()->result_array(); 
 
			retornarJson(null, $profissionais);  
	}
	function calcularDistancia($lat1, $lon1, $lat2, $lon2){
	        $R 		= 6371;
	        $dLat 	= toRad($lat2-$lat1);
	        $dLon 	= toRad($lon2-$lon1);
	        $lat1 	= toRad($lat1);
	        $lat2 	= toRad($lat2);

	        $a 		= sin($dLat/2) * sin($dLat/2) +sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2); 
	        $c 		= 2 * atan2(sqrt($a), sqrt(1-$a)); 
	        $d 		= $R * $c;
			return $d;
	}
	function confirmarRota() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$idEntrega = $CI->input->post('id');
 		$dados = array(
 			'status' => 'ATIVO'
 		);

		$condicao 							= array('id' => $idEntrega);
		
		if($CI->padrao_model->alterar("entrega", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Entrega iniciada com sucesso!")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a atualização, tente novamente."));



	}
	function encerrarEntrega() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$idEntrega = $CI->input->post('id');
 		$dados = array(
 			'status' => 'FINALIZADA',
 			'horarioTermino' =>  date('Y-m-d H:i:s')
 		);

		$condicao 							= array('id' => $idEntrega);
		
		if($CI->padrao_model->alterar("entrega", $dados, $condicao) > 0) {

  		$entrega = $CI->padrao_model->buscar("SELECT ENT.*, CLI.nome, CLI.imagem, CLI.email FROM entrega ENT LEFT JOIN cliente CLI ON ENT.cliente_id = CLI.id WHERE ENT.id = {$idEntrega} ORDER BY ENT.id DESC LIMIT 0,1")->row_array();

	 		$conteudo = array(
				'mensagem' 			=> '<tr>
								            <td align="center">
								                <table width="720px" cellspacing="0" cellpadding="3" class="container">
								                    <tr>
								                        <td style="font-size: 14px; padding-top: 20px">
								                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$entrega["nome"].'</strong>! Sua entrega foi realizada pela JASP!<br /><br />
								                        <center>
								                        	<b>Valor:</b> R$'.$entrega['valor'].'<br />
								                        	<b>De:</b> R$'.$entrega['enderecoOrigem'].'<br />
								                        	<b>Para:</b> R$'.$entrega['enderecoDestino'].'<br />
								                        	  '.$entrega['detalhes'].'<br />
								                        </center>
								                        </p> 
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>',
				'destinatario'		=> $entrega["email"],
				'assunto'			=> 'Entrega realizada pela JASP');

			  enviarEmail($conteudo, 'modelo-padrao');

			retornarJson(null, array('resposta' => true, 'mensagem' => "Entrega finalizada com sucesso!")); 
		}
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar o encerramento, tente novamente."));



	}
 
	function aceitarCorrida() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$idEntrega = $CI->input->post('id');
 		$profissional_id = $CI->input->post('profissional_id');
 		$dados = array(
 			'profissional_id' => $profissional_id 
 		);

		$condicao 							= array('id' => $idEntrega);
		
		if($CI->padrao_model->alterar("entrega", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Aceite realizado com sucesso!")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar o encerramento, tente novamente."));
	}


	function getCorridaAtual() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$cliente_id = $CI->input->post('cliente_id'); 
 		if(!empty($cliente_id) && !is_null($cliente_id)){
  		$corrida = $CI->padrao_model->buscar("SELECT ENT.*, PRO.proCodigo, PRO.proNome, PRO.proImagem FROM entrega ENT LEFT JOIN pro_profissional PRO ON ENT.profissional_id = PRO.proCodigo WHERE ENT.cliente_id = {$cliente_id} AND ENT.status = 'ATIVO' ORDER BY ENT.id DESC LIMIT 0,1")->row_array();

			retornarJson(null, $corrida);  
		}
		else {
				retornarJson(null, array( ));  
		}



	}
	function verificarCorridaProfissional() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		 $profissional_id = $CI->input->post("profissional_id");

  		$corrida = $CI->padrao_model->buscar("SELECT ENT.*, CLI.nome, CLI.imagem FROM entrega ENT LEFT JOIN cliente CLI ON ENT.cliente_id = CLI.id WHERE ENT.cliente_id = CLI.id AND ENT.status = 'ATIVO' AND (ENT.profissional_id IS NULL OR ENT.profissional_id = {$profissional_id}) ORDER BY ENT.id DESC LIMIT 0,1")->row_array();

			retornarJson(null, $corrida);  



	}
	
	function getEntregasProfissional() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$profissional_id = $CI->input->post('profissional_id'); 
 		$corrida = array();
  		$corrida  = $CI->padrao_model->buscar("SELECT * FROM entrega WHERE profissional_id = {$profissional_id} AND status = 'FINALIZADA' ORDER BY id DESC ")->result_array(); 

			retornarJson(null, $corrida);  



	}
	function getEntregas() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$cliente_id = $CI->input->post('cliente_id'); 
 		$corrida = array();
  		$corrida['realizadas'] = $CI->padrao_model->buscar("SELECT * FROM entrega WHERE cliente_id = {$cliente_id} AND status = 'FINALIZADA' ORDER BY id DESC ")->result_array();
  		$corrida['pendentes'] = $CI->padrao_model->buscar("SELECT * FROM entrega WHERE cliente_id = {$cliente_id} AND status = 'PENDENTE' ORDER BY id DESC ")->result_array();

			retornarJson(null, $corrida);  



	}
	function getRotaAtual() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		$id = $CI->input->post("id");
  		$ultima = $CI->padrao_model->buscar("SELECT * FROM entrega_rota WHERE entrega_id = {$id} ORDER BY id DESC ")->row_array();

			retornarJson(null, $ultima);  
	}
	function putRotaAtual() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		$id = $CI->input->post("id");
 		$retorno = 
 		array(
 			'entrega_id' => $id,
 			'lat' => $CI->input->post('lat'),
 			'lng' => $CI->input->post('lng'), 
 			'profissional_id' => $CI->input->post('profissional_id')
 		); 

  		$entrega = $CI->padrao_model->buscar("SELECT * FROM entrega WHERE id = {$id} ORDER BY id DESC ")->row_array();

  		if($entrega['status'] == 'ATIVO')
 			$ultima = $CI->padrao_model->inserir('entrega_rota', $retorno);
 		else
 			$ultima = false;

			retornarJson(null, $ultima);  
	}
	function calcularRota() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		$origem = json_decode($CI->input->post('origem'), true);
 		$destino = json_decode($CI->input->post('destino'), true);
 		$detalhes = ($CI->input->post('detalhes'));

 		$distancia = (round(calcularDistancia($origem["lat"], $origem["lng"], $destino['lat'], $destino['lng']),1)+0);

 		$retorno = 
 		array(
 			'distancia' => $distancia,
 			'latOrigem' => $origem['lat'],
 			'latDestino' => $destino['lat'], 
 			'lngOrigem' => $origem['lng'],
 			'lngDestino' => $destino['lng'], 
 			'cliente_id' => $CI->input->post('cliente_id'),
 			'detalhes'	=> $detalhes,
 			'enderecoDestino' => $destino['endereco'],
 			'enderecoOrigem' => $origem['endereco'],
 			'valor' 	=> 3+3+($distancia * 1.50)
 		); 

 		$retorno['id'] = $CI->padrao_model->inserir('entrega', $retorno);
 			$retorno['origem'] 	 = $origem;
 			$retorno['destino'] 	 = $destino;
 			$retorno['detalhes'] 	 = $detalhes;

 		retornarJson(null, $retorno);
	}
	// Retorna valores radianos
	function toRad($Value) 
	{
	    return $Value * pi() / 180;
	}
	function cancelarCorridaProfissional() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$id = $CI->input->post('id'); 
 		$dados = array(
 			'profissional_id' => null
 		);

		$condicao 							= array('id' => $id);
		
		if($CI->padrao_model->alterar("entrega", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Entrega cancelada com sucesso!")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar o cancelamento, tente novamente."));

	}

	function cancelarCorridaAtual() {
		$CI =& get_instance();
		$CI->load->model("padrao_model");
 		
 		$id = $CI->input->post('id'); 
 		$dados = array(
 			'status' => 'CANCELADA'
 		);

		$condicao 							= array('id' => $id);
		
		if($CI->padrao_model->alterar("entrega", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Entrega cancelada com sucesso!")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar o cancelamento, tente novamente."));

	}

	function getProfissionais($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model"); 

		if(!is_null($dados)){
		$parametro 				= json_decode($dados, true);

		// Retorna os resultados somente se a distância for exibida
		$obrigarLocalizacao 	= false;
		$ignorarRaio 			= false;

		$profissionais 			= array();
		$areas 					= array();
		$areTipo 				= !isset($parametro["areTipo"]) ? null : $parametro["areTipo"];
		$areCodigo 				= !isset($parametro["areCodigo"]) ? null : $parametro["areCodigo"];
		$data 					= !isset($parametro["data"]) ? null : $parametro["data"];
		$dados					= $CI->profissionais_model->profissionalDadosCompletos($areCodigo, $areTipo);

		foreach ($dados->result() as $key => $value) {
			$profissional 							= array();
			$endereco 								= json_decode($value->palLocalEndereco, true);

			// Latitude e longitude do local do profissional
			$proLat 								= $endereco["geometry"]["location"]["lat"];
			$proLong 								= $endereco["geometry"]["location"]["lng"];

			// Caso a busca seja em uma determinada data
			if(!is_null($data)){
				$verificarData = $CI->profissionais_model->profissionalVerificarDisponibilidade($value->proCodigo, $data)->num_rows();

				if(!($verificarData > 0))
					continue;
			}

			// Calcular distância entre local do profissional e do paciente
			if(isset($parametro["lat"]) && isset($parametro["long"]))
				$distancia 	= (round(calcularDistancia($parametro["lat"], $parametro["long"], $proLat, $proLong),1)+0);
			else if($obrigarLocalizacao)
				retornarJson(null, array('resposta' => false, 'mensagem' => "Por favor, ative sua localização para procurar por profissionais")); 
			else if(!$obrigarLocalizacao)
				$distancia = 5;

			// Caso a distância seja menor do que o raio de atendimento, adiciona ao array
			if($value->palTipo == 'CLIENTE' && $distancia < $value->palRaio)
				continue;

			$profissional["distancia"] 			= $distancia;
			$profissional["proCodigo"]			= $value->proCodigo;
			$profissional["proNome"]			= $value->proNome;
			$profissional["proApelido"]			= $value->proApelido;
			$profissional["proApresentacao"]	= $value->proApresentacao;
			$profissional["proImagem"]			= $value->proImagem;

			if(isset($value->areDescricao))
				$profissional["areDescricao"]	= $value->areDescricao;

			if(isset($value->areImagem))
				$profissional["areImagem"]		= $value->areImagem;

			// Nota do profissional
			$profissional["nota"] 				= getProfissionalNota($value->proCodigo, true);

			$profissional["local"] 				= array("palCodigo" => $value->palCodigo, "lat" => $proLat, "lng" => $proLong, "endereco" => $endereco["formatted_address"], "rotulo" => $value->palRotulo);
			$profissional["tipo"]				= array("tipo" => $value->palTipo, "raio" => $value->palRaio);
			$profissional["valor"]				= $value->palValor;


			// Caso nenhuma área tenha sido informada, monta um array com a respectiva área como pai
			if(!isset($areas[$value->areDescricao]))
				$areas[$value->areDescricao] = array("areDescricao" => $value->areDescricao, "areCodigo" => $value->areCodigo, "areImagem" => $value->areImagem,  "profissionais" => array());

				($areas[$value->areDescricao]["profissionais"][] =  $profissional);
		}

		if(!is_null($profissionais))
			retornarJson(null, $areas);
		}
		
		retornarJson(false);
	}
	function getProfissionaisPorArea($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$CI->load->model("pacientes_model");
		

		//Monta outros dados

		if(!is_null($dados)){
		$parametro 				= json_decode($dados, true); 

		
		$areas 					= array(); 
		$profissionais 			= array();
		$areCodigo 				= !isset($parametro["areCodigo"]) ? null : $parametro["areCodigo"]; 
		$dados					= $CI->profissionais_model->profissionalDadosCompletos($areCodigo);
 	
		$i = 0;

		foreach ($dados->result() as $key => $value) {
			$profissional 							= array();
			$endereco 								= json_decode($value->palLocalEndereco, true);

			// Latitude e longitude do local do profissional
			$proLat 								= $endereco["geometry"]["location"]["lat"];
			$proLong 								= $endereco["geometry"]["location"]["lng"];
  

			$profissional["proCodigo"]			= $value->proCodigo;
			$profissional["proNome"]			= $value->proNome;
			$profissional["proApelido"]			= $value->proApelido;
			$profissional["proApresentacao"]	= $value->proApresentacao;
			$profissional["proImagem"]			= $value->proImagem;
			$profissional["proOrgao"]			= $value->proOrgao;
			

			if(isset($value->areDescricao))
				$profissional["areDescricao"]	= $value->areDescricao;

			if(isset($value->areImagem))
				$profissional["areImagem"]		= $value->areImagem;

			// Nota do profissional
			$profissional["nota"] 				= getProfissionalNota($value->proCodigo, true);

			$profissional["local"] 				= array("palCodigo" => $value->palCodigo, "lat" => $proLat, "lng" => $proLong, "endereco" => $endereco["formatted_address"], "rotulo" => $value->palRotulo);
			$profissional["tipo"]				= array("tipo" => $value->palTipo, "raio" => $value->palRaio);
			$profissional["valor"]				= $value->palValor;
			$profissional["valorAssinatura"]				= $value->palValorAssinatura;
			$profissional["palAssinaturaUtilizacao"]				= $value->palAssinaturaUtilizacao;

			

			if($CI->profissionais_model->listarDadosLocalAtendimento($value->palCodigo)->num_rows() == 0 || empty($CI->profissionais_model->listarDadosLocalAtendimento($value->palCodigo)->row()->palValorAssinatura)){
				$profissional["statusAssinatura"]	= 1;
			}else if($CI->pacientes_model->isClienteAssinante($parametro["pacCodigo"],$value->palCodigo)->num_rows() > 0){
				$profissional["statusAssinatura"]	= 3;
			}else{
				$profissional["statusAssinatura"]	= 2;
			}

 
			if($i == 0 && !isset($areas[$value->areDescricao]))
				$areas = array("areDescricao" => $value->areDescricao, "areCodigo" => $value->areCodigo, "areImagem" => $value->areImagem,  "profissionais" => array());
				($areas["profissionais"][] =  $profissional);

				$i++;
		}

		if(!is_null($profissionais))
			retornarJson(null, $areas);
		}
		
		retornarJson(false);
	}
	function getProfissionalAgenda($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 								= montarCamposPost(['proCodigo']);

		if(!is_null($dados)){
		$dados			= $CI->profissionais_model->profissionalAgenda($dados["proCodigo"])->result_array();
		
		if(!is_null($dados))
			retornarJson(null, $dados);
		}
		retornarJson(false);
	}
	function getProfissionalNota($dados = null, $retorno = false)
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model");

		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);
			
			$dados			= $CI->profissionais_model->profissionalNota($parametro["proCodigo"])->row();
			$nota["nota"]	= $dados->nota != null ? $dados->nota : 0;
		}
		
		if(!$retorno)
			retornarJson(null, $nota);
		else
			return $nota["nota"];
	}
	function getLocaisAtendimento($dados = null){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");

		$locais = array();

		if(!is_null($dados)){
			$dados 								= montarCamposPost(['proCodigo']);
			$dados			= $CI->profissionais_model->profissionalLocaisAtendimentoGeral($dados["proCodigo"]);
			foreach ($dados->result() as $key => $value) {
				$endereco = json_decode($value->palLocalEndereco, true);
				$locais[$key]["palCodigo"]		= $value->palCodigo;
				$locais[$key]["local"] 			= array("palCodigo" => $value->palCodigo, "lat" => $endereco["geometry"]["location"]["lat"], "lng" => $endereco["geometry"]["location"]["lng"], "endereco" => $endereco["formatted_address"], "rotulo" => $value->palRotulo);
				$locais[$key]["tipo"]			= array("tipo" => $value->palTipo, "raio" => $value->palRaio);
				$locais[$key]["valor"]			= $value->palValor;
				$locais[$key]["valorAssinatura"]= $value->palValorAssinatura;
			}
		}
		
		retornarJson(null, $locais);
	}
	function getMeusAtendimentos($dados = null){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$parametro 		= json_decode($dados, true);
		$dados			= $CI->profissionais_model->profissionalAtendimentos($parametro["proCodigo"])->result();
		retornarJson(null, $dados);
	}
	function getMeusAtendimentosHistorico($dados = null){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$parametro 		= json_decode($dados, true);
		$dados			= $CI->profissionais_model->profissionalHistoricoPorAgenda($parametro["ageCodigo"])->result();
		retornarJson(null, $dados);
	}

	function getProfissionalEspecialidades($dados = null){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$parametro 		= json_decode($dados, true);
		$dados			= $CI->profissionais_model->profissionalEspecialidades($parametro["proCodigo"])->result();
		retornarJson(null, $dados);
	}



	  function excluirProfissional ( )
	{	
		$CI =& get_instance(); 
		$CI->load->model('padrao_model'); 
		$dados 								= montarCamposPost(['proCodigo']);
		$dados['proStatus']	 				= 'EXCLUIDO';
		$condicao 							= array('proCodigo' => $dados['proCodigo']);
		
		if($CI->padrao_model->alterar("pro_profissional", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Atualização de status realizada com sucesso")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a atualização, tente novamente."));
	}
	  function eliminarProfissional ( )
	{	
		$CI =& get_instance(); 
		$CI->load->model('padrao_model'); 
		$dados 								= montarCamposPost(['proCodigo']);
		$dados['proStatus']	 				= 'ELIMINADO';
		$condicao 							= array('proCodigo' => $dados['proCodigo']);
		
		if($CI->padrao_model->alterar("pro_profissional", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Atualização de status realizada com sucesso")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a atualização, tente novamente."));
	}
	function ativarProfissional ( )
	{	
		$CI =& get_instance(); 
		$CI->load->model('padrao_model'); 
		$dados 								= montarCamposPost(['proCodigo']);
		$dados['proStatus']	 				= 'ATIVO';
		$condicao 							= array('proCodigo' => $dados['proCodigo']);

		if($CI->padrao_model->alterar("pro_profissional", $dados, $condicao) > 0) 
			retornarJson(null, array('resposta' => true, 'mensagem' => "Atualização de status realizada com sucesso")); 
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a atualização, tente novamente."));
	}
	function verificarDocumentos ( )
	{	
		$CI =& get_instance(); 
		$CI->load->model('profissionais_model'); 
		$dados 								= montarCamposPost(['proCpf','proEmail','proDocumentoIdentidade']);

		if($CI->profissionais_model->verificarDocumentos($dados)->num_rows() > 0) 
			die(json_encode(false));
		else
			die(json_encode(true));
	}

	 function setProfissional ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 		= montarCamposPost(['proCodigo', 'proNome', 'proProfissao', 'proStatus', 'proSobrenome', 'proEmail', 'proSexo', 'proNascimento',  'proCpf', 'proApelido', 'proImagem', 'proTelefone', 'orgCodigo', 'proApresentacao', 'proDocumentoIdentidade', 'proDocumentoIdentidadeEmissor', 'proDocumentoIdentidadeData', 'proNascimento', 'proOrgao', 'proLogradouroCep', 'proLogradouro', 'proLogradouroNumero', 'proLogradouroBairro', 'proLogradouroCidade', 'proLogradouroEstado']);
		$dados['proNascimento'] 			 = converterDataMascara($dados['proNascimento']);
		$dados['proDocumentoIdentidadeData'] = converterDataMascara($dados['proDocumentoIdentidadeData']);
			if(!isset($dados['proCodigo'])){
		if(isValidEmail($dados['proEmail'])){

				$mensagemRetorno 		= 'Profissional inserido com sucesso!';
				$senha 					= random_string(4);
				$dados['proSenha'] 		= $CI->encryption->encrypt($senha);
				sendWelcomeEmail($dados,$senha);

				if($CI->padrao_model->inserir("pro_profissional", $dados) > 0){
					retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
				}
				else{
					retornarJson(false);	
				}
		}else{
			retornarJson(null, array('resposta' => false, 'mensagem' => 'O e-mail informado já existe em nossa base de dados. Tente outro e-mail.'));
		}
			}	
			else {		
				$mensagemRetorno 		= 'Profissional alterado com sucesso!';
				$condicao 				= array('proCodigo' => $dados['proCodigo']);
				if($CI->padrao_model->alterar("pro_profissional", $dados, $condicao) > 0) 
					retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno)); 
				else
					retornarJson(false);
			}	
	}
	 function setProfissionalSite ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 	= json_decode(file_get_contents('php://input'),true);
		$dados['proStatus'] = "PENDENTE";
		if(!isset($dados['proCodigo'])){
			if(isValidEmail($dados['proEmail'])){
					$mensagemRetorno 		= 'Seu registro foi realizado em nossa base de dados. Em breve entraremos em contato para ativação na plataforma';
					sendPreWelcomeEmail($dados);
					if($CI->padrao_model->inserir("pro_profissional", $dados) > 0){
						retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
					}
					else{
						retornarJson(null, array('resposta' => false, 'mensagem' => 'Ocorreu um erro ao inserir os dados na plataforma. Tente novamente mais tarde.'));	
					}
			}else{
				retornarJson(null, array('resposta' => false, 'mensagem' => 'O e-mail informado já existe em nossa base de dados. Tente outro e-mail.'));
			}
		}
	}
	 function setProfissionalPendente ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 	= montarCamposPost(['proNome', 'proEmail']);
		$dados['proStatus'] = "PENDENTE";
		if(!isset($dados['proCodigo'])){
			if(isValidEmail($dados['proEmail'])){
					$mensagemRetorno 		= 'Seu registro foi realizado em nossa base de dados. Em breve entraremos em contato para ativação na plataforma';
					sendPreWelcomeEmail($dados);
					if($CI->padrao_model->inserir("pro_profissional", $dados) > 0){
						retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
					}
					else{
						retornarJson(null, array('resposta' => false, 'mensagem' => 'Ocorreu um erro ao inserir os dados na plataforma. Tente novamente mais tarde.'));	
					}
			}else{
				retornarJson(null, array('resposta' => false, 'mensagem' => 'O e-mail informado já existe em nossa base de dados. Tente outro e-mail.'));
			}
		}
	}
	function setProfissionalSiteComplemento()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$CI->load->model('profissionais_model');
		$dados 								= json_decode(file_get_contents('php://input'),true);
		$dados['dados']['proCodigo'] 		= decifracub3($dados['dados']['proCodigo']);
		$dados['dados']['proStatus'] 		= "COMPLEMENTADO";
		$profissional 						= $CI->profissionais_model->profissionalPorCodigo($dados['dados']['proCodigo'])->row();
		$dados['dados']['proEmail']  		= $profissional->proEmail;
		sendPosWelcomeEmail($dados['dados']);
		$condicao = array("proCodigo" => $dados['dados']['proCodigo']);
		if($CI->padrao_model->alterar("pro_profissional", $dados['dados'],$condicao) > 0){
			retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => 'Seu registro foi atualizado em nossa base de dados. Em breve entraremos em contato para ativação na plataforma'));
		}
		else{
			retornarJson(null, array('resposta' => false, 'mensagem' => 'Ocorreu um erro ao inserir os dados na plataforma. Tente novamente mais tarde.'));	
		}
	}
 	function sendWelcomeEmail($dados,$senha){
 		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["proNome"].'</strong>! Você acabou de ser registrado no JASP. Uma senha aleatória foi criada que você acesse seu cadastro:<br /><br /></p>
							                        <p style="text-align:center"><a style="background-color: #fef37e; color: #30343F; text-decoration: none; text-transform: uppercase; padding: 10px; font-weight: bold;" href>'.$senha.'</a></p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["proEmail"],
			'assunto'			=> 'Seja bem vindo ao JASP');

		return enviarEmail($conteudo, 'modelo-padrao');
 	}
 	function sendPreWelcomeEmail($dados){
 		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["proNome"].'</strong>! Você acaba de se cadastrar no JASP. Seu cadastro será avaliado pelos administradores da plaforma e em breve você receberá um novo contato.</p>
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Obrigado por se cadastrar.</p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["proEmail"],
			'assunto'			=> 'Pré-cadastro JASP');

		return enviarEmail($conteudo, 'modelo-padrao');
 	}
 	function sendPosWelcomeEmail($dados){
 		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["proNome"].'</strong>! Recebemos sua complementação de cadastro. Realizaremos a avaliação dos dados para efetivação de seu cadastro.</p>
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Obrigado.</p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["proEmail"],
			'assunto'			=> 'Pós-cadastro JASP');

		return enviarEmail($conteudo, 'modelo-padrao');
 	}
 	function sendComplementacaoEmail($dados){
 		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellspacing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados->proNome.'</strong>! Seu cadastro foi selecionado para complementação. <a href="'.base_url("complementarCadastro/").cifracub3($dados->proCodigo).'">Clique aqui</a> para enviar as informações complementares de seu cadastro.</p>
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Obrigado por se cadastrar.</p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados->proEmail,
			'assunto'			=> 'Complementação de dados - JASP');

		return enviarEmail($conteudo, 'modelo-padrao');
 	}


	function getProfissional()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model"); 
		$completo 	= $CI->input->post("completo") == true ? true : false;
		$dados 		= $CI->profissionais_model->profissionalPorCodigo($CI->input->post("proCodigo"))->row_array();

		if(!$completo) {
			if(!is_null($dados))
				retornarJson(null, $dados);
			else
				retornarJson(false);
		}
		else { 
			$dadosBancarios = $CI->profissionais_model->listarDadosBancariosPorProfissional($dados['proCodigo'])->row_array(); 

			$dados['proNascimento'] = converterData($dados['proNascimento']);
			$dados['proDocumentoIdentidadeData'] = converterData($dados['proDocumentoIdentidadeData']);
			
			$retorno = array(
				'profissional' 		=> $dados, 
				'dadosbancarios' 	=> $dadosBancarios
			 );
			retornarJson(null, $retorno);
		}
	}
	function isValidEmail($email){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 		= $CI->profissionais_model->profissionalPorEmail($email);

		if($dados->num_rows() > 0)
			return false;
		else
			return true;
	}

	/*
	* [Método]: setProfissionalArea
	* [Descrição]: Associa uma área a um profissional
	*/
	function setProfissionalArea()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 					= montarCamposPost(['pro_profissional_proCodigo', 'pro_areaAtuacao_areCodigo']);
		$mensagemRetorno 		= 'Associação de área realizada com sucesso!';
		if($CI->padrao_model->inserir("pro_profissional_has_pro_areaAtuacao", $dados) > 0){
			retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
		}
		else{
			retornarJson(false);	
		}
	}
	/*
	* [Método]: getProfissionalAreas
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalAreas()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarAreasProProfissional($dados['proCodigo'])->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: setProfissionalEspecialidade
	* [Descrição]: Associa uma especialidade a um profissional
	*/
	function setProfissionalEspecialidade()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 					= montarCamposPost(['proCodigo', 'espCodigo']);
		$mensagemRetorno 		= 'Associação de especialidade realizada com sucesso!';
		if($CI->padrao_model->inserir("pro_profissional_has_proEspecialidade", $dados) > 0){
			retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
		}
		else{
			retornarJson(false);	
		}
	}
	/*
	* [Método]: setProfissionalDadosBancarios
	* [Descrição]: Insere/Altera dados de uma conta bancária de um profissional
	*/
	function setProfissionalDadosBancarios()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 		= montarCamposPost(['pdaCodigo','pdaAgencia', 'pdaConta', 'pdaBanco', 'pdaObservacao', 'proCodigo']);
		if(!isset($dados['pdaCodigo'])){
			$mensagemRetorno 		= 'Dados bancários inseridos com sucesso!';
			if($CI->padrao_model->inserir("pro_profissional_dadosbanco", $dados) > 0){
				retornarJson(null, array('resposta' => true, 'url' => 'pages/jasp/profissionais-ativos', 'mensagem' => $mensagemRetorno));
			}
			else{
				retornarJson(false);	
			}
		}
		else {		
			$mensagemRetorno 		= 'Dados bancários alterados com sucesso!';
			$condicao 				= array('pdaCodigo' => $dados['pdaCodigo']);
			if($CI->padrao_model->alterar("pro_profissional_dadosbanco", $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'url' => 'pages/jasp/profissionais-ativos', 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(false);
		}	
	}
	/*
	* [Método]: getProfissionalDadosBancarios
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalDadosBancarios()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarDadosBancariosPorProfissional($dados['proCodigo'])->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: setProfissionalAtendimentoLocal
	* [Descrição]: Insere/Altera dados de uma conta bancária de um profissional
	*/
	function setProfissionalAtendimentoLocal()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 				= montarCamposPost(['palCodigo','palLocalEndereco', 'palLocalSugestao', 'palRotulo', 'proCodigo', 'palValor', 'palValorAssinatura']);
		$dados['palTipo']	= "PROPRIO";

		if(!isset($dados['palCodigo'])){
			$mensagemRetorno 		= 'Dados do local de atendimento inseridos com sucesso!';
			if($CI->padrao_model->inserir("pro_profissionalAtendimentoLocal", $dados) > 0){
				retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
			}
			else{
				retornarJson(false);	
			}
		}
		else {		
			$mensagemRetorno 		= 'Dados do local de atendimento alterados com sucesso!';
			$condicao 				= array('palCodigo' => $dados['palCodigo']);
			if($CI->padrao_model->alterar("pro_profissionalAtendimentoLocal", $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(false);
		}	
	}
	/*
	* [Método]: setProfissionalDispositivo
	* [Descrição]: Insere/Altera dados de uma conta bancária de um profissional
	*/
	function setProfissionalDispositivo()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 				= montarCamposPost(['usdCodigo','usdUuid', 'usdStatus', 'proEmail', 'proCodigo', 'usdPushId']);

		if(!isset($dados['usdCodigo'])){
			$mensagemRetorno 		= 'Dados do dispositivo inseridos com sucesso!';
			if($CI->padrao_model->inserir("pro_profissional_dispositivo", $dados) > 0){
				retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno));
			}
			else{
				retornarJson(false);	
			}
		}
		else {		
			$mensagemRetorno 		= 'Dados do dispositivo alterados com sucesso!';
			$condicao 				= array('usdCodigo' => $dados['usdCodigo']);
			if($CI->padrao_model->alterar("pro_profissional_dispositivo", $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(false);
		}	
	}
	/*
	* [Método]: getProfissionalDispositivo
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalDispositivo()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarDispositivoPorProfissional($dados['proCodigo'])->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalInativo
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalInativo()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarProfissionaisPorStatus('EXCLUIDO')->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalPendente
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalPendente()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarProfissionaisPorStatus('PENDENTE')->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalPendente
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalEmComplementacao()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarProfissionaisPorStatus('EM COMPLEMENTAÇÃO')->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalComplementado
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalComplementado()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarProfissionaisPorStatus('COMPLEMENTADO')->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalAtivo
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalAtivo()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 		= $CI->profissionais_model->listarProfissionaisPorStatus('ATIVO')->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: getProfissionalAtivoComLocalAtendimento
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getProfissionalAtivoComLocalAtendimento()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 		= $CI->profissionais_model->listarProfissionaisComLocalAtendimento()->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}

	
	 function setComplementacaoProfissional ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$CI->load->model('profissionais_model');

		$dados 				= montarCamposPost(['proCodigo']);
		$profissional 		= $CI->profissionais_model->profissionalPorCodigo($dados['proCodigo']);
		$dados['proStatus'] = "EM COMPLEMENTAÇÃO";

		//Altera status do profissional
		$condicao = array("proCodigo" => $dados['proCodigo']);
		$CI->padrao_model->alterar("pro_profissional",$dados,$condicao);
		//Envia e-mail de complementação
		if(sendComplementacaoEmail($profissional->row())){
			retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => "E-mail de complementação enviado com sucesso"));
		}else{
			retornarJson(null, array('resposta' => false, 'mensagem' => 'Ocorreu um erro. Tente novamente.'));
		}
	}
	function setProfissionalAgenda(){
		$CI =& get_instance();
		$CI->load->model('padrao_model');

		$dados 							= montarCamposPost(['ageHorarioInicial','ageHorarioFinal','proCodigo','palCodigo','areCodigo']);
		$dados['ageStatus']				= "LIBERADO";
		$dados['ageTipoSolicitacao']	= "CONSULTA";

		if($CI->padrao_model->inserir("pro_agenda",$dados)) 
			retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => "Agendamento inserido com sucesso!")); 
		else
			retornarJson(false);
	}
	/*
	* [Método]: getPacientesPorProfissional
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	function getPacientesPorProfissional()
	{
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 	= montarCamposPost(['proCodigo']);
		$dados 		= $CI->profissionais_model->listarPacientesPorProfissional($dados['proCodigo'])->result_array();
		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	/*
	* [Método]: excluirLocal 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	function excluirLocal ($post, $campoIdentificador = 'palCodigo', $campoStatus = 'palStatus', $tabela = 'pro_profissionalAtendimentoLocal')
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 								= array(); 
		$dados[$campoStatus]	 			= 'INATIVO';
		$dados[$campoIdentificador]			= urldecode(decifracub3($CI->input->post($campoIdentificador)));
		$mensagemRetorno 					= 'Linha excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da linha.';
  
		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($CI->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}
	/*
	* [Método]: excluirArea 
	* [Descrição]: Exclui um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador, $campoStatus e $tabela
	*/
	function excluirArea ($post, $campoIdentificador = 'areCodigo', $campoStatus = 'areStatus', $tabela = 'pro_profissional_has_pro_areaAtuacao')
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 								= array(); 
		$dados[$campoStatus]	 			= 'INATIVO';
		$dados[$campoIdentificador]			= urldecode(decifracub3($CI->input->post($campoIdentificador)));
		$mensagemRetorno 					= 'Linha excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da linha.';
  
		if(isset($dados[$campoIdentificador])){ 		
			$condicao = array($campoIdentificador => $dados[$campoIdentificador]);
			if($CI->padrao_model->excluir($tabela,  $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}

