<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('enviarEmail'))
{
	function enviarEmail($conteudo, $modelo = 'modelo-padrao')
	{
		$CI =& get_instance(); 
		$CI->load->model("cub3_email_model");

		$dadosEmail = $CI->cub3_email_model->emailListarPorCodigoOuSlug($modelo);
		 
		if($dadosEmail->num_rows() > 0):
			$dadosEmail = $dadosEmail->row_array();
				$conteudo["dataAtual"] 	= dataPorExtenso(date('Y-m-d H:i:s'));
				$corpo = tratarCorpoEmail($dadosEmail["emtCorpo"], $conteudo);
				$config = Array(
				    'protocol' => "smtp",
				    'smtp_host' => stripslashes($CI->config->item("smtp_host", "cub3_config_email")),
				    'smtp_port' => $CI->config->item("smtp_port", "cub3_config_email"),
				    'smtp_user' => $CI->config->item("smtp_user", "cub3_config_email"),
				    'smtp_pass' => $CI->config->item("smtp_pass", "cub3_config_email"),
				    'mailtype'  => "html",  
				    'charset'   => $CI->config->item("charset", "cub3_config_email")
				);
				$CI->load->library('email', $config);
				$CI->email->set_newline("\r\n");

				$CI->email->from(($dadosEmail["emtRemetenteEmail"] != null ? $dadosEmail["emtRemetenteEmail"] : 'ti@cub3.com.br'), ($dadosEmail["emtRemetente"] != null ? $dadosEmail["emtRemetente"] : $CI->config->item("template_tituloSistema")));
				$CI->email->to($conteudo['destinatario']);
				$CI->email->subject($conteudo['assunto']);
				$CI->email->message($corpo);	
				return $CI->email->send();	
		else:
			return false;
		endif;
	}
}
if ( ! function_exists('tratarCorpoEmail'))
{
	function tratarCorpoEmail($corpo, $dados)
	{ 
	    foreach ($dados as $key => $value) {
	        $tag 		= "[@$key]";
	        $corpo 	= str_replace($tag, $value, $corpo);
	    }
	 
	    return $corpo; 
	}
}
 
 