<?php 
	/*
	* [Método]: notificarPush 
	*/
	function notificarPush($dados = null, $tipo = null)
	{
			$CI =& get_instance(); 
			$CI->load->model("padrao_model");

			$registrationIds = array();
			
			if(is_null($dados))
				$dados = $CI->input->post();

			// Define a tabela de cruzamento com os dispositivos
			if(is_null($tipo))
				return false;
			else{
				switch ($tipo) {
					case 'paciente':
						$tabela = 'pac_paciente_dispositivo';
						$id 	= 'pacEmail';
						break;
					case 'profissional':
						$tabela = 'pro_profissional_dispositivo';
						$id 	= 'proEmail';
						break;
					default:
						return false;
				}
			}

			// Monta a mensagem que será enviada
			$acoes 	=  array(array('icon' => 'forward', 'title' => 'Visualizar','callback' => 'window.alterar_rota'));
			$msg 	= array
			(
				'message' 		=> isset($dados["mensagem"]) ? $dados["mensagem"] : '',
				'title'			=> isset($dados["titulo"]) ? $dados["titulo"] : '',
				'subtitle'		=> isset($dados["subTitulo"]) ? $dados["subTitulo"] : '',
				'tickerText'	=> isset($dados["tickerText"]) ? $dados["tickerText"] : '',
				'vibrate'		=> 1,
				'sound'			=> 1,
				'image'			=> isset($dados["imagem"]) ? $dados["imagem"] : 'http://cub3.com.br/assets/img/favicon.png',
				'url'			=> isset($dados["url"]) ? $dados["url"] : '',
				'rota' 			=> isset($dados["rota"]) ? $dados["rota"] : '',
				'rotaDados'		=> isset($dados["rotaDados"]) ? $dados["rotaDados"] : '',
				'actions' 		=> isset($dados["acoes"]) ? $dados["acoes"] : $acoes
			);

			// Caso nenhum e-mail tenha sido especificado, manda para todos os dispositivos cadastrados
			if(!isset($dados["emails"])){
				$query = $CI->padrao_model->buscar("SELECT usdPushId FROM ".$tabela." WHERE usdPushId IS NOT NULL")->result_array();

				for ($i=0; $i < Count($query) ; $i++) { 
					array_push($registrationIds, $query[$i]["usdPushId"]);
				} 
			}
			else{ 
				$emails 			= explode(',', $dados["emails"]);

				for ($i=0; $i < Count($emails) ; $i++) { 
					$query = $CI->padrao_model->buscar("SELECT usdPushId FROM ".$tabela." WHERE usdPushId IS NOT NULL AND ".$id." = '".$emails[$i]."'")->result_array();
					for ($j=0; $j < Count($query); $j++) { 
						$dadosTabela 		= $msg;
						$dadosTabela[$id]	= $query[$i][$id];
						
						$CI->padrao_model->inserir($tabela, $dadosTabela);
						array_push($registrationIds, $query[$j]["usdPushId"]);
					} 
				} 
			}

			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg,
				'coldstart' 	=> 1
			);
			 
			$headers = array
			(
				'Authorization: key=' . "AIzaSyAxjCtcjWZGiHjYMU-SA1bg5hKwfGeTTQk",
				'Content-Type: application/json'
			);
			 
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch ); 

			return $result;

	}

 