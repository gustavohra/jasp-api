<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('montarMenuJson'))
{
	function montarMenuJson($recriarMenu = false)
	{ 
		$CI 					=& get_instance(); 

		if($CI->session->userdata('s_menu') != null || $recriarMenu == false)
			$menus = json_decode($CI->session->userdata('s_menu'), true);
		else{
			$CI->load->model("cub3_menu_model");
			$menus = $CI->cub3_menu_model->menuListarPorGrupo($CI->session->userdata('s_gruCodigo'))->result_array();
			for ($i=0; $i < Count($menus); $i++) { 
				$menus[$i]["menuItens"] = $CI->cub3_menu_model->menuSubmenuListarTodos($menus[$i]["menCodigo"])->result_array();
			}
		}
		return $menus;
	}
}
 

