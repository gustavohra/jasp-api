<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('getConfigProjeto'))
{
	function getConfigProjeto()
	{ 
		return json_decode(file_get_contents("./projeto.json"), true);
	}
}
 
 
if ( ! function_exists('setConfigProjeto'))
{
	function setConfigProjeto($configuracoes)
	{ 
		if($configuracoes != null){
			$configuracoes 	= json_encode($configuracoes, JSON_PRETTY_PRINT);
			$arquivo 		= fopen("./projeto.json", "w");
			
			if($configuracoes != false){
				try {
					fwrite($arquivo, $configuracoes);
					fclose($arquivo);
				} catch (Exception $e) {
					return false;
				}	finally {
				   return true;
				}
			}
			else
				return false;
		}
		else
			return false;
	}
}
 
 