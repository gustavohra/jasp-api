<?php

	function abrirChamado($dados = null) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");
 	
 		$detalhes = $CI->input->post('conteudo');
 		$usuario = $CI->input->post('pacCodigo');
 		if($usuario != null) {
	 		$usuarioDados = $CI->padrao_model->buscar("SELECT pacNome, pacEmail FROM pac_paciente WHERE pacCodigo = ".$usuario)->row_array();
	 		$mensagem 	= "<p><b>".$usuarioDados['pacNome']."</b> (".$usuarioDados['pacEmail'].") realizou através do aplicativo JASP, a abertura de um novo chamado de suporte e apontou os seguintes detalhes: <br /><br /> <div style='text-align:center'>".$detalhes."</div></p>";

			$conteudo = array(
				'mensagem' 			=> '<tr>
								            <td align="center">
								                <table width="720px" cellspacing="0" cellpadding="3" class="container">
								                    <tr>
								                        <td style="font-size: 14px; padding-top: 20px">
								                        '.$mensagem.'
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>',
				'destinatario'		=> 'widsantos@icloud.com,lazarosamuell@icloud.com',
				'assunto'			=> 'Suporte / Novo chamado aberto');

			enviarEmail($conteudo); 
	 		retornarJson(null, array('resposta' => $CI->padrao_model->inserir('suporte', array('conteudo' => $detalhes, 'cliente_id' => $usuario ))));
 		}
 		else
 			retornarJson(false);
	}

	function getChamados()
	{
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 		= $CI->input->post('pacCodigo');

		if($id != null && $id != ''){
			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte WHERE cliente_id = {$id}")->result_array();
			if(!is_null($dados))
				retornarJson(null, array('resposta' => true, 'chamados' => $dados ));
			else
				retornarJson(false);
		}
		else
			retornarJson(null);
	}
	function getChamadoRespostas()
	{
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 		= $CI->input->post('id');

		if($id != null && $id != ''){
			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte_mensagem WHERE suporte_id = {$id}")->result_array();

			if(!is_null($dados))
				retornarJson(null, $dados );
			else
				retornarJson(false);
		}
		else
			retornarJson(null);
	}

	function responderChamado() {
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 			= $CI->input->post('id');
		$msg 			= $CI->input->post('msg');
		$clienteId 		= $CI->input->post('cliente_id');

		$dados 		= array(
			'suporte_id' => $id,
			'conteudo' => $msg,
			'cliente_id' => $clienteId
		 );
		if($id != null && $id != ''){
			$inserirId 		= $CI->padrao_model->inserir("suporte_mensagem", $dados);

			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte_mensagem WHERE id = {$inserirId}")->row_array();

			if(!is_null($dados))
				retornarJson(null, $dados );
			else
				retornarJson(false);
		}
		else
			retornarJson(null);

	}
