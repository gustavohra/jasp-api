<?php


function getPagamento($dados) {
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$CI->load->library("encryption");
			
			$id = $CI->input->post("cliente_id");

			$dados 					= $CI->padrao_model->buscar("SELECT * FROM cliente_cartao WHERE cliente_id = {$id} ORDER BY id DESC LIMIT 0,1")->row_array();

			if($dados != null):
			$informacoes = array(
				'cvv' =>  $CI->encryption->decrypt($dados["cvv"]),
				'numero' =>   ($CI->encryption->decrypt($dados["numero"])),
				'primeiroNome' =>  $CI->encryption->decrypt($dados["primeiroNome"]),
				'ultimoNome' =>  $CI->encryption->decrypt($dados["ultimoNome"]),
				'expiration' =>  ($dados["expiration"]),
				'mes' =>  isset($dados["mes"]) ? $CI->encryption->decrypt($dados["mes"]) : '',
				'ano' =>  isset($dados["ano"]) ? $CI->encryption->decrypt($dados["ano"]) : '',
			);
 

			retornarJson(null, array('card' => $informacoes ));
			else:
				retornarJson(null);
			endif;

 

}
function setPagamento($dados) {
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$CI->load->library("encryption");
		
		if($dados != null){
			$dados 					= json_decode($dados, true);
			$exp = explode("/", $dados['expiration']);

			$informacoes = array(
				'cvv' =>  $CI->encryption->encrypt($dados["cvv"]),
				'numero' =>  $CI->encryption->encrypt($dados["numero"]),
				'primeiroNome' =>  $CI->encryption->encrypt($dados["primeiroNome"]),
				'ultimoNome' =>  $CI->encryption->encrypt($dados["ultimoNome"]),
				'mes' =>  $CI->encryption->encrypt($exp[0]),
				'ano' =>  $CI->encryption->encrypt($exp[1]),
				'expiration' =>   ($dados["expiration"]),
				'cliente_id' =>  $dados["cliente_id"]);

				if(!isset($dados['id']))
					$CI->padrao_model->inserir('cliente_cartao', $informacoes);
				else
					$CI->padrao_model->alterar('cliente_cartao',array('id' => $dados['id'], 'cliente_id' => $dados['cliente_id'] ), $informacoes);

			retornarJson(true);
		}
		else {
			retornarJson(null, array('mensagem' => 'Informações incorretas', 'resposta' => false ));			
		}

}
	
if ( ! function_exists('realizarLoginFb'))
{
	function realizarLoginFb($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model"); 
		$CI->load->model("padrao_model"); 
		$CI->load->helper('string');
			$CI->load->library('encryption');	

		if($dados != null){
			$dados 					= json_decode($dados, true);
			$cliente 				= $CI->clientes_model->clienteValidarLogin($dados["email"])->row_array();

			if(!is_null($cliente)){ 
				retornarJson(null, array("resposta" => true, "usuario" => $cliente, "sessao" => $CI->session->userdata)); 
			}
			else{ 
				$nomeArquivo 	= random_string('unique', 16) . ".png";
				$nomes 			= explode(' ', $dados['name']);

				$novoCliente 				= array(
					'nome' 		 => $nomes[0], 
					'sobrenome' => $nomes[Count($nomes)-1],
					'imagem' 	 => $nomeArquivo,
					'email'  	 => $dados['email'],
					'facebookId' => $dados['id'],
					"horario"	 => date("Y-m-d H:i:s")
				);
				
				$imageString = file_get_contents($dados['picture']['data']['url']);
				$save = file_put_contents('others/uploads/avatar/'.$nomeArquivo,$imageString);
				$CI->padrao_model->inserir("cliente",$novoCliente);

				$dados["id"] 				= $CI->db->insert_id();
				$cliente 					= $CI->cliente_model->clientePorEmail($dados["email"])->row_array();
	 
				retornarJson(null, array("resposta" => true, "usuario" => $cliente, "sessao" => $CI->session->userdata)); 
			}
		}
		else
			retornarJson(false);
	}
} 

	function abrirChamado($dados = null) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");
 	
 		$detalhes = $CI->input->post('conteudo');
 		$usuario = $CI->input->post('pacCodigo');
 		if($usuario != null) {
	 		$usuarioDados = $CI->padrao_model->buscar("SELECT pacNome, pacEmail FROM pac_paciente WHERE pacCodigo = ".$usuario)->row_array();
	 		$mensagem 	= "<p><b>".$usuarioDados['pacNome']."</b> (".$usuarioDados['pacEmail'].") realizou através do aplicativo BeOk, a abertura de um novo chamado de suporte e apontou os seguintes detalhes: <br /><br /> <div style='text-align:center'>".$detalhes."</div></p>";

			$conteudo = array(
				'mensagem' 			=> '<tr>
								            <td align="center">
								                <table width="720px" cellspacing="0" cellpadding="3" class="container">
								                    <tr>
								                        <td style="font-size: 14px; padding-top: 20px">
								                        '.$mensagem.'
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>',
				'destinatario'		=> 'widsantos@icloud.com,lazarosamuell@icloud.com',
				'assunto'			=> 'Suporte / Novo chamado aberto');

			enviarEmail($conteudo); 
	 		retornarJson(null, array('resposta' => $CI->padrao_model->inserir('suporte', array('conteudo' => $detalhes, 'cliente_id' => $usuario ))));
 		}
 		else
 			retornarJson(false);
	}

	function getChamados()
	{
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 		= $CI->input->post('pacCodigo');

		if($id != null && $id != ''){
			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte WHERE cliente_id = {$id}")->result_array();
			if(!is_null($dados))
				retornarJson(null, array('resposta' => true, 'chamados' => $dados ));
			else
				retornarJson(false);
		}
		else
			retornarJson(null);
	}
	function getChamadoRespostas()
	{
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 		= $CI->input->post('id');

		if($id != null && $id != ''){
			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte_mensagem WHERE suporte_id = {$id}")->result_array();

			if(!is_null($dados))
				retornarJson(null, $dados );
			else
				retornarJson(false);
		}
		else
			retornarJson(null);
	}

	function responderChamado() {
		$CI =& get_instance();
		$CI->load->helper("util_helper");
		$CI->load->model("padrao_model");
		$id 			= $CI->input->post('id');
		$msg 			= $CI->input->post('msg');
		$clienteId 		= $CI->input->post('cliente_id');

		$dados 		= array(
			'suporte_id' => $id,
			'conteudo' => $msg,
			'cliente_id' => $clienteId
		 );
		if($id != null && $id != ''){
			$inserirId 		= $CI->padrao_model->inserir("suporte_mensagem", $dados);

			$dados 		= $CI->padrao_model->buscar("SELECT * FROM suporte_mensagem WHERE id = {$inserirId}")->row_array();

			if(!is_null($dados))
				retornarJson(null, $dados );
			else
				retornarJson(false);
		}
		else
			retornarJson(null);

	}
