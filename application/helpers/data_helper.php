<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * converterData
 *
 * Transforma datas MySQL <> Brasil
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('converterData'))
{
	function converterData($string)
	{
		if(strstr($string,'/'))
		{
			$date = substr($string,6,4)."-".substr($string,3,2)."-".substr($string,0,2);
		}
		else if(strstr($string,'-'))
		{
			$date = substr($string,8,2)."/".substr($string,5,2)."/".substr($string,0,4);
		}
		else
		{
			$date = "Formato não aceito pela função.";
		}
		return $date;
	}	
}
/**
 * converterDataMascara
 *
 * Transforma datas MySQL <> Brasil
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('converterDataMascara'))
{
	function converterDataMascara($string)
	{
		$date = substr($string,4,4)."-".substr($string,2,2)."-".substr($string,0,2);
		return $date;
	}	
}

/**
 * Formatar Horário
 *
 * Transforma datas MySQL <> Brasil
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('formatarHorario'))
{
	function formatarHorario($string)
	{
		$string = substr($string, 11,5);
		return $string;
	}	
}


/**
 * Diferença entre datas em dias
 *
 * Transforma datas MySQL <> Brasil
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('dinferencaEntreDatasDias'))
{
	function dinferencaEntreDatasDias($dataInicial, $dataFinal)
	{
		$data_inicial 	= (strpos($dataInicial, "/") > -1)?converterData($dataInicial):substr($dataInicial, 0, 10);
		$data_final 	= (strpos($dataFinal, "/") > -1)?converterData($dataFinal):substr($dataFinal, 0, 10);

		$time_inicial = strtotime($data_inicial);
		$time_final = strtotime($data_final);
		$diferenca = $time_final - $time_inicial;
		$dias = (int)floor( $diferenca / (60 * 60 * 24));

		return $dias;
	}	
}

/**
 * mesPorExtenso
 *
 * Retorna um mês por extenso
 *
 * @access	public
 * @param	string
 * @return	str
 */
if (!function_exists('mesPorExtenso'))
{
	function mesPorExtenso($string)
	{
		$mes = array(
			"01" => "Janeiro",
			"02" => "Fevereiro",
			"03" => "Março",
			"04" => "Abril",
			"05" => "Maio",
			"06" => "Junho",
			"07" => "Julho",
			"08" => "Agosto",
			"09" => "Setembro",
			"10" => "Outubro",
			"11" => "Novembro",
			"12" => "Dezembro"
		);
		return $mes[$string];
	}	
}
if(!function_exists('dataPorExtenso')) {
	function dataPorExtenso($data) {
		setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese'); 
		date_default_timezone_set('America/Sao_Paulo');
		return utf8_encode(strftime('%A, %d de %B de %Y', strtotime($data)));
	}
}
