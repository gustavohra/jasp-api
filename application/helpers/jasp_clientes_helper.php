<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getClientes()
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$clientes 		= array();
 
		$tipo = $CI->input->post("tipo"); 
		$clientes 		= $CI->clientes_model->listarclientes()->result_array(); 
 
			retornarJson(null, $clientes);  
	}
	function getDispositivos()
	{  
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		$clientes 		= array();
 
		$tipo = $CI->input->post("tipo"); 
		$clientes 		= $CI->padrao_model->buscar("SELECT DIS.*, USU.nome FROM cliente_dispositivo DIS INNER JOIN cliente USU ON USU.id = DIS.cliente_id")->result_array(); 
 
			retornarJson(null, $clientes);  
	}
	function getclienteNota($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");

		$nota = array("nota" => 0);

		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$dados			= $CI->clientes_model->clienteNota($parametro["id"])->row();
			$nota["nota"]	= $dados->nota;
		}
		
		retornarJson(null, $nota);
	}
	
	function getClassificacoesPendentes($dados = null)
	{  

		$CI =& get_instance();
		$CI->load->model("clientes_model");

		if(!is_null($dados)){
		$parametro 		= json_decode($dados, true);
		$dados			= $CI->clientes_model->clienteNotasPendentes($parametro["id"])->result();

		if(!is_null($dados))
			retornarJson(null, $dados);
		}
		
		retornarJson(false);


	}
	function setNotaProfissional($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);
			//Altera o status da agenda
			$dados 			= array("natNota" 	=> $parametro["natNota"]);
			$condicao 		= array("natCodigo" => $parametro["natCodigo"]);
			$solicitacao 	= $CI->padrao_model->alterar("pro_notaAtendimento", $dados, $condicao);
			retornarJson(null, $solicitacao);
		}
		retornarJson(false);
	}

	function clienteAtualizarCadastro($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");

		if($dados != null){
			$cliente 					= json_decode($dados, true);
			unset($cliente['tipo']);
			
			if(isset($cliente["usuImagem"]) && strpos($cliente["usuImagem"], 'base64,') !== false)
				$cliente["usuImagem"] 	= uploadImagem64($cliente["usuImagem"], 'others/uploads/clientes/fotos');  

			 if(isset($cliente["novaSenha"])){
				$cliente["senha"] 		= $CI->encryption->encrypt($cliente["novaSenha"]);
				unset($cliente["novaSenha"]);
			}
			$condicao 					= array('email' => $cliente["email"]);
			if($CI->padrao_model->alterar("cliente", $cliente, $condicao)) {
				$cliente 			= $CI->clientes_model->clientePorCodigo($cliente["id"])->row_array();

				$conteudo 	= array(
							'nome'			=> $cliente["nome"],   
							'resumo' 			=> 'Alteração de perfil no JASP',
							'destinatario'		=> $cliente["email"],
							'assunto'			=> 'Alteração de perfil no aplicativo JASP');

					enviarEmail($conteudo, 'perfil-atualizado');

					retornarJson(null, array("resposta" => true, "cliente" => $cliente, 'tipoLogin' => 'cliente'));
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a alteração do perfil. Por favor, tente novamente." ));
		}
		else
			retornarJson(false);
	}
	/*
	/*
	* [Método]: atualizarPushId
	* [Descrição]: Atualiza ou insere registros para o enivo de mensagens PUSH
	* 
	* @author gustavoaguiar 
	* @param $dados
	* @return boolean
	*/
	function atualizarPushId($dados = null)
	{ 
		if($dados != null):
			$dados 		= json_decode($dados, true);
			$CI 		=& get_instance();
			$CI->load->model("padrao_model");

			$retorno 					= array();
			$condicao						= array("usdUuid" => $dados["usdUuid"]);

			// Verifica se o cliente já possui um ou mais dispositivos cadastrados com o mesmo código de identificação
			$dispositivos 					= $CI->padrao_model->buscar("SELECT usdCodigo FROM cliente_dispositivo WHERE usdUuid = '".$dados["usdUuid"]."'")->num_rows();

			// Caso exista, realiza a atualização do Push ID
			if($dispositivos > 0):
				if($CI->padrao_model->alterar("cliente_dispositivo", $dados, $condicao))
					retornarJson(true);
				else
					retornarJson(false);
			else:
				// Caso contrário, insere um novo dispositivo
				if($CI->padrao_model->inserir("cliente_dispositivo", $dados))
					retornarJson(true);
				else
					retornarJson(false);			
			endif;
		else:
			retornarJson(false);
		endif;
	}
 
	function enviarEmailSenha($dados)
	{ 
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");
 
		$cliente 							= $CI->clientes_model->clientePorEmail($dados["email"])->row_array();
 
		if($cliente != null){
			$data 							= new DateTime();
			$dataValidade 					= new DateTime();
			$dataValidade->modify('+1 day');

			$tokenSenha 					= substr(mt_rand(100000,999999), 0, 6);
			$dadosReset 					= array('urtToken' => $tokenSenha, 'urtValidade' => $dataValidade->format('Y-m-d H:i:s'), 'id' => $cliente["id"]);
			$condicaoReset 					= array('id' => $cliente["id"]);
			$condicaoResetDados 			= array('urtStatus' => 'Inativo');
			$CI->padrao_model->alterar('cliente_reset_senha', $condicaoResetDados, $condicaoReset);

				if($CI->padrao_model->inserir('cliente_reset_senha', $dadosReset)){
					$conteudo 	= array(
								'nome'			=> $cliente["nome"],  
								'token' 			=> $tokenSenha,
								'resumo' 			=> 'Alteração de senha no JASP',
								'destinatario'		=> $cliente["email"],
								'assunto'			=> 'Alteração de senha no aplicativo JASP');

					if(enviarEmail($conteudo, 'senha-solicitacao'))
						retornarJson(true);
					else
						retornarJson(false);
				}
				else
					retornarJson(false); 
		}
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "E-mail não cadastrado!" ));

	} 

	function recuperarSenha($dados)
	{

		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");

		$dados = json_decode($dados,true);

		if($dados != null)
			enviarEmailSenha($dados);
		else
			retornarJson(null, array("resposta" => false, "mensagem" => "Informações inválidas, por favor, tente novamente."));
	}

	function validarTokenRecuperacaoSenha($dados) {

		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");

		$dados = json_decode($dados, true);

		$token 	=	$dados["token"];

		if($token != null) {
			$dados 	= $CI->clientes_model->clienteValidarToken($token)->result_array(); 
			if($dados != null) {
				retornarJson(null, array('resposta' => true));
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}


	function alterarSenha($dados) {

		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");

		$dados = json_decode($dados, true);

		$token 			= $dados["token"];
		$novaSenha 		= $dados["novaSenha"];

		if($token != null) {
			$dadosToken 	= $CI->clientes_model->clienteValidarToken($token)->result_array(); 
			if($dadosToken != null) {
				$dadosAlteracao 			= array();
				$dadosAlteracao["senha"] = $CI->encryption->encrypt($novaSenha);

				$condicao 					= array('email' => $dados["email"]);
				if($CI->padrao_model->alterar("cliente", $dadosAlteracao, $condicao)) {
					retornarJson(null, array('resposta' => true));
				}
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}

	function getMeusCupons($dados = null){
		$CI =& get_instance();
		$CI->load->model("clientes_model");


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$dados			= $CI->clientes_model->cuponsPorcliente($parametro["id"])->result();
		}
		
		retornarJson(null, $dados);
	}
	function setusuoteAdesao($dados = null){
		$CI =& get_instance();
		$CI->load->model("clientes_model");


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$historico 		= $CI->clientes_model->historicoUtilizacaousuotePorCliente($parametro["id"],$parametro["pctCodigo"]);

			$CI->load->model("padrao_model");
			$CI->load->model("usuotes_model");

			$usuote = $CI->usuotes_model->usuotePorCodigo($parametro["pctCodigo"])->row();

			$dados = array(
				"id"			=>  $parametro["id"],
				"pctCodigo"			=>  $parametro["pctCodigo"],
				"papUtilizacoes"	=>  $usuote->pctUtilizacao,
				"papAdesao"			=>  date("Y-m-d H:i:s"),
				"papStatus"			=>  "ATIVO");

			$CI->padrao_model->inserir("cliente_usuote",$dados);
			retornarJson(null, array('resposta' => true));
		}
		else
			retornarJson(null, array('resposta' => false));
		
	}
	function getMeususuotes($dados = null){
		$CI =& get_instance();
		$CI->load->model("clientes_model");


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$dados			= $CI->clientes_model->usuotesPorcliente($parametro["id"])->result();
		}
		
		retornarJson(null, $dados);
	}
	function setConsultausuote($dados = null){
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		$CI->load->model("usuotes_model");;


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$usuote 		= $CI->usuotes_model->usuoteAssociacaoPorCodigo($parametro["papCodigo"])->row();
			//Altera total de utilizacoes do usuote
			$novoTotal		= $usuote->papUtilizacoes - 1;
			$novoStatus 	= ($novoTotal == 0)?"INATIVO":"ATIVO";
			$dados 			= array("papUtilizacoes" => $novoTotal, "papStatus" => $novoStatus);
			$condicao 		= array("papCodigo" => $parametro["papCodigo"]);
			$CI->padrao_model->alterar("cliente_usuote",$dados,$condicao);
			//Altera dados da agenda
			$dados 			= array("ageStatus" => "AUTORIZADO", "id" => $usuote->id, "ageTipoSolicitacao" => "usuOTE", "ageObservacao" => "Marcação ".($usuote->papUtilizacoes-1)." de ".$usuote->pctUtilizacao." contratadas.");
			$condicao 		= array("ageCodigo" => $parametro["ageCodigo"]);
			$CI->padrao_model->alterar("pro_agenda",$dados,$condicao);
		}
		
		retornarJson(true);
	}
	function setServicoAdesao($dados = null){
		$CI =& get_instance();
		$CI->load->model("servicos_model");


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$historico 		= $CI->servicos_model->servicoPorCodigoPorcliente($parametro["id"],$parametro["serCodigo"]);

			if($historico->num_rows() > 0){
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Ocorreu um problema. Você já aderiu à assinatura.'));
			}else{
				$CI->load->model("padrao_model");
				$CI->load->model("usuotes_model");

				$dados = array(
					"id"			=>  $parametro["id"],
					"serCodigo"			=>  $parametro["serCodigo"],
					"pasAdesao"			=>  date("Y-m-d H:i:s"),
					"pasStatus"			=>  "ATIVO");

				$CI->padrao_model->inserir("cliente_servico",$dados);

				retornarJson(null, array('resposta' => true));
			}
		}
		else
				retornarJson(null, array('resposta' => false));
		
	}
	function getMeusServicos($dados = null){
		$CI =& get_instance();
		$CI->load->model("clientes_model");

		$dados 					= montarCamposPost(['id']);
		$dados			= $CI->clientes_model->servicosPorcliente($dados["id"])->result_array();

		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	function setConsultaServicos($dados = null){
		$CI =& get_instance();
		$CI->load->model("padrao_model");
		$CI->load->model("servicos_model");;


		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$servico 		= $CI->servicos_model->servicoAssociacaoPorCodigo($parametro["pasCodigo"])->row();
			$historico 		= $CI->servicos_model->servicoUtilizacoesNoMes($parametro["pasCodigo"])->row();

			//Verifica total do mês
			if($historico->total >= $servico->serUtilizacao){
				retornarJson(false);
			}else{
				//Altera dados da agenda
				$contador = 	$historico->total+1;
				$dados 			= array("ageStatus" => "AUTORIZADO", "id" => $servico->id, "ageTipoSolicitacao" => "SERVICO", "ageObservacao" => $contador."ª marcação no mês");
				$condicao 		= array("ageCodigo" => $parametro["ageCodigo"]);
				$CI->padrao_model->alterar("pro_agenda",$dados,$condicao);
				//Acrescenta dados no histórico de utilizações
				$dados = array(
					"pasCodigo"			=>  $parametro["pasCodigo"],
					"pshHorario"		=>  date("Y-m-d H:i:s"));

				$CI->padrao_model->inserir("pro_servico_historico",$dados);

				retornarJson(true);
			}
		}	
	}
	function getTransacoes($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");

		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);

			$dados			= $CI->clientes_model->clienteTransacoes($parametro["id"])->result();
		}
		
		retornarJson(null, $dados);
	}
	function validarTransacao($dados = null)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");

		if(!is_null($dados)){
			$parametro 		= json_decode($dados, true);
			$dados			= $CI->clientes_model->transacaoPorToken($parametro["token"]);

			//Caso não existam transações
			if($dados->num_rows() == 0){
				retornarJson(null, array("mensagem" => "Transação inexistente."));
			}
			if($dados->num_rows() > 0){
				$dados = $dados->row();
				//Caso o valor da transação seja diferente do valor informado
				if($dados->traValor != $parametro["valor"]){
					retornarJson(null, array("mensagem" => "O valor informado é diferente do valor da transação."));
				}
				//Caso o valor da transação seja diferente do valor informado
				if($dados->traStatus == "Estornado"){
					retornarJson(null, array("mensagem" => "A transação sofreu estorno por parte do cliente e não poderá ser realizada."));
				}
			}
		}
		
		retornarJson(null, array("mensagem" => true));
	}


	  function excluirCliente ( )
	{	
		$CI =& get_instance(); 
		$CI->load->model('padrao_model'); 
		$dados 								= array();  
		$tabela 							= "cliente";
		$dados['status']	 				= 'EXCLUIDO';
		$dados['id']					=  $CI->input->post("id"); 
		$mensagemRetorno 					= 'Linha excluída com sucesso!';
		$mensagemRetornoErro 				= 'Não foi possível realizar a remoção da linha.';
  
		if(isset($dados['id'])){ 		
			$condicao = array('id' => $dados['id']);
			if($CI->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
	}



	 function setCliente ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 		= montarCamposPost(['id', 'nome', 'sobrenome', 'email', 'sexo', 'nascimento', 'telefone', 'cpf']);

			if(!isset($dados['id'])){
					if(isValidEmail($dados['email'])){
							$senha 					= random_string(5);
							$dados['senha'] 		= cifracub3_256($senha);
							sendWelcomeEmail($dados,$senha);
							if($CI->padrao_model->inserir("cliente", $dados) > 0){
								retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => 'Cliente inserido com sucesso'));
							}else{
								retornarJson(null, array('resposta' => false, 'mensagem' => 'Não foi possível realizar o cadastro do cliente.'));
							}
							}else{
						retornarJson(null, array('resposta' => false, 'mensagem' => 'O e-mail informado já existe em nossa base de dados. Tente outro e-mail.'));
					}
			}	
			else {		
				$mensagemRetorno 		= 'Cliente alterado com sucesso!';
				$condicao 				= array('id' => $dados['id']);
				if($CI->padrao_model->alterar("cliente", $dados, $condicao) > 0) 
					retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno)); 
				else
					retornarJson(false);
			}	
		
	}
 	function sendWelcomeEmail($dados,$senha){
 		$conteudo = array(
			'mensagem' 			=> '<tr>
							            <td align="center">
							                <table width="720px" cellsusuing="0" cellpadding="3" class="container">
							                    <tr>
							                        <td style="font-size: 14px; padding-top: 20px">
							                        <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["nome"].'</strong>! Você acabou de ser registrado no JASP. Uma senha aleatória foi criada que você acesse seu cadastro:<br /><br /></p>
							                        <p style="text-align:center"><a style="background-color: #fef37e; color: #30343F; text-decoration: none; text-transform: uppercase; padding: 10px; font-weight: bold;" href>'.$senha.'</a></p>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>',
			'destinatario'		=> $dados["email"],
			'assunto'			=> 'Seja bem vindo ao JASP');

		return enviarEmail($conteudo, 'modelo-padrao');
 	}


	function getCliente()
	{
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$dados 		= $CI->clientes_model->clientePorCodigo($CI->input->post("id"))->row_array();

		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}
	function isValidEmail($email){
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$dados 		= $CI->clientes_model->clientePorEmail($email);

		if($dados->num_rows() > 0)
			return false;
		else
			return true;
	}
	function isAssinaturaValida(){
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$CI->load->model("clientes_model");
		$dados 	= montarCamposPost(['id', 'proCodigo', 'palCodigo']);

		if($CI->profissionais_model->listarDadosLocalAtendimento($dados['palCodigo'])->num_rows() == 0 || empty($CI->profissionais_model->listarDadosLocalAtendimento($dados['palCodigo'])->row()->palValorAssinatura)){
			retornarJson(null, array('resposta' => 1));
		}else if($CI->clientes_model->isClienteAssinante($dados['id'],$dados['palCodigo'])->num_rows() > 0){
			retornarJson(null, array('resposta' => 3));
		}else{
			retornarJson(null, array('resposta' => 2));
		}
	}