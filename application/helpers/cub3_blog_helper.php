<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('getCategoriasJson'))
{
	function getCategoriasJson($json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListarCategorias()->result_array();
		$retorno		 					= $dadosBlog;

		if(is_null($retorno))
           redirect('erro404');
       else{
		 if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
       }
	}
}
if ( ! function_exists('getSlidersJson'))
{
	function getSlidersJson($json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListarSliders()->result_array();
		$retorno		 					= $dadosBlog;

		if(is_null($retorno))
           redirect('erro404');
       else{
		 if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
       }
	}
}
 
if ( ! function_exists('getPostsJson'))
{
	function getPostsJson()
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListar();
		$retorno		 					= $dadosBlog->result_array();
 
		retornarJson(null, $retorno);
	}
}
 
if ( ! function_exists('getCategoriasSelect'))
{
	function getCategoriasSelect()
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListarCategoriasSelect()->result_array();
		$retorno		 					= $dadosBlog;
		 
		retornarJson(null, $retorno);
	}
}
 
 
if ( ! function_exists('getSubCategorias'))
{
	function getSubCategorias($bcaCodigo, $json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListarSubCategorias($bcaCodigo);
		$retorno		 					= $dadosBlog->result_array();
		 
		 if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
	}
}
 
if ( ! function_exists('getPostCategoriasJson'))
{
	function getPostCategoriasJson($bpoCodigo = null, $json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
 		if($bpoCodigo != null){
			$dadosBlog 			= $CI->cub3_blog_model->blogListarCategoriasPorBpoCodigo($bpoCodigo);
			$retorno		 	= $dadosBlog->result_array();
 		}
 		else
 			$retorno = null;
		 
		 if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
	}
} 

if ( ! function_exists('getPostVisualizacoesJson'))
{
	function getPostVisualizacoesJson($bpoCodigo = null)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
 		if($bpoCodigo != null){
			$dadosBlog 			= $CI->cub3_blog_model->blogListarVisualizacoesPorBpoCodigo($bpoCodigo);
			$retorno		 	= $dadosBlog->row_array();
 		}
 		else
 			$retorno = null;
		 
		retornarJson(null, $retorno);
	}
} 

if ( ! function_exists('getPostJson'))
{
	function getPostJson($slug = '')
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
 		if($slug != ''){
			$dadosBlog 			= $CI->cub3_blog_model->blogListarPorSlug($slug);
			$retorno		 	= $dadosBlog->row_array();
 		}
 		else
 			$retorno = null;
		 
		retornarJson(null, $retorno);
	}
}
if ( ! function_exists('getNoticiasMaisVisualizadas'))
{
	function getNoticiasMaisVisualizadas()
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 			= $CI->cub3_blog_model->blogListarPorVisualizacoes();
		$retorno		 	= $dadosBlog->result_array();

 
		retornarJson(null, $retorno);
	}
}
 
if ( ! function_exists('getPostPorCategoria'))
{
	function getPostPorCategoria($qtd = 5, $categoria = null)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 			= $CI->cub3_blog_model->blogListarPorCategoria($qtd, $categoria);
		$retorno		 	= $dadosBlog->result_array();

		retornarJson(null, $retorno);
	}
}
 
 
if ( ! function_exists('getTags'))
{
	function getTags()
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$tags 				= $CI->cub3_blog_model->blogListarTags();
		$retorno		 	= $tags->result_array();

		retornarJson(null, $retorno);
	}
}
 
if ( ! function_exists('getBlogJson'))
{
	function getBlogJson($bpoCodigo = null, $json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 			= $CI->cub3_blog_model->blogListarPorCodigo($bpoCodigo);
		
		$retorno		 	= $dadosBlog->row_array();
		
 		if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
	}
}
 
if ( ! function_exists('getBlogPorSlugJson'))
{
	function getBlogPorSlugJson($bpoSlug = null)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 			= $CI->cub3_blog_model->blogListarPorSlug($bpoSlug);

		$retorno		 	= $dadosBlog->row_array();

		retornarJson(null, $retorno);
	}
}
 
if ( ! function_exists('getBlogCategoriaPorCodigo'))
{
	function getBlogCategoriaPorCodigo($bcaCodigo = null, $json = true)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$dadosBlog 							= $CI->cub3_blog_model->blogListarCategoriaPorCodigo($bcaCodigo)->row_array();
		if(isset($dadosBlog["bcaCodigo"]))
			$dadosBlog["subCategorias"] 		= $CI->cub3_blog_model->blogListarSubCategorias($dadosBlog["bcaCodigo"])->result_array();

		$retorno		 					= $dadosBlog;
 
		 if($json)
			retornarJson(null, $retorno);
		else
			return $retorno;
	}
}
if ( ! function_exists('blogCategoriaValidarSlug'))
{
	function blogCategoriaValidarSlug($bcaSlug = '')
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");

		if($bcaSlug != ''){
			$verificarSlug 		= $CI->cub3_blog_model->blogListarCategoriaPorSlug($bcaSlug)->num_rows();

			if(Count($verificarSlug) > 0)
				return $bcaSlug;
			else {
				// Caso o slug já exista, acrescenta um valor de timestamp unix
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $bcaSlug."-".$unix;
			}
		}
		else{
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $unix;
		}
	}
}
 
 
 
if ( ! function_exists('blogValidarSlug'))
{
	function blogValidarSlug($bpoSlug = '')
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		if($bpoSlug != ''){
			$verificarSlug 		= $CI->cub3_blog_model->blogListarPorSlug($bpoSlug)->num_rows();

			if($verificarSlug == 0)
				return $bpoSlug;
			else {
				// Caso o slug já exista, acrescenta um valor de timestamp unix
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $bpoSlug."-".$unix;
			}
		}
		else{
				$now = time();
				$human = unix_to_human($now);
				$unix = human_to_unix($human);

				return $unix;
		}
	}
}
 
 
 
 
if ( ! function_exists('tratarCategorias'))
{
	function tratarCategorias($categorias, $bpoCodigo)
	{
		if($categorias != null) {
			$categoriasArray 			   = array();
			for ($i=0; $i < Count($categorias); $i++) { 
				$auxCategoria = array();
				$auxCategoria["bcaCodigo"] = $categorias[$i]["value"];
				$auxCategoria["bpoCodigo"] = $bpoCodigo;
				array_push($categoriasArray, $auxCategoria);
			}
			return $categoriasArray;
		}
		else
			return null;
	}
}
 
if ( ! function_exists('adicionarVisualizacaoPost'))
{
	function adicionarVisualizacaoPost($bpoCodigo)
	{
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$CI->load->model("padrao_model");
		$ip 	= getIp();
		$agente = json_encode($_SERVER['HTTP_USER_AGENT']);
		$dados 	= array('bpoCodigo' => $bpoCodigo, 'bpvEnderecoIp' => $ip, 'bpvAgente' => $agente);
		if($CI->padrao_model->inserir('cub3_blog_posts_visualizacao', $dados) > 0){
			$CI->cub3_blog_model->adicionarVisualizacao($bpoCodigo);
			return true;
		}
		else
			return false;
	}
}

if ( ! function_exists('salvarCachePosts'))
{
	function salvarCachePosts($dados)
	{ 
		if($dados != null){
			$dados 			= json_encode($dados, JSON_PRETTY_PRINT);
			$arquivo 		= fopen("./cache/posts/posts.json", "w");
			
			if($dados != false){
				try {
					fwrite($arquivo, $dados);
					fclose($arquivo);
				} catch (Exception $e) {
					return false;
				}	finally {
				   return true;
				}
			}
			else
				return false;
		}
		else
			return false;
	}
}


if ( ! function_exists('salvarCacheTags'))
{
	function salvarCacheTags($dados)
	{ 
		if($dados != null){
			$dados 			= json_encode($dados, JSON_PRETTY_PRINT);
			$arquivo 		= fopen("./cache/posts/tags.json", "w");
			
			if($dados != false){
				try {
					fwrite($arquivo, $dados);
					fclose($arquivo);
				} catch (Exception $e) {
					return false;
				}	finally {
				   return true;
				}
			}
			else
				return false;
		}
		else
			return false;
	}
}

if ( ! function_exists('criarCachePostsGeral'))
{
	function criarCachePostsGeral()
	{ 
		$CI =& get_instance();
		$CI->load->model("cub3_blog_model");
		$posts 			= $CI->cub3_blog_model->blogListar()->result_array();
		$tags 			= $CI->cub3_blog_model->blogListarTags()->row_array();
		$retorno 		= 0; 
 
		if(salvarCachePosts($posts) && salvarCacheTags($tags))
			retornarJson(null, array('resposta' => true, 'mensagem' => 'Cache criado com sucesso!' ));
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => 'Não foi possível criar o cache.' ));
	}
}




