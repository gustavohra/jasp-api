<?php

function getDashboard() {

    	$CI =& get_instance();
    	$CI->load->model("padrao_model");	

		$dados["qtdClientes"]		= $CI->padrao_model->buscar("SELECT COUNT(*) as qtd FROM cliente")->row_array()["qtd"];
		$dados["qtdDispositivos"]	= $CI->padrao_model->buscar("SELECT COUNT(*) as qtd FROM cliente_dispositivo")->row_array()["qtd"];
		$dados["qtdConteudo"]		= $CI->padrao_model->buscar("SELECT COUNT(*) as qtd FROM pro_profissional")->row_array()["qtd"];
		$dados["mensagens"]			= array( );

		retornarJson($dados);
}

function getCadastros() {

    	$CI =& get_instance();
    	$CI->load->model("padrao_model");	

    	$dados = "
	SELECT 
    SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
    SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
    SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
    SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
    SUM(IF(month = 'May', total, 0)) AS 'May',
    SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
    SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
    SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
    SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
    SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
    SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
    SUM(IF(month = 'Dec', total, 0)) AS 'Dec' 
    FROM (
SELECT DATE_FORMAT(cliente.horario, \"%b\") AS month, COUNT(cliente.id) as total
FROM cliente
WHERE cliente.horario <= NOW() and cliente.horario >= Date_add(Now(),interval - 12 month)
GROUP BY DATE_FORMAT(cliente.horario, \"%m-%Y\")) as sub";
	$sql = $CI->padrao_model->buscar($dados)->row_array();
    $retorno =array();

    foreach ($sql as $key => $value) {
        $aux = [];
        $aux['label'] = $key;
        $aux['value'] = $value;
        array_push($retorno, $aux);
        
    }

retornarJson(null, $retorno);
}

function getUsuarios() {
        $CI =& get_instance();
        $CI->load->model("cub3_perfil_model");   
        $CI->load->model("cub3_perfil_model");
        $dados = $CI->cub3_perfil_model->perfilListarTodos()->result_array();
        retornarJson(null, $dados);

}


      function excluirUsuario ( )
    {   
        $CI =& get_instance(); 
        $CI->load->model('padrao_model'); 
        $dados                              = array();  
        $tabela                             = "cub3_usuario";
        $dados['usuStatus']                 = 'EXCLUIDO';
        $dados['id']                    =  $CI->input->post("id"); 
        $mensagemRetorno                    = 'Linha excluída com sucesso!';
        $mensagemRetornoErro                = 'Não foi possível realizar a remoção da linha.';
  
        if(isset($dados['usuCodigo'])){        
            $condicao = array('usuCodigo' => $dados['usuCodigo']);
            if($CI->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
                retornarJson(null, array('resposta' => true, 'mensagem' => $mensagemRetorno)); 
            else
                retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
        }
        else
                retornarJson(null, array('resposta' => false, 'mensagem' => $mensagemRetornoErro));
    }



     function setUsuario ()
    {
        $CI =& get_instance();
        $CI->load->model('padrao_model');
        $CI->load->library("encryption");
        $dados      = montarCamposPost(['usuCodigo', 'usuNome', 'usuSobrenome', 'usuEmail', 'usuSexo', 'usuNascimento', 'usuTelefone', 'usuCpf']);

            if(!isset($dados['usuCodigo'])){
                    if(isValidEmail($dados['usuEmail'])){

                            $senha                  = strtoupper(random_string(5));
                            $dados['usuSenha']         = $CI->encryption->encrypt($senha);
                            sendWelcomeEmail($dados,$senha);
                            $id = $CI->padrao_model->inserir("cub3_usuario", $dados);

                            if($id > 0){
                                $CI->padrao_model->inserir('cub3_grupo_has_usuario', array('gruCodigo' => 2, 'usuCodigo' => $id ));
                                retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => 'Usuario inserido com sucesso'));
                            }else{
                                retornarJson(null, array('resposta' => false, 'mensagem' => 'Não foi possível realizar o cadastro do Usuario.'));
                            }
                            }else{
                        retornarJson(null, array('resposta' => false, 'mensagem' => 'O e-mail informado já existe em nossa base de dados. Tente outro e-mail.'));
                    }
            }   
            else {      
                $mensagemRetorno        = 'Usuario alterado com sucesso!';
                $condicao               = array('usuCodigo' => $dados['usuCodigo']);
                if($CI->padrao_model->alterar("cub3_usuario", $dados, $condicao) > 0) 
                    retornarJson(null, array('resposta' => true, 'url' => null, 'mensagem' => $mensagemRetorno, 'titulo' => 'Novo usuário')); 
                else
                    retornarJson(false);
            }   
        
    }

    function getUsuario()
    {
        $CI =& get_instance();
        $CI->load->model("cub3_perfil_model");
        $dados      = $CI->cub3_perfil_model->perfilSelecionarCodigo($CI->input->post("usuCodigo"))->row_array();

        if(!is_null($dados))
            retornarJson(null, $dados);
        else
            retornarJson(null, array('resposta' => false ));
    }
    function isValidEmail($email){
        $CI =& get_instance();
        $CI->load->model("cub3_perfil_model");
        $dados      = $CI->cub3_perfil_model->perfilSelecionarEmail($email);

        if($dados->num_rows() > 0)
            return false;
        else
            return true;
    }
    function sendWelcomeEmail($dados,$senha){
        $conteudo = array(
            'mensagem'          => '<tr>
                                        <td align="center">
                                            <table width="720px" cellsusuing="0" cellpadding="3" class="container">
                                                <tr>
                                                    <td style="font-size: 14px; padding-top: 20px">
                                                    <p align="justify" style="font-size: 16px; padding-top: 5px;color: #30343F">Olá <strong>'.$dados["usuNome"].'</strong>! Você acabou de ser registrado no JASP. Uma senha aleatória foi criada que você acesse seu cadastro:<br /><br /></p>
                                                    <p style="text-align:center"><a style="background-color: #fef37e; color: #30343F; text-decoration: none; text-transform: uppercase; padding: 10px; font-weight: bold;" href>'.$senha.'</a></p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>',
            'destinatario'      => $dados["usuEmail"],
            'assunto'           => 'Seja bem vindo à plataforma JASP');

        return enviarEmail($conteudo, 'modelo-padrao');
    }



function solicitarCodigo($dados) {

        $CI =& get_instance();
        $CI->load->model("cub3_usuario_model");
        $CI->load->model("padrao_model");
        $CI->load->helper("cub3_email_helper");
   
        $dados = json_decode($dados, true); 
        $usuario                            = $CI->cub3_usuario_model->usuarioPorEmail($dados["email"])->row_array();
 
        if($usuario != null){
            $codigo                         = $usuario['usuCodigo'];
            $nome                           = $usuario['usuNome'];
            $email                          = $usuario['usuEmail'];
            $data                           = new DateTime();
            $dataValidade                   = new DateTime();
            $dataValidade->modify('+1 day');

            $tokenSenha                     = substr(mt_rand(100000,999999), 0, 6);
            $dadosReset                     = array('urtToken' => $tokenSenha, 'urtValidade' => $dataValidade->format('Y-m-d H:i:s'), 'id' => $codigo );
            $condicaoReset                  = array('id' => $codigo);
            $condicaoResetDados             = array('urtStatus' => 'Inativo'); 
            $CI->padrao_model->alterar('reset_senha', $condicaoResetDados, $condicaoReset);

                if($CI->padrao_model->inserir('reset_senha', $dadosReset)){

                    $html       = "<center>
                                    <p>Uma solicitação de nova senha foi realizada para o seu login. Caso não tenha sido você, por favor, desconsidere este e-mail.</p>
                                    <p>O código de confirmação é:</p>
                                    <p><span style=\"margin-top:15px;width:200px;padding:10px 50px;text-align:center;background:none;background-color:#00a399;border:0px;font-weight:bold;color:#fff;\">{$tokenSenha}</span></p>
                                    </center>";
                    $conteudo   = array(
                                'pacNome'           => $nome,  
                                'token'             => $tokenSenha,
                                'resumo'            => 'Alteração de senha no JASP',
                                'destinatario'      => $email,
                                'mensagem'          => $html,
                                'assunto'           => 'Alteração de senha no JASP');


                    if(enviarEmail($conteudo ))
                        retornarJson(null,  array('resposta' => true));
                    else
                        retornarJson(false);
                }
                else
                    retornarJson(false); 
        }
        else
            retornarJson(null, array('resposta' => false, 'mensagem' => "E-mail não cadastrado!" ));
}


    function validarCodigo($dados) {

        $CI =& get_instance();
        $CI->load->model("cub3_usuario_model");
        $CI->load->model("padrao_model");
        $CI->load->library('encryption');   


        $dados = json_decode($dados,true);

        if($dados['codigo'] != null) {
            $dadosToken     = $CI->cub3_usuario_model->usuarioValidarToken($dados['codigo'], $dados['email'])->result_array(); 
            if($dadosToken != null) { 
                if($dadosToken != null) {
                    $dadosAlteracao             = array();
                    $dadosAlteracao["usuSenha"] = $CI->encryption->encrypt($dados['novaSenha']);

                    $condicao                   = array('usuEmail' => $dados["email"]);
                    if($CI->padrao_model->alterar("cub3_usuario", $dadosAlteracao, $condicao)) {
                        retornarJson(null, array('resposta' => true));
                    }
                }
                else
                    retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
            }
            else
                retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
        }
        else
                retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
    }



function atualizarPerfil($dados) {

        $CI =& get_instance();
        $CI->load->model("cub3_perfil_model");
        $CI->load->model("padrao_model");

        if($dados != null){
            $usuario                    = json_decode($dados, true);
            unset($usuario['tipo']);
            
            if(isset($usuario["usuImagem"]) && strpos($usuario["usuImagem"], 'base64,') !== false)
                $usuario["usuImagem"]   = uploadImagem64($usuario["usuImagem"], 'others/uploads/usuarios/fotos');  

             if(isset($usuario["novaSenha"])){
                $usuario["usuSenha"]       = $CI->encryption->encrypt($usuario["novaSenha"]);
                unset($usuario["novaSenha"]);
                unset($usuario["novaSenha2"]);
            }
            $condicao                   = array('usuEmail' => $usuario["usuEmail"]);
            if($CI->padrao_model->alterar("cub3_usuario", $usuario, $condicao)) {
                $usuario            = $CI->cub3_perfil_model->perfilSelecionarCodigo($usuario["usuCodigo"])->row_array();

                $conteudo   = array(
                            'nome'          => $usuario["usuNome"],   
                            'resumo'            => 'Alteração de perfil no JASP',
                            'destinatario'      => $usuario["usuEmail"],
                            'assunto'           => 'Alteração de perfil na plataforma JASP');

                    enviarEmail($conteudo, 'perfil-atualizado');

                    retornarJson(null, array("resposta" => true, "usuario" => $usuario, 'tipoLogin' => 'usuario'));
            }
            else
                retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível realizar a alteração do perfil. Por favor, tente novamente." ));
        }
        else
            retornarJson(false);
}