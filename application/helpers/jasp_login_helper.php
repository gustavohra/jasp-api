<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('clienteRealizarLogin'))
{
	function clienteRealizarLogin($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		if($dados != null){
 			
 			if(!isset($dados['login']))
				$dados 		= json_decode($dados, true);

			$cliente 				= $CI->clientes_model->clienteValidarLogin($dados["login"])->row_array();

			if(!is_null($cliente)){
				$cliente['tipo'] 	= 'cliente';
				// Verifica se a senha é válida 
				$validarSenha 		= $CI->encryption->decrypt($cliente["senha"]) == $dados["senha"] ? true : false; 
				if($validarSenha){ 
					retornarJson(null, array("resposta" => true, "cliente" => $cliente,   "sessao" => $CI->session->userdata));
				}
				else
					retornarJson(null, array("resposta" => false, "mensagem" => "Senha incorreta, por favor, verifique os dados inseridos."));	
			}
			else
				retornarJson(null, array("resposta" => false, "mensagem" => "Usuário e senha incorretos, por favor, verifique os dados inseridos."));
		}
		else
			retornarJson(false);
	}
}

if ( ! function_exists('clienteRealizarLoginFacebook'))
{
	function clienteRealizarLoginFacebook($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");
		if($dados != null){ 

			$cliente 					= $CI->clientes_model->clienteValidarLoginFacebook($dados)->row_array();

			// Armazena apenas o token de acesso. As demais variáveis foram preservadas para futuras necessidades
			$dados["pacAuthFacebook"]	= $dados["pacAuthFacebook"]["accessToken"]; 


			if(!is_null($cliente)){
					retornarJson(null, array("resposta" => true, "cliente" => $cliente, 'tipoLogin' => 'cliente',  "sessao" => $CI->session->userdata));
			}
			else{
				$CI->padrao_model->inserir("cliente", $dados);
    
    			$cliente 			= $CI->clientes_model->clientePorCodigo($CI->db->insert_id())->row_array();
				retornarJson(null, array("resposta" => true, "cliente" => $cliente, 'tipoLogin' => 'cliente',   "sessao" => $CI->session->userdata));
			}
		}
		else
			retornarJson(false);
	}
}

if ( ! function_exists('clienteRealizarCadastro'))
{
	function clienteRealizarCadastro($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model");
		if($dados != null){ 

 			if(!isset($dados['email']))
				$dados 		= json_decode($dados, true);

			// Verifica se o cliente existe
			$cliente 					= $CI->clientes_model->clienteValidarLogin($dados["email"])->row_array();

			if(!is_null($cliente)){
					retornarJson(null, array("resposta" => false, "mensagem" => "E-mail já cadastrado!"));
			}
			else{
				if(isset($dados["imagem"]))
					$dados["imagem"] = uploadImagem64($dados["imagem"], 'others/uploads/clientes/fotos/'); 
				
				unset($dados["termosDeUso"]);

				$dados["senha"] 		= $CI->encryption->encrypt($dados["senha"]);
				$id = $CI->padrao_model->inserir("cliente", $dados);
     

    			$cliente 			= $CI->clientes_model->clientePorCodigo($id)->row_array();
    			$cliente['tipo'] 	= 'cliente'; 

				retornarJson(null, array("resposta" => true, "cliente" => $cliente,  "sessao" => $CI->session->userdata)); 
			}
		}
		else
			retornarJson(false);
	}
}

if ( ! function_exists('profissionalRealizarLogin'))
{
	function profissionalRealizarLogin($dados)
	{  
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$dados 		= json_decode($dados, true);

		if($dados != null && $dados["senha"] != '' && $dados["login"] != ''){ 

			$prof 				= $CI->profissionais_model->profissionalValidarLogin($dados["login"])->row_array();
			if(!is_null($prof)){
 
				$prof['tipo'] 	= 'profissional'; 
				// Verifica se a senha é válida
				$validarSenha 		= $CI->encryption->decrypt($prof["proSenha"]) == $dados["senha"] ? true : false;
				if($validarSenha)
					retornarJson(null, array("resposta" => true, "profissional" => $prof, "sessao" => $CI->session->userdata));
				else
					retornarJson(null, array("resposta" => false, "mensagem" => "Senha incorreta, por favor, verifique os dados inseridos."));	
			}
			else
				retornarJson(null, array("resposta" => false, "mensagem" => "Usuário e senha incorretos, por favor, verifique os dados inseridos."));
		}
		else
			retornarJson(null, array("resposta" => false, "mensagem" => "Usuário e senha incorretos, por favor, verifique os dados inseridos."));
	}
}


	function recuperarSenha($dados)
	{ 
		$CI =& get_instance();
		$CI->load->model("clientes_model");
		$CI->load->model("padrao_model"); 
 
 			if(!isset($dados['login']))
				$dados 		= json_decode($dados, true);

		$cliente 							= $CI->clientes_model->clientePorEmail($dados["login"])->row_array();
  
			$codigo = $cliente['id'];
			$nome = $cliente['nome'];
			$tipo = 'cliente';
			$email = $cliente['email']; 

		if($cliente != null){
			$data 							= new DateTime();
			$dataValidade 					= new DateTime();
			$dataValidade->modify('+1 day');

			$tokenSenha 					= substr(mt_rand(100000,999999), 0, 6);
			$dadosReset 					= array('urtToken' => $tokenSenha, 'urtValidade' => $dataValidade->format('Y-m-d H:i:s'), 'id' => $codigo, 'tipo' => $tipo);
			$condicaoReset 					= array('id' => $codigo);
			$condicaoResetDados 			= array('urtStatus' => 'Inativo'); 
 
			$CI->padrao_model->alterar('reset_senha', $condicaoResetDados, $condicaoReset);

				if($CI->padrao_model->inserir('reset_senha', $dadosReset)){
					$conteudo 	= array(
								'nome'			=> $nome,  
								'token' 			=> $tokenSenha,
								'resumo' 			=> 'Alteração de senha no JASP',
								'destinatario'		=> $email,
								'mensagem' 			=> "Token: {$tokenSenha}",
								'assunto'			=> 'Alteração de senha no aplicativo JASP');

					if(enviarEmail($conteudo))
			retornarJson(null, array('resposta' => true, 'mensagem' => "Por favor, verifique sua caixa de entrada!" ));
					else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível reiniciar sua senha. Por favor, entre em contato com o suporte técnico." ));
				}
				else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível reiniciar sua senha. Por favor, entre em contato com o suporte técnico." ));
		}
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "E-mail não cadastrado!" ));

	} 
	function recuperarSenhaProfissional($dados)
	{ 
		$CI =& get_instance();
		$CI->load->model("profissionais_model");
		$CI->load->model("padrao_model"); 
 
 			if(!isset($dados['login']))
				$dados 		= json_decode($dados, true);

		$profissional 							= $CI->profissionais_model->profissionalPorEmail($dados["login"])->row_array();
  
			$codigo = $profissional['proCodigo'];
			$nome = $profissional['proNome'];
			$tipo = 'profissional';
			$email = $profissional['proEmail']; 

		if($profissional != null){
			$data 							= new DateTime();
			$dataValidade 					= new DateTime();
			$dataValidade->modify('+1 day');

			$tokenSenha 					= substr(mt_rand(100000,999999), 0, 6);
			$dadosReset 					= array('urtToken' => $tokenSenha, 'urtValidade' => $dataValidade->format('Y-m-d H:i:s'), 'id' => $codigo, 'tipo' => $tipo);
			$condicaoReset 					= array('id' => $codigo);
			$condicaoResetDados 			= array('urtStatus' => 'Inativo'); 
 
			$CI->padrao_model->alterar('reset_senha', $condicaoResetDados, $condicaoReset);

				if($CI->padrao_model->inserir('reset_senha', $dadosReset)){
					$conteudo 	= array(
								'nome'			=> $nome,  
								'token' 			=> $tokenSenha,
								'resumo' 			=> 'Alteração de senha no JASP',
								'destinatario'		=> $email,
								'mensagem' 			=> "Token: {$tokenSenha}",
								'assunto'			=> 'Alteração de senha no aplicativo JASP');

					if(enviarEmail($conteudo))
			retornarJson(null, array('resposta' => true, 'mensagem' => "Por favor, verifique sua caixa de entrada!" ));
					else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível reiniciar sua senha. Por favor, entre em contato com o suporte técnico." ));
				}
				else
			retornarJson(null, array('resposta' => false, 'mensagem' => "Não foi possível reiniciar sua senha. Por favor, entre em contato com o suporte técnico." ));
		}
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => "E-mail não cadastrado!" ));

	} 
 
	function validarTokenRecuperacaoSenha($dados) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");

		$dados = json_decode($dados, true);

		$token 	=	$dados["token"];

		if($token != null) {
			$validar 	= $CI->padrao_model->buscar("SELECT * FROM reset_senha WHERE urtToken = '{$token}' AND urtStatus = 'Ativo' LIMIT 0,1")->result_array(); 

			if($validar != null) { 
				alterarSenha($dados);
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}

 
	function validarTokenRecuperacaoSenhaProfissional($dados) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");

		$dados = json_decode($dados, true);

		$token 	=	$dados["token"];

		if($token != null) {
			$validar 	= $CI->padrao_model->buscar("SELECT * FROM reset_senha WHERE urtToken = '{$token}' AND urtStatus = 'Ativo' LIMIT 0,1")->result_array(); 

			if($validar != null) { 
				alterarSenhaProfissional($dados);
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}

 


	function alterarSenha($dados) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");
			$CI->load->library('encryption');	


		$token 			= $dados["token"];
		$novaSenha 		= $dados["senha"];

		if($token != null) {
			$dadosToken 	= $CI->padrao_model->buscar("SELECT * FROM reset_senha WHERE urtToken = '{$token}' AND urtStatus = 'Ativo' LIMIT 0,1")->row_array(); 

			if($dadosToken != null) {
				$dadosAlteracao 			= array(); 
				$dadosAlteracao["senha"]  	= $CI->encryption->encrypt($novaSenha);

					$condicao 					= array('id' => $dadosToken["id"]);
					if($CI->padrao_model->alterar("cliente", $dadosAlteracao, $condicao)) {
						retornarJson(null, array('resposta' => true));
					} 
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}


	function alterarSenhaProfissional($dados) {

		$CI =& get_instance(); 
		$CI->load->model("padrao_model");
			$CI->load->library('encryption');	


		$token 			= $dados["token"];
		$novaSenha 		= $dados["senha"];

		if($token != null) {
			$dadosToken 	= $CI->padrao_model->buscar("SELECT * FROM reset_senha WHERE urtToken = '{$token}' AND urtStatus = 'Ativo' LIMIT 0,1")->row_array(); 

			if($dadosToken != null) {
				$dadosAlteracao 			= array(); 
				$dadosAlteracao["proSenha"]  	= $CI->encryption->encrypt($novaSenha);

					$condicao 					= array('proCodigo' => $dadosToken["id"]);
					if($CI->padrao_model->alterar("pro_profissional", $dadosAlteracao, $condicao)) {
						retornarJson(null, array('resposta' => true));
					} 
			}
			else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
		}
		else
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Token inválido ou expirado.'));
	}

