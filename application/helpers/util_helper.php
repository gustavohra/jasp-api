<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Realiza o decode de URL
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('utf8_urldecode'))
{
	function utf8_urldecode($str)
	{
		$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
    	return html_entity_decode($str,null,'UTF-8');;
	}
}

/**
 * Verificar Permissões do Login
 *
 * Verifica se um determinado usuário poderá executar uma ação de acordo com seu nível de acesso
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('verificarPermissoesLogin'))
{
	function verificarPermissoesLogin()
	{
		$CI =& get_instance();
		$CI->load->model("cub3_menu_model");
		$menus = $CI->session->userdata("s_menu");

					if($menus == "null" || $menus == ""):
						echo "<small style='text-align:center; padding-top: 30px;font-size:11px'>Você não tem permissões de acesso aos menus do sistema.</small>";
					else:
						$menus = json_decode($menus);
						$itemArvore = 0;
						foreach ($menus as $menu) { 
							$menuItens = $CI->cub3_menu_model->menuSubmenuListarTodos($menu->menCodigo);
							$qtdItens = $menuItens->num_rows();
							$itemArvore  = $qtdItens > 0 ? 1 : 0; 
							echo '<li ng-click="ativarMenu(\''. $menu->menCodigo.'\')" ng-class="{ativo: menuAtivo == \''.$menu->menCodigo.'\', arvore: '.$itemArvore.'}">';
							echo '	<h3><i class="fa '.$menu->menIcone.' icone"></i><span>'.$menu->menDescricao;

							if($qtdItens > 0):
								echo '</span></h3>		<ul>';
								foreach ($menuItens->result() as $menuItem) {
									echo '			<li tooltips title="'.$menu->menDescricao.'" tooltip-side="right" tooltip-size="small"><a href="'.$menuItem->mitLink.'">'.$menuItem->mitDescricao.'</a></li>';
								}
								echo '		</ul>';
							else:
								echo '</h3>';
							endif;
							echo '</li>'; 
						}
					endif;
	}
}


/**
 * Verificar se o usuário é administrador
 *
 * Verifica o status de uma sessão no sistema
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('verificarAdministrador'))
{
	function verificarAdministrador($flag=null)
	{
		$CI =& get_instance();
		if(urldecode($CI->input->get("authKey")) == '76e91230b0b1b15c862baba88552181abf618a6a91431284ddd81a3d4b6e931e995067ed3e4197049f6e927f0ff9f2b215e92feea2ae56fd9c8f202cda3f568dpLhLOEXFQTEJHvyy2w/A9pUdK8qUHQ9bYEiooQWy3Cqlzgdz6TWk'):
			return true;
		else:
			$sessao = $CI->session->userdata("s_usuCodigo");

			//Caso o usuário esteja logado e não esteja ná página de login
			if(($sessao != "") && empty($flag)):
				$CI->load->model("cub3_permissoes_model");
				$CI->load->model("cub3_grupo_model");

				$grupoUsuario		= $CI->cub3_grupo_model->grupoPorUsuario($sessao);

				// Caso o usuário seja um usuário CUB3, todos os acessos serão permitidos
				if($grupoUsuario->row()->gruCodigo == 1):
					return true;
				else:
					return false;
				endif;
			else:
				return false;
			endif;
		endif;
	}
}

/**
 * Verificar Status da Sessão
 *
 * Verifica o status de uma sessão no sistema
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('verificarStatusSessao'))
{
	function verificarStatusSessao($flag=null)
	{
		$CI =& get_instance();
		if($CI->session->userdata("s_usuCodigo") != null):
			$sessao = $CI->session->userdata("s_usuCodigo"); 
		else:
			redirect(base_url()."/login");
		endif;
	}
}
if ( ! function_exists('verificarStatusSessaoProfissional'))
{
	function verificarStatusSessaoProfissional($flag=null)
	{
		$CI =& get_instance();
		if($CI->session->userdata("s_proCodigo") != null):
			$sessao = $CI->session->userdata("s_proCodigo"); 
		else:
			redirect(base_url()."/ProfissionalLogin");
		endif;
	}
}
 
/**
 * Verificar Status da Sessão
 *
 * Verifica o status de uma sessão no sistema
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('verificarSessao'))
{
	function verificarSessao($flag=null)
	{
		$CI =& get_instance();
		if($CI->session->userdata("s_usuCodigo") != null):
			return true;
		else:
			return false;
		endif;
	}
}
 
 

if (!function_exists('cifracub3'))
{
	function cifracub3($string){
		return base64_encode($string);
	}
}
if (!function_exists('decifracub3'))
{
	function decifracub3($string){
		return base64_decode($string);
	}
}

if (!function_exists('cifracub3_256'))
{
	function cifracub3_256($string){
		$CI =& get_instance();
		$CI->load->library("Encryption");

		$CI->encryption->initialize(array(
    		'driver' 	=> 'mcrypt',
	        'cipher' 	=> 'rijndael-256',
	        'mode' 		=> 'ctr',
	        'key' 		=> $CI->config->item("encryption_key"))
		);

		return $CI->encryption->encrypt($string);
	}
}
if (!function_exists('decifracub3_256'))
{
	function decifracub3_256($string){
		$CI =& get_instance();
		$CI->load->library("Encryption");

		$CI->encryption->initialize(array(
    		'driver'	=> 'mcrypt',
	        'cipher' 	=> 'rijndael-256',
	        'mode' 		=> 'ctr',
	        'key' 		=> $CI->config->item("encryption_key"))
		);

		return $CI->encryption->decrypt($string);
	}
}

if (!function_exists('carregarCss'))
{
	function carregarCss($array){
		if(!empty($array)):
			foreach ($array as $item) {
				echo "<link href=\"".$item."\" rel=\"stylesheet\" type=\"text/css\" />\n";
			}
		endif;
	}
}
if (!function_exists('carregarJavascript'))
{
	function carregarJavascript($array){
		if(!empty($array)):
			foreach ($array as $item) {
				echo "<script src=\"".$item."\" type=\"text/javascript\"></script>\n";
			}
		endif;
	}
}


if (!function_exists('corpoEmail'))
{
	function corpoEmail($conteudo){
		$CI =& get_instance();
		$header = file_get_contents("cub3/mail/templateHeader.html");
		$footer = file_get_contents("cub3/mail/templateFooter.html");
		
		$corpo = $header . $conteudo["mensagem"] . $footer;

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'ti@cub3.com.br',
		    'smtp_pass' => 'cubdev18',
		    'mailtype'  => 'html',  
		    'charset'   => 'utf-8'
		);
		//$CI->upload->do_upload($field_name);
		$CI->load->library('email', $config);
		$CI->email->set_newline("\r\n");
		$CI->email->from('ti@cub3.com.br', 'intraCUB3');
		$CI->email->to($conteudo['destinatario']);
		if(isset($conteudo['bcc'])) {
			$CI->email->bcc($conteudo['bcc']);
		}
		$CI->email->subject($conteudo['assunto']);
		$CI->email->message($corpo);	
		return $CI->email->send();


	}
}
	/**
 * Retorna mensagens de status
 *
 * Verifica o status de uma sessão no sistema
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('mensagemStatus'))
{
	function mensagemStatus($tipo,$texto=null)
	{
		switch ($tipo) {
			case 'sucesso':
				$texto = (empty($texto))?"Os dados foram inseridos.":$texto;
				$mensagem = '<div class="alert alert-success" role="alert">
							  <i class="fa fa-thumbs-up fa-2x"></i>  <strong>Sucesso!</strong> '.$texto.'
							</div>';
				return $mensagem;
				break;
			case 'erro':
				$texto = (empty($texto))?"Os dados não foram inseridos.":$texto;
				$mensagem = '<div class="alert alert-danger" role="alert">
							  <i class="fa fa-thumbs-down fa-2x"></i>  <strong>Erro!</strong> '.$texto.'
							</div>';
				return $mensagem;
				break;
			case 'informacao':
				$texto = (empty($texto))?"Informações foram armazenadas no histórico de açnoes.":$texto;
				$mensagem = '<div class="alert alert-info" role="alert">
							  <i class="fa fa-info fa-2x"></i>  <strong>Alerta!</strong> '.$texto.'
							</div>';
				return $mensagem;
				break;
			case 'alerta':
				$texto = (empty($texto))?"Atenção. Algo de anormal foi detectado":$texto;
				$mensagem = '<div class="alert alert-warning" role="alert">
							  <i class="fa fa-warning fa-2x"></i>  <strong>Cuidado!</strong> '.$texto.'
							</div>';
				return $mensagem;
				break;
			
		}
	}
}

if (!function_exists('realparamysql'))
{
	function realparamysql($string){
		return str_replace(',','.',str_replace('.','',$string));
	}
}
if (!function_exists('mysqlparareal'))
{
	function mysqlparareal($string){
		return number_format($string,2,",",".");
	}
}

if (!function_exists('unserialize'))
{
	function unserialize($str){
		$returndata = array();
	    $strArray = explode("&", $str);
	    $i = 0;
	    foreach ($strArray as $item) {
	        $array = explode("=", $item);
	        $returndata[$array[0]] = $array[1];
	    }

	    return $returndata;
	}
}

if (!function_exists('verificarArrayConteudo'))
{
	function verificarArrayConteudo($array)
	{
		$conteudo = array();

		if(is_null($array))
		{
			$conteudo["conTitulo"]		= "Página não encontrada!";
			$conteudo["conConteudo"]	= "Por favor, verifique o endereço digitado e tente novamente.";
			return $conteudo;
		}
		else
			return $array;
	}
}

if (!function_exists('strlen_compare'))
{
	function strlen_compare($a,$b){
	    if(function_exists('mb_strlen')){
	         return mb_strlen($b) - mb_strlen($a);
	    }
	    else{
	         return strlen($b) - strlen($a);
	    }
	}
}

if (!function_exists('strlen_array_sort'))
{
	function strlen_array_sort($array,$order='dsc'){
	    usort($array,'strlen_compare');
	    if($order=='asc'){
	        $array=array_reverse($array);
	    }
	    return $array;
	}
}
if (!function_exists('pegarIp'))
{
	function pegarIp(){
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
	}
}

if ( ! function_exists('now'))
{
    /**
     * Get "now" time
     *
     * Returns time() based on the timezone parameter or on the
     * "time_reference" setting
     *
     * @param   string
     * @return  int
     */
    function now($timezone = NULL)
    {
        if (empty($timezone))
        {
            $timezone = config_item('time_reference');
        }
        if ($timezone === 'local' OR $timezone === date_default_timezone_get())
        {
            return time();
        }
        $datetime = new DateTime('now', new DateTimeZone($timezone));
        sscanf($datetime->format('j-n-Y G:i:s'), '%d-%d-%d %d:%d:%d', $day, $month, $year, $hour, $minute, $second);
        return mktime($hour, $minute, $second, $month, $day, $year);
    }
}

	/*
	* [Método]: carregarTema
	* [Descrição]: Carrega um tema de acordo com as configurações do sistema ou usuário
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/util_helper.php
	* @param bool flag, array() dadosArray
	* @return 
	*/

if (!function_exists('carregarTema'))
{
	function carregarTema($data = null){
		$CI =& get_instance();
		if($data != null){
			if(!isset($data["exibirSidebar"]))
				$data["exibirSidebar"] = true;

			if(!isset($data["exibirHeader"]))
				$data["exibirHeader"] = true;

			if(is_null($CI->session->userdata('s_usuTema'))) {
				if(!isset($data["tema"]))
					$CI->load->view(($CI->config->item("tema_principal") !== null ? $CI->config->item("tema_principal") : 'template/sistema/principal'),$data);
				else
					$CI->load->view($data["tema"], $data);
			}
			else{
				if(!isset($data["tema"]))
					$CI->load->view($CI->session->userdata('s_usuTema'),$data);
				else
					$CI->load->view($data["tema"], $data);
			}
		}
		else{
			if(is_null($CI->session->userdata('s_usuTema'))) 
				$CI->load->view(($CI->config->item("tema_principal") !== null ? $CI->config->item("tema_principal") : 'template/sistema/principal'));
			else
				$CI->load->view($CI->session->userdata('s_usuTema'));	
			}
	}
}

	/*
	* [Método]: getIp
	* [Descrição]: Retorna o IP do usuário que realiza a requisição
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package 
	* @param bool flag, array() dadosArray
	* @return 
	*/

if (!function_exists('getIp'))
{
	function getIp(){

		$ip = "";
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
			    $ip = $_SERVER['REMOTE_ADDR'];
			}	
			return $ip;	
		}
}

	/*
	* [Método]: retornarJson
	* [Descrição]: Permite o retorno no formato JSON
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/util_helper.php
	* @param bool flag, array() dadosArray
	* @return 
	*/

if (!function_exists('retornarJson'))
{
	function retornarJson($flag = null, $dadosArray = null, $usarHeader = true){ 
		if($usarHeader)
			header('Content-type: application/json');

		if($flag != null){
			if(is_null($dadosArray)){
				$resposta = array('resposta' => $flag);
				return die(json_encode($resposta));
			}
			else{
				$dadosArray["resposta"] = $flag;
				return die(json_encode($dadosArray));
			}
		}
		else
		{
				return die(json_encode($dadosArray));	
		}
	}
}

	/*
	* [Método]: limparSessao
	* [Descrição]: Função temporária para a limpeza da sessão
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package 
	* @param bool flag, array() dadosArray
	* @return 
	*/

if (!function_exists('limparSessao'))
{
	function limparSessao(){
		$CI =& get_instance();
	   $sessao = $CI->session->all_userdata();

	    foreach ($sessao as $key => $value) {
	        if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
	            $CI->session->unset_userdata($key);
	        }
	    }
	}
}

	/*
	* [Método]: exibirView
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/helpers/util_helper.php
	* @param 
	* @return 
	*/
if (!function_exists('exibirView'))
{
	 function exibirView($valor, $caminho)
	{
		$CI =& get_instance();
		if($valor !== null && $caminho !== null)
			$CI->load->view($caminho);
		else
			$CI->load->view($caminho);
	}

} 

	/*
	* [Método]: iniciarAngular
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/cub3ci/application/helpers/util_helper.php
	* @param 
	* @return 
	*/

if (!function_exists('carregarDependenciasAngular'))
{	
	function carregarDependenciasAngular($origem = null)
	{
		switch ($origem) {
			case 'portal':
				$depOrigem = 'dep_angular_portal';
				break;
			default:
				$depOrigem = 'dep_angular';
				break;
		}
		// Recebe o arquivo de configurações do projeto
		$configuracoes 		= json_decode(file_get_contents("./projeto.json"), true);
		$retorno 			= "'";
		$retorno 			.= implode("','", $configuracoes[$depOrigem]);
		$retorno 			.= "'";
		return $retorno; 
	}
}


	/*
	* [Método]: valorPorExtenso
	* [Descrição]: Permite o retorno de um valor por extenso
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/bellolar/application/helpers/util_helper.php
	* @param bool flag, array() dadosArray
	* @return 
	*/
	function removerFormatacaoNumero( $strNumero )
	    {
	 
	        $strNumero = trim( str_replace( "R$", null, $strNumero ) );
	 
	        $vetVirgula = explode( ",", $strNumero );
	        if ( count( $vetVirgula ) == 1 )
	        {
	            $acentos = array(".");
	            $resultado = str_replace( $acentos, "", $strNumero );
	            return $resultado;
	        }
	        else if ( count( $vetVirgula ) != 2 )
	        {
	            return $strNumero;
	        }
	 
	        $strNumero = $vetVirgula[0];
	        $strDecimal = mb_substr( $vetVirgula[1], 0, 2 );
	 
	        $acentos = array(".");
	        $resultado = str_replace( $acentos, "", $strNumero );
	        $resultado = $resultado . "." . $strDecimal;
	 
	        return $resultado;
	 
	    }

if (!function_exists('valorPorExtenso'))
{
	
function valorPorExtenso( $valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false )
    {
        $valor = removerFormatacaoNumero( $valor );
 
        $singular = null;
        $plural = null;
 
        if ( $bolExibirMoeda )
        {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        }
        else
        {
            $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("", "", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        }
 
        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
 
 
        if ( $bolPalavraFeminina )
        {
        
            if ($valor == 1) 
            {
                $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            }
            else 
            {
                $u = array("", "um", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            }
            
            
            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas","quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
            
            
        }
 
 
        $z = 0;
 
        $valor = number_format( $valor, 2, ".", "." );
        $inteiro = explode( ".", $valor );
 
        for ( $i = 0; $i < count( $inteiro ); $i++ ) 
        {
            for ( $ii = mb_strlen( $inteiro[$i] ); $ii < 3; $ii++ ) 
            {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }
 
        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count( $inteiro ) - ($inteiro[count( $inteiro ) - 1] > 0 ? 1 : 2);
        for ( $i = 0; $i < count( $inteiro ); $i++ )
        {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
 
            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count( $inteiro ) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ( $valor == "000")
                $z++;
            elseif ( $z > 0 )
                $z--;
                
            if ( ($t == 1) && ($z > 0) && ($inteiro[0] > 0) )
                $r .= ( ($z > 1) ? " de " : "") . $plural[$t];
                
            if ( $r )
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }
 
        $rt = mb_substr( $rt, 1 );
 
        return($rt ? trim( $rt ) : "zero");
	}
}


if (!function_exists('mascara'))
{
	function mascara($val, $mask)
	{
		 $maskared = '';
		 $k = 0;
		 for($i = 0; $i<=strlen($mask)-1; $i++)
		 {
		 if($mask[$i] == '#')
		 {
		 if(isset($val[$k]))
		 $maskared .= $val[$k++];
		 }
		 else
		 {
		 if(isset($mask[$i]))
		 $maskared .= $mask[$i];
		 }
		 }
		 return $maskared;
	}
} 

if(!function_exists('uploadImagem64')) {
	function uploadImagem64($arquivo, $diretorio) { 
		if($arquivo != null && $diretorio != null):
			$upload_path      = str_replace( '/', DIRECTORY_SEPARATOR, $diretorio ) . DIRECTORY_SEPARATOR;

			list($type, $arquivo) = explode(';', $arquivo);
			list(, $arquivo)      = explode(',', $arquivo);

			$decoded          = base64_decode($arquivo) ; 

			$hashed_filename  = md5( microtime() ) . '_.png';
			if (!file_exists($upload_path)) {
				mkdir($upload_path, 0755, true);
			}
			$image_upload     = file_put_contents( $upload_path . $hashed_filename, $decoded );

			return base_url() . $diretorio . '/' . $hashed_filename;
		else:
			return null;
		endif;
	}
}

function montarCamposPost($colunas) {
		$CI =& get_instance();
	$retorno = array();
	for ($i=0; $i < Count($colunas); $i++) { 
		$aux = $colunas[$i];

		if(isset($aux) && !is_null($aux) && ($CI->input->post($aux) != null))
			$retorno[$aux] = $CI->input->post($aux);
	}
	return $retorno;
}
function random_string($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


function getDataURI($imagePath) {
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if(file_exists($imagePath)){
	    $type = $finfo->file($imagePath);
	    return 'data:'.$type.';base64,'.base64_encode(file_get_contents($imagePath));
	}
	else
		return '';
}
