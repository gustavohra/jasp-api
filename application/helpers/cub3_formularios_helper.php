<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 30/04/2016 às 21:55:06
	*
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/cub3_formularios_helper.php
	*/ 

	/*
	* [Método]: gerarFormulario
	* [Descrição]: Gera um formulário simples a partir do id ou slug recebido
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/cub3_formularios_helper.php
	* @param 
	* @return 
	*/
	function gerarFormulario($id, $exibirBotao = true, $model = "formulario.model", $modelDados = null)
	{ 
		$CI =& get_instance();
		$CI->load->model("cub3_formularios_model");

		// Verifica se existe um código ou slug passado por parâmetro
		$verificarId = $CI->cub3_formularios_model->formularioVerificar($id); 
		$formulario  = $verificarId->row();
		$forCodigo 	 = $formulario->forCodigo;

		if($formulario->forAutomatico)
			$urlPost = "cub3_formularios/inserirDadosEmFormulario?forCodigo=".urlencode(cifracub3($forCodigo));
		else
			$urlPost = $formulario->forUrlPost;

		if(!is_null($forCodigo)):
			echo '<div class="formularioContainer"> ' .
					'<div class"row" ng-init="form.forCodigo = '.$forCodigo.'; dadosFormulario = \''.$modelDados.'\'"> ' .
					    '<ng-form ng-submit="enviar()" validate name="formulario.form" ng-init="getCampos(form.forCodigo);urlPost = \''.$urlPost.'\'" ng-controller="FormularioSimplesCtrl">	 ' .		
							'<cub3-carregar-spinner ng-show="!formularioCarregado"></cub3-carregar-spinner>'.
							'<formly-form model="'.$model.'" fields="formulario.campos"> ' .
							'</formly-form>  ' .
						  ($exibirBotao == true ? '<button type="button" ng-click="enviar(formulario.form)" ng-cloak class="btn btn-success direita btn-avancar" ng-show="formularioCarregado"><i class="fa fa-check"></i> Confirmar</button><br clear="all" /> ' : '') .
				 		'</ng-form> ' .
				'</div> ' .
				'</div>';
		else:
			echo '<blockquote>' .
				  '<small>Formulário inválido.</small>' .
				'</blockquote>';
		endif;

	}
		/*
	* [Método]: tratarAnexo
	* [Descrição]: Recebe um array e o nome do campo para ser modificado
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/ouvidoriapsdbh55/application/helpers/cub3_formularios_helper.php
	* @param 
	* @return 
	*/

	function tratarAnexo($dados, $nomeCampo = null)
	{
		if($dados != null) {
			if(isset($dados["anexo"])){
				if($nomeCampo != null){
					$dados[$nomeCampo] = $dados["anexo"];
					unset($dados["anexo"]);
				} 
					unset($dados["tipoAnexo"]);
					unset($dados["nomeAnexo"]);
			}
		}

		return $dados;
	}




if ( ! function_exists('salvarCache'))
{
	function salvarCache($dados, $codigo)
	{ 
		if($dados != null){
			$dados 			= json_encode($dados, JSON_PRETTY_PRINT);
			$arquivo 		= fopen("./application/cache/formularios/formulario-".$codigo.".json", "w");
			
			if($dados != false){
				try {
					fwrite($arquivo, $dados);
					fclose($arquivo);
				} catch (Exception $e) {
					return false;
				}	finally {
				   return true;
				}
			}
			else
				return false;
		}
		else
			return false;
	}
}

if ( ! function_exists('criarCache'))
{
	function criarCache($forCodigo, $json = true)
	{ 
		$CI =& get_instance();
		$dados			= getFormularioCamposJson($forCodigo, false);

		if(salvarCache($dados, $forCodigo)){
			if($json)
				retornarJson(null, array('resposta' => true, 'mensagem' => 'Cache criado com sucesso!' ));
			else
				return true;
		}
		else{
			if($json)
				retornarJson(null, array('resposta' => false, 'mensagem' => 'Não foi possível criar o cache.' ));
			else
				return false;
		}
	}
}


if ( ! function_exists('criarCacheGeral'))
{
	function criarCacheGeral()
	{ 
		$CI =& get_instance();
		$formularios 	= $CI->cub3_formularios_model->formulariosListar()->result_array();
		$retorno 		= 0;
		for ($i=0; $i < Count($formularios); $i++) { 
			salvarCache(getFormularioCamposJson($formularios[$i]["forCodigo"], false), $formularios[$i]["forCodigo"]);
			$retorno++;
		}

		if($retorno == Count($formularios))
			retornarJson(null, array('resposta' => true, 'mensagem' => 'Cache criado com sucesso!' ));
		else
			retornarJson(null, array('resposta' => false, 'mensagem' => 'Não foi possível criar o cache.' ));
	}
}


if ( ! function_exists('exportarFormulario'))
{
	function exportarFormulario($dados)
	{  
		$CI =& get_instance();
		if ($dados != null) { 
		    header('Content-Disposition: attachment; filename="'.$CI->config->item('portal_nome').'.formulario.json"'); 
			header('Content-type: application/json');
		    print_r($dados);
	    exit;
		}		
	}
}
if ( ! function_exists('getTabelaCampos'))
{
	function getTabelaCampos($nomeTabela, $campos = null)
	{  
		$CI 	=& get_instance();
		if($campos == null)
			$campos = $CI->db->field_data($nomeTabela);
		
		$formCampos = array();
		$aux 	= 0;
		foreach ($campos as $campo) { 
				$formCampos[$aux]["key"] 						= $campo->name;
				$formCampos[$aux]["templateOptions"] 			= json_encode(array("label" => "Campo da coluna " . $campo->name));
				$formCampos[$aux]["fctOrdem"] 					= $aux;
				$formCampos[$aux]["defaultValue"] 				= $campo->default; 
			switch ($campo->type) {
				case 'varchar':
			 			$formCampos[$aux]["type"] = "input";
						$formCampos[$aux]["className"] = "col-md-12";
					break;
				case 'text':
						$formCampos[$aux]["type"] = "textarea";
						$formCampos[$aux]["className"] = "col-md-12";
					break;
				case 'int':
						$formCampos[$aux]["type"] = "input";
						$formCampos[$aux]["className"] = "col-md-12";
					break;
				default: 
						$formCampos[$aux]["type"] = "input";
						$formCampos[$aux]["className"] = "col-md-12";
					break;
			}
 
			$aux++;
		}
		return $formCampos;
	}
}
if ( ! function_exists('getTabelasNomes'))
{
	function getTabelasNomes()
	{  
		$CI 			=& get_instance();
		$tabelas 		= $CI->db->list_tables(); 
		retornarJson(null, $tabelas);
		
	}
}
if ( ! function_exists('getFormularioCamposJson'))
{
	function getFormularioCamposJson($forCodigo, $retornoJson = true)
	{  
		if($forCodigo == '')
			retornarJson(false);
		else {
			$CI 		=& get_instance();
			$campos  	= $CI->cub3_formularios_model->formularioListarCampos($forCodigo)->result_array();
			$arrayPai 	= array();
			$retorno 	= array();

			if($campos != null){
			for ($i=0; $i < count($campos); $i++) { 
				$aux 		= $campos[$i];

				switch ($aux["fctAgrupamento"]) {
					case 1:
							// Monta o array pai que irá agrupar os campos
							$arrayPai["className"] 				= $aux["className"];
							$arrayPai["fieldGroup"]				= array();
							break;
					case 0:
								$auxArray 					= formularioMontarCampos($aux, null, $forCodigo);

								if(!isset($arrayPai["className"])){
									$arrayPai["className"]			= "row";
									$arrayPai["fieldGroup"]			= array();
								} 

								array_push($arrayPai["fieldGroup"], $auxArray);
						break;
					default:
						
						break;
				}
			}

				array_push($retorno, $arrayPai);
			}
			else{
				$retorno = null;
			}
			if($retornoJson)
				retornarJson(null,$retorno);
			else
				return $retorno;
		}
	}
}
if ( ! function_exists('formularioMontarCampos'))
{
	function formularioMontarCampos($array, $campos = null, $forCodigo = null)
	{  
			if($campos == null){
				// Atributos do Formly
				$campos = array('type', 'template', 'templateUrl', 'key', 'defaultValue', 'hide', 'hideExpression', 'model', 'expressionProperties', 'className', 'id', 'name', 'extras', 'data', 'templateOptions', 'templateManipulator', 'wrapper', 'ngModelElAttrs', 'ngModelAttrs', 'controller', 'link', 'watcher');
			}
			$retorno 	= array(); 
			for ($j=0; $j < Count($campos); $j++) {  
				$auxCampo  	= $campos[$j];

				if(isset($array[$auxCampo]) && $array[$auxCampo] != null){
					switch ($auxCampo) { 
						case 'templateOptions': 
								$retorno[$auxCampo] = array();
								$retorno[$auxCampo] = json_decode(stripslashes($array[$auxCampo]), true); 
								
								if($forCodigo != null){
									$retorno[$auxCampo]["forCodigo"] = $forCodigo;
									$retorno[$auxCampo]["fctCodigo"] =  $array["fctCodigo"];
								}
							break;
						case 'expressionProperties': 
								$retorno[$auxCampo] = array();
								$retorno[$auxCampo] = json_decode(stripslashes($array[$auxCampo]), true);  
							break;
						case 'data': 
								$retorno[$auxCampo] = array();
								if($retorno["type"] == "tinymce") {
									$retorno[$auxCampo]["tinymceOption"] = array(); 
									$retorno[$auxCampo]["tinymceOption"] = json_decode(stripslashes($array[$auxCampo]));


								} else {
									$retorno[$auxCampo] = json_decode($array[$auxCampo]);
								}
							break; 
						case 'controller': 
								$retorno[$auxCampo] = array();
								$retorno[$auxCampo] = stripslashes($array[$auxCampo]);
							break; 
						default:
								$retorno[$auxCampo] = $array[$auxCampo];
							break;
					}
				}
			}

			return $retorno;
	}
} 


if ( ! function_exists('inserirDadosEmFormulario'))
{
	function inserirDadosEmFormulario($dados, $forCodigo)
	{  
		$CI 		=& get_instance();
 		$CI->load->model('padrao_model');
 		$CI->load->model('cub3_formularios_model');

 		$formulario 		=	$CI->cub3_formularios_model->formularioListarPorCodigo($forCodigo)->row_array();
 		
 		if($dados != null){
	 		if($formulario != null){
	 			if(isset($formulario["forBancoDeDados"]) && isset($formulario["forBancoDeDadosKey"])){
			 		$campoIdentificador = $formulario["forBancoDeDadosKey"];
			 		$tabela 			= $formulario["forBancoDeDados"];
					$urlRetorno 		= $formulario["forUrlRedirect"];
					$mensagemRetorno 	= $formulario["forMensagemRetorno"];

					if(!isset($dados[$campoIdentificador])){ 
						$operacao 		= $CI->padrao_model->inserir($tabela, $dados); 
						if($operacao >= 0)
							retornarJson(true, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => ($mensagemRetorno == null ? 'Formulário enviado com sucesso!' : $mensagemRetorno) ));
						else
							retornarJson(false);	
					}	
					else {		
						$mensagemRetorno 		= 'Informações alteradas com sucesso!';
						$condicao 				= array($campoIdentificador => $dados[$campoIdentificador]);
						if($CI->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
							retornarJson(null, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => $mensagemRetorno)); 
						else
							retornarJson(false);
					}
	 			}	 
		 		else
					retornarJson(null, array('resposta' => false, "mensagem" => "Não foi possível enviar o formulário. Por favor, entre em contato com o suporte técnico. Erro: F01")); 	
	 		}
	 		else
				retornarJson(null, array('resposta' => false, "mensagem" => "Não foi possível enviar o formulário. Por favor, entre em contato com o suporte técnico. Erro: F02")); 	
 		}		
 		else
				retornarJson(null, array('resposta' => false, "mensagem" => "Por favor, verifique o preenchimento dos campos.")); 

	}
} 

if ( ! function_exists('validarSlug'))
{
	function validarSlug($slug)
	{  
		$CI 		=& get_instance();
 		$CI->load->model('cub3_formularios_model');
 		$formularios 		=	$CI->cub3_formularios_model->formularioListarPorSlug($slug);

 		if($formularios->num_rows() != null && $formularios->num_rows() > 0)
 			return $slug . ($formularios->num_rows()+1);
 		else
 			return $slug;
	}
}