<?php

	/*
	* [Método]: getConfig
	* [Descrição]: Realiza a busca dos dados no banco de dados e retorna um array em JSON 
	*/
	  function getConfig()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 		= json_decode($CI->padrao_model->buscar("SELECT * FROM jasp_config WHERE tipo = 'financeiro'")->row_array()['valores'], true);

		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}

	  function getTransacoes()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$dados 		=  $CI->padrao_model->buscar("SELECT TRA.*, PAC.nome FROM log_transacao TRA, cliente PAC WHERE PAC.id = TRA.id")->result_array();

		if(!is_null($dados))
			retornarJson(null, $dados);
		else
			retornarJson(false);
	}

	/*
	* [Método]: setConfig 
	* [Descrição]: Insere ou edita um valor no banco de dados. Observar a instanciação dos campos $campoIdentificador e $tabela
	* [Comentários]: Caso o código identificador seja passado, ele realiza a alteração e caso contrário, a inserção.
	*/
 function setConfig ()
	{
		$CI =& get_instance();
		$CI->load->model('padrao_model');
		$json 		= json_encode(montarCamposPost(['token', 'chave', 'comissaoProfissional', 'retencao']));	
		$dados 		= array('valores' => $json, 'tipo' => 'financeiro' );

		$urlRetorno =   null;
		$campoIdentificador = 'tipo';
		$tabela = 'jasp_config';

		if(!isset($dados[$campoIdentificador])){
			$mensagemRetorno 		= 'Linha inserida com sucesso!';
			if($CI->padrao_model->inserir($tabela, $dados) > 0)
				retornarJson(null, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => $mensagemRetorno));
			else
				retornarJson(false);	
		}	
		else {		
			$mensagemRetorno 		= 'Linha alterada com sucesso!';
			$condicao 				= array($campoIdentificador => $dados[$campoIdentificador]);
			if($CI->padrao_model->alterar($tabela, $dados, $condicao) > 0) 
				retornarJson(null, array('resposta' => true, 'url' => $urlRetorno, 'mensagem' => $mensagemRetorno)); 
			else
				retornarJson(false);
		}	
	}
