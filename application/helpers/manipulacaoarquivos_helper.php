<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  - moveUploadParaPasta - Permite mover um arquivo temporário para uma pasta
 *
 * @access	public
 * @param	mixed
 * @param	mixed
 */
if (!function_exists('moveUploadParaPasta'))
{
	function moveUploadParaPasta($arquivo,$diretorio)
	{
		
		$nomeArquivo 	= criarNomeArquivo($arquivo['name']);
		$diretorio		.= "/".$nomeArquivo;

		if(move_uploaded_file($arquivo['tmp_name'], $diretorio)){
			return $diretorio;
		}

	}
}
/**
 *  - extensaoViaNomeDoArquivo - Permite capturar a extensao de um arquivo
 *
 * @access	public
 * @param	mixed
 */
if (!function_exists('extensaoViaNomeDoArquivo'))
{
	function extensaoViaNomeDoArquivo($arquivo){
		$extensao = end(explode(".",$arquivo));
		return ".".strtolower($extensao);
	}
}
/**
 *  - criarNomeArquivo - Cria o nome de um arquivo baseado em sua extensao original
 *
 * @access	public
 * @param	mixed
 */
if (!function_exists('criarNomeArquivo'))
{
	function criarNomeArquivo($nomeOriginal){
		$CI =& get_instance();
    	$CI->load->helper("string");
    	$nomeArquivo 		= random_string('unique', 16);
		$extensaoArquivo 	= extensaoViaNomeDoArquivo($nomeOriginal);

		return $nomeArquivo.$extensaoArquivo;
	}
}
/**
 *  - removerPastasArquivos - Remove uma pasta e seus arquivos internos
 *
 * @access	public
 * @param	mixed
 */
if (!function_exists('removerPastasArquivos'))
{
	function removerPastasArquivos($caminho){
		if ($dd = opendir($caminho)) {
	        while (false !== ($Arq = readdir($dd))) {
	            if($Arq != "." && $Arq != ".."){
	                $Path = "$caminho/$Arq";
	                if(is_dir($Path)){
	                    removerPastasArquivos($Path);
	                }elseif(is_file($Path)){
	                    unlink($Path);
	                }
	            }
	        }
	        closedir($dd);
	    }
	    rmdir($caminho);
	}
}
/**
 *  - percorreArquivos - Em uma pasta, percorre por todos arquivos
 *  dos arquivos.
 *
 * @access	public
 * @param	mixed
 * @return	array
 */
if (!function_exists('percorreArquivos'))
{
	function percorreArquivos($caminho){
		$dados = array();
		$itens = scandir($caminho);
		if ($itens !== false) { 
		    foreach ($itens as $item) { 
		        if(is_file($caminho."/".$item)){
		        	array_push($dados,$caminho."/".$item);
		        }
		    }
		}
		return $dados;
	}
}
/**
 *  - percorrerDiretorios - Em uma pasta, percorre por todos os seus diretórios e retorna um array com os endereços
 *  dos arquivos.
 *
 * @access	public
 * @param	mixed
 * @return	array
 */
// if (!function_exists('percorrerDiretorios'))
// {
// 	function percorrerDiretorios($caminho){
// 		$itens = scandir($caminho);
// 		if ($itens !== false) { 
// 		    foreach ($itens as $item) { 
// 		        if(is_dir($item){
// 		        	// bla bla bla...
// 		        }
// 		    }
// 		}
// 		die();
// 		return $dados;
// 	}
// }