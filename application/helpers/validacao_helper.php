<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Valida se string é um CPF válido
if (!function_exists('isCpf'))
{
	function isCpf($cpf)
	{
		$cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
		if ( strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'):
			return FALSE;
		else:
			for($t=9;$t<11;$t++):
				for ($d=0,$c=0;$c<$t;$c++):
					$d += $cpf{$c}*(($t+1)-$c);
				endfor;
				$d = ((10*$d)%11)%10;
				if ($cpf{$c} != $d):
					return FALSE;
				endif;
			endfor;
			return TRUE;
		endif;
	}
}
//Valida se string é um CNPJ válido
if (!function_exists('isCnpj'))
{
	function isCnpj($cnpj)
	{
		$cnpj = str_pad(@ereg_replace(‘[^0-9]’, ”, $cnpj), 14, ‘0’, STR_PAD_LEFT);
		if (strlen($cnpj) != 14):
			return false;
		else:
			for ($t=12;$t<14;$t++):
				for ($d=0,$p=$t–7,$c=0;$c<$t;$c++):
					$d += $cnpj{$c} * $p;
					$p = ($p < 3) ? 9 : –$p;
				endfor;
				$d = ((10*$d)%11)%10;
				if ($cnpj{$c} != $d):
					return false;
				endif;
			endfor;
			return($cnpj);
		endif;
	}
}
//Valida se string é um CEP válido
if (!function_exists('isCep'))
{
	function isCep($string)
	{
		return (count($string) != 8)?false:true;
	}
}
//Valida se string é um IP válido
if (!function_exists('isIp'))
{
	function isIp($string)
	{
		return (!filter_var($string, FILTER_VALIDATE_IP) === false)?true:false;
	}
}

//Valida se string é um E-mail válido
if (!function_exists('isEmail'))
{
	function isEmail($string)
	{
		return (!filter_var($string, FILTER_VALIDATE_EMAIL) === false)?true:false;
	}
}

//Valida se string é uma URL válida
if (!function_exists('isUrl'))
{
	function isUrl($string)
	{
		return (!filter_var($string, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED) === false)?true:false;
	}
}

//Valida se string é uma INT válida
if (!function_exists('isInt'))
{
	function isInt($string)
	{
		return (!filter_var($string, FILTER_VALIDATE_INT, FILTER_FLAG_ALLOW_OCTAL) === false)?true:false;
	}
}

//Valida se string é um boolean válido
if (!function_exists('isBoolean'))
{
	function isBoolean($string)
	{
		return (!filter_var($string, FILTER_VALIDATE_BOOLEAN, FILTER_FLAG_ALLOW_OCTAL) === false)?true:false;
	}
}

?>