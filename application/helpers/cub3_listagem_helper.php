<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 30/04/2016 às 21:55:06
	*
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/cub3_formularios_helper.php
	*/ 

	/*
	* [Método]: gerarListagem
	* [Descrição]: Gera uma listagem simples a partir do id  
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/helpers/cub3_formularios_helper.php
	* @param 
	* @return 
	*/
	function gerarListagem($id, $controller = null)
	{  
		$CI =& get_instance();
		$CI->load->model("cub3_listagem_model");

		if(is_null($controller)){ 
			// Verifica se existe um código ou slug passado por parâmetro
			$verificarId = $CI->cub3_listagem_model->listagemPorCodigoOuSlug($id); 
			$listagem  = $verificarId->row();   
			if($listagem != null)
				$controller  = ($listagem->lisController ==  '') ? 'Cub3ListagemSimplesCtrl' : $listagem->lisController;
			else
				$controller  = "Cub3ListagemSimplesCtrl";
		}
		else
			$controller  = "Cub3ListagemSimplesCtrl";

		if(!is_null($id)):
			echo '<div ><div ng-controller="Cub3ListagemSimplesCtrl" ng-cloak  ng-init="getListagem(\''.$id.'\');"><cub3-carregar-spinner ng-show="!dadosCarregados"></cub3-carregar-spinner>'.
					  	'<montar-grid titulos="cub3Listagem.titulos" valores="cub3Listagem.dados" ng-if="dadosCarregados" botoes="cub3Listagem.botoes" header="cub3Listagem.header"  tema="material" ctrl="'.$controller.'" ng-init="lisCodigo = '.$id.'"></montar-grid>  '.
					  ' <div class="panel panel-danger" ng-if="exibirErro"> <div class="panel-heading"> <h3 class="panel-title">Ocorreu um erro!</h3> </div> <div class="panel-body"> A página atual não pode ser exibida. Por favor, entre em contato com o suporte da CUB3. </div> </div>'.
					'</div></div>'; 
		else:
			echo '<blockquote>' .
				  '<small>Identificação inválida.</small>' .
				'</blockquote>';
		endif;

	} 
	function tratarOrigem($local,$arquivo){
		if(strpos($arquivo, "http://graph.facebook.com") == false){
			return $arquivo;
		}else{
			return $local."/".$arquivo;
		}

	}
	function tratarTamanho($imagem){
		$tamanho = getimagesize($imagem);
		if($tamanho[0] > 300){
			$imagem = str_replace(base_url(), "", $imagem);
			//Calcula nova altura da imagem
			$novaAltura = $tamanho[1]/$tamanho[0] * 300;

			$CI =& get_instance();
			$CI->load->library('wideimage/WideImage');
			$CI->wideimage->load($imagem)->resize(300, $novaAltura, 'outside')->saveToFile($imagem);
		}
	}
	function tratarTamanhoAreas($diretorio, $imagem){
		$tamanho = getimagesize($diretorio.$imagem);
		
		$CI =& get_instance();
		$CI->load->library('wideimage/WideImage');

		if($tamanho[0] > 0 && $tamanho[0] < 640){
			//Calcula nova altura da imagem
			$novaAltura = $tamanho[1]/$tamanho[0] * 320;

			$nome = strtolower($diretorio."mini.".$imagem);
			$CI->wideimage->load($diretorio.$imagem)->resize(320, $novaAltura, 'inside')->saveToFile($nome);
		}
		if($tamanho[0] >= 640 && $tamanho[0] < 728){
			//Calcula nova altura da imagem
			$novaAltura = $tamanho[1]/$tamanho[0] * 640;

			$nome = strtolower($diretorio."media.".$imagem);
			$CI->wideimage->load($diretorio.$imagem)->resize(640, $novaAltura, 'inside')->saveToFile($nome);
		}
		if($tamanho[0] >= 728){
			//Calcula nova altura da imagem
			$novaAltura = $tamanho[1]/$tamanho[0] * 728;

			$nome = strtolower($diretorio."maxima.".$imagem);
			$CI->wideimage->load($diretorio.$imagem)->resize(728, $novaAltura, 'inside')->saveToFile($nome);
		}
	}
	function gerarImagens(){
		$CI =& get_instance();
		$CI->load->model("Profissionais_model");
		$CI->load->model("Pacientes_model");
		$CI->load->model("Areas_model");

		$imagens 	= array();
		$tipo 		= $CI->input->post("tipo") == null ? $CI->input->get("tipo") : $CI->input->post("tipo"); 
		$absPath 	= $CI->input->post("absPath") == null ? $CI->input->get("absPath") : $CI->input->post("absPath"); 		
		$tamanho 	= $CI->input->post("tamanho") == null ? $CI->input->get("tamanho") : $CI->input->post("tamanho"); 		

		switch ($tipo) {
			case 'pacientes':
					//Pacientes
					$dados 		= $CI->Pacientes_model->pacienteImagem();
					foreach ($dados->result() as $key => $value) {
						tratarTamanho($value->pacImagem); 
							array_push($imagens, $tamanho.".".$value->pacImagem); 
					}
				break;
			case 'profissionais':
					//Profissionais
					$dados 		= $CI->Profissionais_model->profissionalImagem();
					foreach ($dados->result() as $key => $value) {
						tratarTamanho("others/uploads/profissionais/fotos/".$value->proImagem);
						if($absPath)
							array_push($imagens, base_url()."others/uploads/profissionais/fotos/".$tamanho.".".$value->proImagem);
						else
							array_push($imagens, "_".$tamanho.".".$value->proImagem);
					}
				break;
			case 'areas':
					//Áreas
					$dados 		= $CI->Areas_model->areaImagem();
					foreach ($dados->result() as $key => $value) {
						if(file_exists("others/uploads/conteudo/". $value->areImagem)){
							if($tamanho != ''){
								if(!file_exists("others/uploads/conteudo/".$tamanho.".".$value->areImagem))
									tratarTamanhoAreas("others/uploads/conteudo/", $value->areImagem);
							}

							if($absPath){
								$caminhoPersonalizado = base_url()."others/uploads/conteudo/".$tamanho.".".$value->areImagem;
								if(file_exists($caminhoPersonalizado))
									array_push($imagens, array('original' => $value->areImagem, 'convertido' => $caminhoPersonalizado ));
								else
									array_push($imagens,array('original' => base_url()."others/uploads/conteudo/".$value->areImagem ));
							}
							else {
								$caminhoPersonalizado = $tamanho.".".$value->areImagem;
								if(file_exists("others/uploads/conteudo/".$caminhoPersonalizado))
									array_push($imagens, array('original' => $value->areImagem, 'convertido' => $tamanho.".".$value->areImagem ));
								else
									array_push($imagens, array('original' => $value->areImagem ));
							}
						}
					}
				break;
		}

		retornarJson(null, $imagens);
	}
