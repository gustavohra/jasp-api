<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

  function token_get($sessao)
{
    $CI =& get_instance();
    $CI->load->helper("authorization");
    $CI->load->helper("jwt");
    $tokenData = $sessao;
    $tokenData['id'] = 1; //TODO: Replace with data for token
    $output  = AUTHORIZATION::generateToken($tokenData);
    return $output;
}
  function auth_token($sessao)
{
    $CI =& get_instance();
    $CI->load->helper("authorization");
    $CI->load->helper("jwt");
    $sessao = json_decode($sessao, true);

    if (isset($sessao['token'])) {
        $decodedToken = AUTHORIZATION::validateToken($sessao['token']);
        if ($decodedToken != false) {
            retornarJson(null, array('resposta' => true, 'dados' => $decodedToken));
        }
    }
    retornarJson(null,  array('resposta' => false ));
}