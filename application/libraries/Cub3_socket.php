<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	/*
	* CUB3 / Classes
	*
	* [Descrição]: Library para integração com sockets, utilizando o Elephant.io
	* [Criação]: 04/05/2016 às 17:21:22
	*
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/previna/application/libraries/cub3_boleto.php
	*/
	use ElephantIO\Client,
	    ElephantIO\Engine\SocketIO\Version1X,
	    ElephantIO\Exception\ServerConnectionFailureException;

	class Cub3_socket {

	  var $ci;
	  var $session = false;
		function __construct() { 
	    		$this->ci =& get_instance();
	 			$this->ci->load->driver('session');
		}
			/*
		* [Método]: iniciarConexao
		* [Descrição]: Realiza uam conexão de teste e retorna o resultado
		* [Comentários]: 
		* 
		* @author gustavoaguiar
		* @package None
		* @param 
		* @return 
		*/
		public function iniciarConexao()
		{  
			$client = new \ElephantIO\Client(new \ElephantIO\Engine\SocketIO\Version1X($this->ci->config->item("cub3_servidor_socket")));

				try
				{
				    $client->initialize(); 
				    $client->close();
				    return true;
				}
				catch (ServerConnectionFailureException $e)
				{
				    return false;
				}
		} 
	}
	
