<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once( APPPATH . 'libraries/facebook/php-sdk-v4/autoload.php' );
 
// Classes do Facebook
 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

// use Facebook\FacebookRequest;
// use Facebook\GraphUser;
// use Facebook\FacebookRequestException;
 
class Cub3_Facebook
{
  var $ci;
  var $session = false;
 
  public function __construct()
  {
    // Atalho para o CI
    $this->ci =& get_instance();
 
    // Inicia o SDK
    FacebookSession::setDefaultApplication( $this->ci->config->item('api_id', 'cub3_facebook'), $this->ci->config->item('app_secret', 'cub3_facebook') );
  }
 
  // Retorna a sessão ativa do FB
  public function get_session()
  {

    if ( $this->ci->session->userdata('fb_token') ) {
        // Valida o token de acordo com o valor armazenado em sessão
      $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );
      
      try {
        if ( ! $this->session->validate() ) {
          $this->session = false;
        }
      } catch ( Exception $e ) {

        $this->session = false;
      }
    }
    else
    {

      $helper = new FacebookRedirectLoginHelper( $this->ci->config->item('redirect_url', 'cub3_facebook') );
      try {
        $this->session = $helper->getSessionFromRedirect();
      } catch( FacebookRequestException $ex ) {

        print_r($ex->getResponse());

      } catch( \Exception $ex ) {
        print_r($ex->getResponse());

      }
    }
  }


  // Método de Login
  public function login()
  {
    $this->get_session();
    if ( $this->session )
    {
      $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );
 
      $user = $this->get_user();

      if ( $user && ! empty( $user['email'] ) )
      {
         $result = $this->ci->user_model->get_user( $user['email'] );

          if ( ! $result )
          {
            // Caso o usuário não esteja registrado, não usaremos no caso, já que os clientes precisam possuir um cadastro ativo
            $this->ci->session->set_flashdata( 'fb_user', $user );
            redirect( base_url( 'home' ) );
          }
          else
          {
            if ( $this->ci->user_model->sign_in( $result->username, $result->password ) )
            {
              // Caso esteja tudo ok
              redirect( base_url( 'home' ) );
            }
            else
            {
              // Caso ocorra algum erro
              die( 'ERROR' );
              redirect( base_url( 'login' ) );
            }
          }
      }
      else
      {
        die( 'ERROR' );
      }
    }
  }
 
  // Retorna a URL de login
  public function login_url()
  {
    $helper = new FacebookRedirectLoginHelper( $this->ci->config->item('redirect_url', 'cub3_facebook') );
 
    return $helper->getLoginUrl( $this->ci->config->item('permissions', 'cub3_facebook') );
    // Retorna a autenticação via FB
  }
 
  // Retorna o perfil do usuário
  public function get_user()
  {
    $this->get_session();
    if ( $this->session )
    {
      // O campo fields lista os parâmetros para retorno. Todos os parâmetros podem ser acessados através do link:
      // https://developers.facebook.com/docs/graph-api/reference/v2.5/profile
      $request = ( new FacebookRequest( $this->session, 'GET', '/me?fields=id,first_name,last_name,email,about,birthday,hometown,link' ) )->execute();
      $user    = $request->getGraphObject()->asArray();
 
      return $user;
    }
    return false;
  }
 
  // Retorna a foto do perfil do usuário
  public function get_profile_pic( $user_id )
  {
    $this->get_session();
    if ( $this->session )
    {
      $request = ( new FacebookRequest( $this->session, 'GET', '/' . $user_id . '/picture?redirect=0&type=large' ) )->execute();
      $pic     = $request->getGraphObject()->asArray();
 
      if ( ! empty( $pic ) && ! $pic['is_silhouette'] ) {
        return $pic['url'];
      }
    }
    return false;
  }

  // Métodos para Fanpage
  public function get_fanpage($metricas = null)
  {
    if($this->ci->session->userdata('fb_token') == null)
      $this->ci->session->set_userdata(array('fb_token' => $this->solicitarToken()));

    $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') ); 
    // As metricas deverão ser passadas com separação por vírgula, ex: page_stories,page_impressions...
    $fanPageId  = $this->ci->config->item("idFanPage", "cub3_facebook");
    $request = ( new FacebookRequest( $this->session, 'GET', '/' . $fanPageId . '/photos?type=uploaded' ) )->execute();
    $retorno = $request->getGraphObject()->asArray();

    if($retorno != null)
      return $retorno;
    else
      return false;

  }
    /*
  * [Método]: get_foto_por_id
  * [Descrição]: 
  * [Comentários]: 
  * 
  * @author gustavoaguiar
  * @package /Applications/MAMP/htdocs/telmaadvincula/application/libraries/Cub3_Facebook.php
  * @param 
  * @return 
  */
  public function get_foto_por_id($id)
  {
    if($this->ci->session->userdata('fb_token') == null)
      $this->ci->session->set_userdata(array('fb_token' => $this->solicitarToken()));

    $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );   
    $request = ( new FacebookRequest( $this->session, 'GET', '/' . $id ) )->execute();
    $retorno = $request->getGraphObject()->asArray();

    if($retorno != null)
      return $retorno;
    else
      return false;

  }


    /*
  * [Método]: solicitarToken
  * [Descrição]: 
  * [Comentários]: 
  * 
  * @author gustavoaguiar
  * @package /Applications/MAMP/htdocs/telmaadvincula/application/libraries/Cub3_Facebook.php
  * @param 
  * @return 
  */
  private function solicitarToken()
  {
      $optional_headers = null;

      $urlApi = "https://graph.facebook.com/oauth/access_token?client_id=".$this->ci->config->item("api_id", "cub3_facebook")."&client_secret=".$this->ci->config->item("app_secret", "cub3_facebook")."&grant_type=client_credentials";
      $token  = file_get_contents($urlApi);
      $token  = explode("=", $token)[1]; 

      if ($token === null) {
      return "Ocorreu um erro: Token inválido";
      }

      return $token;
  }


}