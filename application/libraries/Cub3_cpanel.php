<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Gufy\CpanelPhp\Cpanel;

class Cub3_cpanel
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->config("cub3_cpanel");

        // Define uma conexão principal com base nas config estabelecidas em cub3_cpanel
		$cpanel = new \Gufy\CpanelPhp\Cpanel([
			"host"				=>  $this->ci->config->item("host", "cub3_cpanel"),
			"username"			=>  $this->ci->config->item("usuario", "cub3_cpanel"),
			"auth_type"			=>  $this->ci->config->item("auth_type", "cub3_cpanel"),
			"password"			=>  $this->ci->config->item("senha", "cub3_cpanel")
			]);

		$this->ci->cpanel = $cpanel;
	}

	public function listarContasGeral()
	{
		// Caso não queira a listagem do domínio principal, setar para 1

		$queryDominio 	= array(
			"maxaccounts"	=> "100",
			"no_validade"	=> "1",
	        'regex'        	=> '/^[a-z0-9_-]{6,18}$/'
			);

		$contasEmail = json_decode($this->ci->cpanel->cpanel('Email', 'listpopswithdisk', $queryDominio ))->cpanelresult->data;

		return $contasEmail;
	}

	public function listarContasPorDominio($dominio)
	{
		// Caso não queira a listagem do domínio principal, setar para 1
		$dominio 		= $dominio["cliDominio"];
		$queryDominio 	= array(
			"domain"		=> urlencode($dominio),
			"maxaccounts"	=> "100",
			"no_validade"	=> "1",
	        'regex'        	=> '/^[a-z0-9_-]{6,18}$/'
			);

		$arrayDominios 	= array();

		// Realiza a requisição ao cPanel
		$dominios = json_decode($this->ci->cpanel->cpanel('Email', 'listpopswithdisk', $queryDominio ))->cpanelresult->data;
		// Retorna: txtdiskquota, diskquotas, diskusedpercent, mtime, diskused, humandiskquota, _diskused, login, email, domain, user

		for ($i=0; $i < count($dominios); $i++) { 
			$aux 		= $dominios[$i];
			
			if($this->verificarDominio($aux->email, $dominio))
				array_push($arrayDominios, $aux);
		}

		return $arrayDominios;
	}	

	public function listarContasFtpPorDominio($dominio)
	{
		// Caso não queira a listagem do domínio principal, setar para 1
		$dominio 		= $dominio["cliDominio"];

		$arrayDominios 	= array();

		// Realiza a requisição ao cPanel
		$dominios = json_decode($this->ci->cpanel->cpanel('Ftp', 'listftpwithdisk', $dominio))->cpanelresult->data;

		// Retorna: txtdiskquota, diskquotas, diskusedpercent, mtime, diskused, humandiskquota, _diskused, login, email, domain, user

		for ($i=0; $i < count($dominios); $i++) { 
			$aux 		= $dominios[$i];
			
			if($this->verificarDominioFtp($aux->reldir, $dominio))
				array_push($arrayDominios, $aux);
			//if($this->verificarDominio($aux->serverlogin, $dominio))
		}

		return $arrayDominios;
	}

	public function listarContasSubDominio($dominio)
	{

		// Caso não queira a listagem do domínio principal, setar para 1
		$dominio 		= $dominio["cliDominio"];

		$arrayDominios 	= array();

		// Realiza a requisição ao cPanel
		$dominios 		= json_decode($this->ci->cpanel->cpanel('SubDomain', 'listsubdomains', $dominio))->cpanelresult->data;

		// Retorna: txtdiskquota, diskquotas, diskusedpercent, mtime, diskused, humandiskquota, _diskused, login, email, domain, user

		for ($i=0; $i < count($dominios); $i++) { 
			$aux 		= $dominios[$i];
			
			if($this->verificarSubDominio($aux->rootdomain, $dominio))
				array_push($arrayDominios, $aux);
			//if($this->verificarDominio($aux->serverlogin, $dominio))
		}

		return $arrayDominios;
	}	

	public function listarBancosDeDados($login)
	{
		// Caso não queira a listagem do domínio principal, setar para 1
		$login 			= $login["cptLogin"];

		$arrayBancos 	= array();

		// Realiza a requisição ao cPanel
		$bancos 		= json_decode($this->ci->cpanel->cpanel('MysqlFE', 'getalldbusersanddbs', $login))->cpanelresult->data;

		// Retorna: dbusers, db, size, reason, result

		for ($i=0; $i < count($bancos); $i++) { 
			$aux 		= $bancos[$i];
			
			// O recebimento dos dbs é em array. A função abaixo percorre o array e monta um novo para retornar
			if($this->verificarBancoDeDados($aux->dbuser, $login)){
				for ($j=0; $j < count($aux->dbs) ; $j++) { 
					$auxDb 			= array(
											"db" 	=> $aux->dbs[$j],
											"user"	=> $aux->dbuser
											);
					array_push($arrayBancos, $auxDb);
				}			
			}
		}

		return $arrayBancos;
	}			
	public function inserirEmail($dominio, $email, $password, $quota) {

		$dominio = $dominio["cliDominio"];
		$usuario = array();
 
		// Valida se o endereço possui domínio ou não


		if(strpos($email, '@') !== false):
			// Verifica se o domínio equivale à requisição 
			$aux = explode("@", $email);
 
			if(!isset($usuario[1])):
				$usuario[0] = $aux[0];
				$usuario[1] = $aux[1];
			endif;
		else:
			$usuario[0] = $email;
			$usuario[1] = $dominio;
		endif;


		$nomeUsuario = $usuario[0];

		if($this->verificarDominio(($usuario[0]."@".$usuario[1]), $dominio)):
			$query 		= array(
				"domain"			=> $dominio,
				"email"				=> $nomeUsuario,
				"password"			=> $password,
				"quota"				=> $quota
			); 
		
			return json_decode($this->ci->cpanel->addEmailAccount($nomeUsuario, ($nomeUsuario."@".$dominio), $password, $quota))->cpanelresult->data[0]->result;
		else:
			return false;
		endif;
	}
	public function inserirFtp($dominio, $user, $pass, $quota, $homedir) {

		//$dominio = $dominio["cliDominio"];
		
		// Como todas as contas são subordinadas à CUB3:
		$diretorio  = "producao/" . $dominio["cliDominio"] . "/" . $homedir;
		$dominio 	= "cub3.com.br";

		$usuario 	= array();

		// Valida se o endereço possui domínio ou não
		if(strpos($user, '@') !== false) {
			// Verifica se o domínio equivale à requisição 
			$aux = explode("@", $user);

			if(!isset($usuario[1]))
			{
				$usuario[0] = $user;
				$usuario[1] = $dominio;
			}
		} else {
			$usuario[0] = $user;
			$usuario[1] = $dominio;
		}

		$nomeUsuario = $usuario[0];

		//if($this->verificarDominio(($usuario[0]."@".$usuario[1]), $dominio)):
			$query 		= array(
				'user'				=> $usuario[0],
				'pass'				=> $pass,
				'quota'				=> $quota,
				'homedir'			=> $diretorio
			);

		
			return json_decode($this->ci->cpanel->cpanel('Ftp', 'addftp', $this->ci->cpanel->getUsername(), $query))->cpanelresult->data[0]->result;

		// else:
		// 	return false;
		// endif;
	}

	public function inserirSubDominio($rootdomain, $domain, $dir) {

		//$dominio = $dominio["cliDominio"];
	
		$dominio 	= array();

		// Valida se o endereço possui domínio ou não
		if(strpos($domain, '.') !== false) {
			// Verifica se o domínio equivale à requisição 
			$aux = explode(".", $domain);

			if(!isset($dominio[1]))
			{
				$dominio[0] = $domain;
				$dominio[1] = $rootdomain["cliDominio"];
			}
			$nomeDominio = $dominio[0];
		}
		else
		{
			$nomeDominio = $domain;
		}


		//if($this->verificarDominio(($usuario[0]."@".$usuario[1]), $dominio)):
			$query 		= array(
				'domain'			=> $nomeDominio,
				'rootdomain'		=> $rootdomain["cliDominio"],
				'dir'				=> "/public_html/producao/".$rootdomain["cliDominio"]."/".$dir,
				// Permitir pontos nos sub domínios ou não?
				'disallowdot'		=> "1"
			);

			return json_decode($this->ci->cpanel->cpanel('SubDomain', 'addsubdomain', $this->ci->cpanel->getUsername(), $query))->cpanelresult->data[0]->result;

		// else:
		// 	return false;
		// endif;
	}

	public function inserirBancoDeDados($loginDB, $db) {

		$banco 					= array();

		$query 		= array(
			'db'				=> "cub3_".$db
		);

		return json_decode($this->ci->cpanel->execute_action('2','MysqlFE', 'createdb', $this->ci->cpanel->getUsername(), $query))->cpanelresult->event->result;

	}
	public function criarUsuarioBancoDeDados($loginDB, $passwordDB) {

		$banco 					= array();

		$query 		= array(
			'dbuser'				=> "cub3_".$loginDB,
			'password'				=> $passwordDB
		);

		return json_decode($this->ci->cpanel->execute_action('2','MysqlFE', 'createdbuser', $this->ci->cpanel->getUsername(), $query))->cpanelresult->event->result;

	}
	public function inserirUsuarioBancoDeDados($loginDB, $db) {

		$banco 					= array();

		$query 		= array(
			'privileges'		=> "ALL PRIVILEGES",
			'db'				=> "cub3_".$db,
			'dbuser'			=> $loginDB["cptLogin"]
		);

		return json_decode($this->ci->cpanel->cpanel('MysqlFE', 'setdbuserprivileges', $this->ci->cpanel->getUsername(), $query))->cpanelresult->event->result;

	}
	private function verificarDominio($conta, $dominio)
	{

		if(strpos($conta, '@') !== false) {
			// Verifica se o domínio equivale à requisição 
			$aux = explode("@", $conta);

			if($aux[1] == $dominio)
				return true;
			else
				return false;
		} else {
			return false;
		}

	}
	private function verificarDominioFtp($conta, $dominio)
	{
		// Valida e verifica se o diretório do domínio equivale à requisição 
		if($conta != "public_ftp" && $conta != ""):
			if(strpos($conta, '/') !== false):
				$aux = explode("/", $conta);
			
				if(count($aux) > 2):
						if($aux[1] == "producao" || $aux[1] == $dominio):
							if($aux[2] == $dominio):
								return true;
							else:
								return false;
							endif;
						else:
							return false;
						endif;
				endif;
			endif;
		else:
			return false;
		endif;
	}
	private function verificarSubDominio($rootdomain, $dominio)
	{
		// Valida e verifica se o diretório do sub dominio equivale à requisição 

		if($rootdomain == $dominio)
			return true;
		else
			return false;
	}
	private function verificarBancoDeDados($dbuser, $login)
	{
		// Valida e verifica se o diretório do sub dominio equivale à requisição 

		if($dbuser == $login)
			return true;
		else
			return false;
	}

}

/* End of file cub3_cpanel.php */
/* Location: ./application/libraries/cub3_cpanel.php */
