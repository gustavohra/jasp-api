<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Carrega bibliotecas para o analytics
require_once 'google-api-php-client/src/auth/Google_AssertionCredentials.php';
require_once 'google-api-php-client/src/Google_Client.php';
require_once 'google-api-php-client/src/contrib/Google_AnalyticsService.php';

class Cub3_analytics
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();

        $this->ci->load->config("cub3_analytics");

        $this->client_id  						= $this->ci->config->item('ga_api_clientId', 'cub3_analytics'); 
        $this->service_account_name  			= $this->ci->config->item('email', 'cub3_analytics'); 
        $this->key_file_location  				= $this->ci->config->item('p12', 'cub3_analytics'); 


		$this->client = new Google_Client();
		$this->client->setApplicationName("ApplicationName");
		$service = new Google_AnalyticsService($this->client);

		if (isset($_SESSION['service_token'])) {
		  $this->client->setAccessToken($_SESSION['service_token']);
		}

		$key = file_get_contents($this->key_file_location);
		$cred = new Google_AssertionCredentials(
		    $this->service_account_name,
		    array(
		        'https://www.googleapis.com/auth/analytics',
		    ),
		    $key,
		    'notasecret'
		);
		$this->client->setAssertionCredentials($cred);
		if($this->client->getAuth()->isAccessTokenExpired()) {
		  $this->client->getAuth()->refreshTokenWithAssertion($cred);
		}
		$_SESSION['service_token'] = $this->client->getAccessToken();

	}

	public function visitas($dataInicial, $dataFinal)
	{
		//session_start();


		$analytics = new Google_AnalyticsService($this->client);

		$profileId = $this->ci->config->item("google_analytics_id");
		$metrics = "ga:sessions";

		$optParams = array("dimensions" => "ga:date");
		$results = $analytics->data_ga->get($profileId, $dataInicial, $dataFinal, $metrics, $optParams);

		$data['report'] = $results; 

		return $data;	
	}

}

/* End of file cub3_analytics.php */
/* Location: ./application/libraries/cub3_analytics.php */
