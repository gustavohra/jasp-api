<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	/*
	* CUB3 / Classes
	*
	* [Descrição]: Library para integração com o Iugu
	*
	* @author gustavoaguiar
	*/
	
	// Biblioteca necessária para chamada de funções do Iugu
	include_once(dirname(__FILE__)."/iugu/iugu/lib/Iugu.php");

	class Cub3_iugu {

	  var $ci;
	  var $session = false;
		function __construct() { 
    		$this->ci =& get_instance();
 			$this->ci->load->driver('session');

 			// Inicia o Iugu com a chave de autenticação
			$configuracoes 	= CUB3_CONFIGURACOES;
			$apiKey 		= $configuracoes["cub3_iugu"][0]["valor"]; 
			Iugu::setApiKey($apiKey); 
		}

		/*
		* [Método]: realizarCobranca
		* [Descrição]: 
		* @author gustavoaguiar
		* @param 
		* @return 
		*/
		public function realizarCobranca($dados)
		{
			// Realiza a cobrança e retorna a resposta do iugu
			return Iugu_Charge::create($dados);
		} 
		/*
		* [Método]: realizarReembolso
		* [Descrição]: 
		* [Comentários]: 
		* 
		* @author gustavo.aguiar
		* @package /Volumes/Arquivos/Dev/www/jasp/application/libraries/Cub3_iugu.php
		* @param 
		* @return 
		*/
		public function realizarReembolso($dados)
		{
			$invoice = Iugu_Invoice::fetch($dados);
			$invoice->refund();
		}


	}
	
