<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/* 
	Configurações iniciais do Projeto
	Versão: 1.0.0
	contato@cub3.com.br 
	*/
	$configuracoes = CUB3_CONFIGURACOES;

	// De acordo com os endereços definidos, realiza a identificação do domínio
	$config['base_url'] = ''; 
	switch (true) {
		case Count($configuracoes["enderecos"]) > 1:
			for ($i=0; $i < Count($configuracoes["enderecos"]); $i++) { 
					$aux = $configuracoes["enderecos"][$i];
					
					if($_SERVER['HTTP_HOST'] == $aux["host"]){
						$config['base_url'] = $aux["base_url"]; 
						break;
					}
			}
			break;
		default: 
				$aux = $configuracoes["enderecos"][0];
				if($_SERVER['HTTP_HOST'] == $aux["host"])
					$config['base_url'] = $aux["base_url"];
 
			break;
	}  
	// Configurações de URL
	$config['uri_protocol']			= 'REQUEST_URI';
	$config['url_suffix'] 			= ''; 

	// Idioma e charset
	$config['language']				= $configuracoes["idioma"]; 
	$config['charset'] 				= $configuracoes["codificacao"]; 
	$config['enable_hooks'] 		= FALSE;
	$config['subclass_prefix'] 		= 'MY_'; 
	$config['composer_autoload'] 	= filter_var($configuracoes["composer_autoload"], FILTER_VALIDATE_BOOLEAN); 
	$config['permitted_uri_chars'] 	= 'a-z 0-9~%.:_\-='; 
	$config['allow_get_array'] 		= TRUE;
	$config['enable_query_strings'] = FALSE;
	$config['controller_trigger'] 	= 'c';
	$config['function_trigger'] 	= 'm';
	$config['directory_trigger'] 	= 'd'; 

	// Log e tratamento de erros
	$config['log_threshold'] 		= 0; 
	$config['log_path'] 			= ''; 
	$config['log_file_extension'] 	= ''; 
	$config['log_file_permissions'] = 0644; 
	$config['log_date_format'] 		= 'Y-m-d H:i:s'; 
	$config['error_views_path'] 	= ''; 
	
	// Cache
	$config['cache_path'] 			= '';
	$config['cache_query_string'] 	= FALSE;

	// Criptografia
	$config['encryption_key'] 			= base64_decode($configuracoes["encryption_key"]);

	// Sessão
	$config['sess_driver'] 				= 'files';
	$config['sess_cookie_name'] 		= 'ci_session';
	$config['sess_expiration'] 			= intval($configuracoes["sess_expiration"]);
	$config['sess_save_path'] 			= NULL;
	$config['sess_match_ip'] 			= FALSE;
	$config['sess_time_to_update'] 		= 300;
	$config['sess_regenerate_destroy'] 	= FALSE;

	// Cookies
	$config['cookie_prefix']			= '';
	$config['cookie_domain']			= '';
	$config['cookie_path']				= '/';
	$config['cookie_secure']			= FALSE;
	$config['cookie_httponly'] 			= FALSE;

	// Tratamento de quebra de linha
	$config['standardize_newlines'] 	= FALSE;

	// Tratamento de XSS
	$config['global_xss_filtering'] 	= FALSE;
	$config['csrf_protection'] 			= FALSE;
	$config['csrf_token_name']		 	= 'csrf_test_name';
	$config['csrf_cookie_name'] 		= 'csrf_cookie_name';
	$config['csrf_expire'] 				= 7200;
	$config['csrf_regenerate'] 			= TRUE;
	$config['csrf_exclude_uris'] 		= array();

	// Compressão de exibição em gzip
	$config['compress_output'] 			= FALSE; 

	$config['time_reference'] 			= 'local'; 

	// Reescrita de tags
	$config['rewrite_short_tags'] 		= FALSE; 

	// Proxy
	$config['proxy_ips'] = '';

	// Configurações adicionais
	$config["cub3_servidor_socket"]					= "http://messenger.cub3.com.br:49998";
	$config["portal_nome"]							= $configuracoes["nome"];
	$config["nome_app"]								= $configuracoes["nome_app"];
	$config["versao_sistema"]						= $configuracoes["versao"];
	$config['template_tituloSistema'] 				= $configuracoes["nome"];
	$config['template_logo'] 						= $configuracoes["logo"];
	$config['template_logo_light']					= isset($configuracoes["logo_light"]) ? $configuracoes["logo_light"] : '';
	$config["tema_principal"]						= $configuracoes["tema_principal"];  
	$config["carregar_scripts"]						= filter_var($configuracoes["carregar_scripts"], FILTER_VALIDATE_BOOLEAN); 

	// Monta a paleta de cores do projeto
	switch (true) {
		case Count($configuracoes["cores"]) > 1:
			for ($i=0; $i < Count($configuracoes["cores"]); $i++) { 
					$aux 						= $configuracoes["cores"][$i]; 
					$config["cor-".$aux["id"]] 	= $aux["valor"];  
			}
			break;
		default: 
				$aux 							= $configuracoes["cores"][0];
				$config["cor-".$aux["id"]] 		= $aux["valor"];  
			break;
	}  	

	// Configurações de e-mail
	$config["cub3_config_email"]["smtp_host"] 		= $configuracoes["config_email"]["smtp_host"];
	$config["cub3_config_email"]["smtp_port"] 		= intval($configuracoes["config_email"]["smtp_port"]); 
	$config["cub3_config_email"]["smtp_user"] 		= $configuracoes["config_email"]["smtp_user"];
	$config["cub3_config_email"]["smtp_pass"] 		= base64_decode($configuracoes["config_email"]["smtp_pass"]);
	$config["cub3_config_email"]["charset"] 		= $configuracoes["config_email"]["charset"];

	// Configurações do Git
	$config["cub3_git"]["usuario"] 					= "admin";
	$config["cub3_git"]["senha"] 					= "dGhlYm9tYjE=";
	$config["cub3_git"]["repositorio"] 				= "http://cub3.com.br/cub3_cdn/urlSistema/";					

	// Configurações API Facebook
	switch (true) {
		case Count($configuracoes["cub3_facebook"]) > 1:
			for ($i=0; $i < Count($configuracoes["cub3_facebook"]); $i++) { 
					$aux 										= $configuracoes["cub3_facebook"][$i]; 
					$config["cub3_facebook"][$aux["campo"]] 	= $aux["valor"];  
			}
			break;
		default: 

				$aux 											= $configuracoes["cub3_facebook"][0];
				$config["cub3_facebook"][$aux["campo"]] 		= $aux["valor"];  
			break;
	}  	

	// Configurações de redes sociais

		switch (true) {
			case Count($configuracoes["redes_sociais"]) > 1:
				for ($i=0; $i < Count($configuracoes["redes_sociais"]); $i++) { 
						$aux 										= $configuracoes["redes_sociais"][$i]; 
						$config["redes_sociais"][$aux["nome"]] 		= $aux["link"];  
				}
				break;
			default: 

					$aux 											= $configuracoes["redes_sociais"][0];
					$config["redes_sociais"][$aux["nome"]] 			= $aux["link"];  
				break;
		}  	
	$config["google_analytics_id"] 				= isset($configuracoes["google_analytics_id"]) ? $configuracoes["google_analytics_id"] : '';