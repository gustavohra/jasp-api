<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Recebe o arquivo de configurações do projeto
$configuracoes 			= CUB3_CONFIGURACOES;
$autoload['packages'] 	= array(); 
$autoload['libraries'] 	= $configuracoes["libraries"]; 
$autoload['drivers'] 	= $configuracoes["drivers"]; 
$autoload['helper'] 	= $configuracoes["helper"]; 
$autoload['config'] 	= $configuracoes["config"]; 
$autoload['language'] 	= array(); 
$autoload['model'] 		= array();
