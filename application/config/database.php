<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/* 
	Configurações do banco de dados
	Versão: 1.0.0
	contato@cub3.com.br 
	*/

	// Recebe o arquivo de configurações do projeto
	$configuracoes 		= CUB3_CONFIGURACOES;
	$active_group 		= 'default';
	$query_builder 		= TRUE; 
	$aux 				= array();
	$aux["hostname"]	= "";
	$verificacao		= true;

	$db['default'] = array(
		'dsn'		=> '',  
		'dbdriver' 	=> 'mysqli',
		'dbprefix' 	=> '',
		'pconnect' 	=> FALSE,
		'db_debug' 	=> (ENVIRONMENT !== 'production'),
		'cache_on' 	=> FALSE,
		'cachedir' 	=> '',
		'char_set' 	=> 'utf8',
		'dbcollat' 	=> 'utf8_general_ci',
		'swap_pre' 	=> '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);  
	switch (true) {
		case Count($configuracoes["db"]) > 1:
			for ($i=0; $i < Count($configuracoes["db"]); $i++) { 
					$aux = $configuracoes["db"][$i];
					if($_SERVER['HTTP_HOST'] == $aux["hostname"]){
						$db['default']["hostname"] = $aux["hostname"];
						$db['default']["username"] = $aux["username"];
						$db['default']["password"] = decifracub3($aux["password"]);
						$db['default']["database"] = $aux["database"]; 
						break;
					}
			}
			break;
		default: 
				$aux 				= $configuracoes["db"][0];  
				$db['default']["hostname"] = $aux["hostname"];
				$db['default']["username"] = $aux["username"];
				$db['default']["password"] = decifracub3($aux["password"]);
				$db['default']["database"] = $aux["database"]; 
			break;
	} 
