<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

	// Recebe o arquivo de configurações do projeto e monta as rotas
	$configuracoes 						= CUB3_CONFIGURACOES;
	switch (true) {
		case Count($configuracoes["rotas"]) > 1:
			for ($i=0; $i < Count($configuracoes["rotas"]); $i++) { 
					$aux 				= $configuracoes["rotas"][$i]; 
					$route[$aux["titulo"]] = $aux["caminho"];  
			}
			break;
		default: 
				$aux = $configuracoes["rotas"][0]; 
				$route[$aux["titulo"]] = $aux["caminho"];
			break;
	}  

	$route['translate_uri_dashes'] 	= FALSE; 