<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_grupo_model extends CI_Model {
	/*
	* [Método]: grupoListarTodos
	* [Descrição]: Lista todos os grupos da base de dados em cruzamento com as unidades associadas
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Grupo_model.php
	* @param 
	* @return 
	*/
	public function grupoListarTodos(){
		$sql = "SELECT * FROM cub3_grupo GRU, cub3_unidade UNI WHERE UNI.uniCodigo <> 1 AND UNI.uniCodigo = GRU.uniCodigo ORDER BY gruDescricao ASC";
		return $this->db->query($sql);
	}
	/*
	* [Método]: grupoSelecionarCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Grupo_model.php
	* @param 
	* @return 
	*/
	public function grupoSelecionarCodigo($gruCodigo){
		$sql = "SELECT * FROM cub3_grupo GRU, cub3_unidade UNI WHERE UNI.uniCodigo = GRU.uniCodigo AND GRU.gruCodigo = ?";
		return $this->db->query($sql,array($gruCodigo));
	}
	/*
	* [Método]: grupoPorPorUnidade
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Grupo_model.php
	* @param 
	* @return 
	*/
	public function grupoPorPorUnidade($uniCodigo){
		$sql = "SELECT * FROM cub3_grupo GRU, cub3_unidade UNI WHERE UNI.uniCodigo = GRU.uniCodigo AND GRU.uniCodigo = ?";
		return $this->db->query($sql,array($uniCodigo));
	}
	/*
	* [Método]: grupoPorUsuario
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Grupo_model.php
	* @param 
	* @return 
	*/
	public function grupoPorUsuario($usuCodigo){
		$sql = "SELECT * FROM cub3_grupo_has_usuario SGU, cub3_grupo GRU WHERE SGU.usuCodigo = ".$usuCodigo." AND SGU.gruCodigo = GRU.gruCodigo";
		return $this->db->query($sql);
	}
}

/* End of file grupo_model.php */
/* Location: ./application/models/grupo_model.php */