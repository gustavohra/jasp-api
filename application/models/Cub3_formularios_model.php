<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 29/04/2016 às 19:14:05
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_formularios_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
		/*
	* [Método]: formulariosListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function formulariosListar($listaOrganizarCampo = "forTitulo", $listaOrganizarOrdem = "ASC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_formulario WHERE forStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_formulario WHERE forStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}

	/*
	* [Método]: formularioListarPorSlug
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarPorSlug($forSlug) {
		$sql = "SELECT * FROM cub3_formulario WHERE forSlug = '".$forSlug."'"; 
		return $this->db->query($sql);
	}

	/*
	* [Método]: formularioListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarPorCodigo($forCodigo) {
		$sql = "SELECT *, (SELECT usuNome FROM cub3_usuario WHERE usuCodigo = cub3_formulario.usuCodigo) AS usuNome, (SELECT usuImagem FROM cub3_usuario WHERE usuCodigo = cub3_formulario.usuCodigo) AS usuImagem  FROM cub3_formulario WHERE forStatus = 'ATIVO' AND forCodigo = ".$forCodigo."";
		return $this->db->query($sql);
	}

	/*
	* [Método]: formularioListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarPorCodigoUnico($forCodigo) {
		$sql = "SELECT * FROM cub3_formulario WHERE forStatus = 'ATIVO' AND forCodigo = ".$forCodigo."";
		return $this->db->query($sql);
	}
	/*
	* [Método]: formularioListarCamposPorCodigo
	* [Descrição]: Lista todos os campos do formulário recebendo um forCodigo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarCampos($forCodigo) {
		$sql = "SELECT * FROM cub3_formulario_campo WHERE forCodigo = ".$forCodigo." AND fctStatus = 'ATIVO' ORDER BY fctOrdem ASC";
		return $this->db->query($sql);
	}	

	/*
	* [Método]: formularioListarCamposPorCodigo
	* [Descrição]: Lista todos os campos do formulário recebendo um forCodigo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarCampoPorCodigo($fctCodigo) {
		$sql = "SELECT * FROM cub3_formulario_campo WHERE fctCodigo = ".$fctCodigo." AND fctStatus = 'ATIVO' ORDER BY fctOrdem ASC";
		return $this->db->query($sql);
	}	
	/*
	* [Método]: formularioListarCamposPorCodigo
	* [Descrição]: Lista todos os campos do formulário recebendo um forCodigo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarCamposTipo($forCodigo) {
		$sql = "SELECT * FROM cub3_formulario_campo_tipo WHERE frcStatus = 'ATIVO' AND focCodigo = ".$forCodigo."";
		return $this->db->query($sql);
	}	
	/*
	* [Método]: formularioListarCamposPorCodigo
	* [Descrição]: Lista todos os campos do formulário recebendo um forCodigo
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function formularioListarClasses() {
		$sql = "SELECT * FROM cub3_formulario_campo_classe ORDER BY fccCodigo ASC";
		return $this->db->query($sql);
	}		

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function formularioVerificar($id)
	{
		$aValid = array('-', '_'); 

		if(!ctype_alnum(str_replace($aValid, '', $id)))  
			$sql = "SELECT * FROM cub3_formulario WHERE forCodigo = ".$id;
		else
			$sql = "SELECT * FROM cub3_formulario WHERE BINARY forSlug = BINARY '".$id."'";

		return $this->db->query($sql);

	}
	public function formularioCampoOptionsUrl($fctCodigo) { 
					$sql = "SELECT cub3_formulario_campo.fctOptionsUrl AS urlResposta FROM cub3_formulario_campo WHERE cub3_formulario_campo.fctCodigo = ".$fctCodigo." LIMIT 0,1"; 
		return $this->db->query($sql);
	}


 } 
