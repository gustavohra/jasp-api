<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Blog_model
	* [Criação]: 29/04/2016 às 01:09:18
	* 
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_blog_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* [Método]: blogListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListar($listaOrganizarCampo = "bpoCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT *, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT *, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
	/*
	* [Método]: blogListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarPorCodigo($bpoCodigo) {
		$sql = "SELECT *, bpoThumbnail AS anexo, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' AND bpoCodigo = ".$bpoCodigo."";
		return $this->db->query($sql);
	}

	/*
	* [Método]: blogListarCategoriaPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarCategoriaPorCodigo($bcaCodigo) {
		$sql = "SELECT * FROM cub3_blog_categoria WHERE bcaCodigoPai  IS NULL AND bcaStatus = 'ATIVO' AND bcaCodigo = ".$bcaCodigo."";
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarPorSlug($bpoSlug) {
		$sql = "SELECT *, bpoThumbnail AS anexo, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' AND bpoSlug = '".$bpoSlug."'";
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarCategoriaPorSlug($bcaSLug) {
		$sql = "SELECT * FROM cub3_blog_categoria WHERE bcaSLug = '".$bcaSLug."'";
		return $this->db->query($sql);
	}


	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarPorVisualizacoes($ordenacao = 'DESC') {
		$sql = "SELECT *, bpoThumbnail AS anexo, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' ORDER BY bpoVisualizacoes ".$ordenacao." LIMIT 0,3";
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarPorCategoria($qtd = 3, $bcaCodigo = 0) {
		$sql = "SELECT cub3_blog_posts.*, cub3_blog_posts.bpoThumbnail AS anexo, (SELECT usuNome FROM cub3_usuario WHERE cub3_blog_posts.usuCodigo = cub3_usuario.usuCodigo LIMIT 0,1) AS usuNome, cub3_blog_posts_has_cub3_blog_categoria.bpoCodigo FROM cub3_blog_posts_has_cub3_blog_categoria INNER JOIN cub3_blog_posts ON cub3_blog_posts_has_cub3_blog_categoria.bpoCodigo = cub3_blog_posts.bpoCodigo WHERE cub3_blog_posts_has_cub3_blog_categoria.bcaCodigo = ".$bcaCodigo." ORDER BY cub3_blog_posts_has_cub3_blog_categoria.bpoCodigo DESC LIMIT 0,".$qtd;
		return $this->db->query($sql);
	}   
		/*
	* [Método]: limparPostCategorias
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/telmaadvincula/application/models/Cub3_blog_model.php
	* @param 
	* @return 
	*/
	public function limparPostCategorias($bpoCodigo)
	{
		$sql = "DELETE FROM cub3_blog_posts_has_cub3_blog_categoria WHERE bpoCodigo = ".$bpoCodigo;
		return $this->db->query($sql, array());  
	}
 

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarTags() {
		$sql = "SELECT bpoTags FROM cub3_blog_posts WHERE bpoStatus = 'ATIVO' and bpoTags != ''";
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarCategorias() {
		$sql = "SELECT * FROM cub3_blog_categoria  WHERE bcaCodigoPai IS NULL AND  bcaStatus = 'ATIVO'";
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarSubCategorias($bcaCodigo) {
		$sql = "SELECT * FROM cub3_blog_categoria  WHERE bcaCodigoPai = ".$bcaCodigo." AND  bcaStatus = 'ATIVO'";
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarCategoriasSelect() {
		$sql = "SELECT cub3_blog_categoria.bcaTitulo AS name, cub3_blog_categoria.bcaCodigo AS value, (SELECT bca.bcaTitulo FROM cub3_blog_categoria AS bca WHERE bca.bcaCodigo = cub3_blog_categoria.bcaCodigoPai) AS 'group' FROM cub3_blog_categoria  WHERE cub3_blog_categoria.bcaStatus = 'ATIVO'";
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarCategoriasPorBpoCodigo($bpoCodigo) {
		$sql = "SELECT cub3_blog_posts_has_cub3_blog_categoria.bcaCodigo AS value, cub3_blog_categoria.bcaTitulo AS name, (SELECT bca.bcaTitulo FROM cub3_blog_categoria AS bca WHERE bca.bcaCodigo = cub3_blog_categoria.bcaCodigoPai) AS 'group'  FROM cub3_blog_posts_has_cub3_blog_categoria, cub3_blog_categoria WHERE cub3_blog_posts_has_cub3_blog_categoria.bpoCodigo = ".$bpoCodigo." AND cub3_blog_categoria.bcaCodigo = cub3_blog_posts_has_cub3_blog_categoria.bcaCodigo";
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarVisualizacoesPorBpoCodigo($bpoCodigo) {
		$sql = "SELECT COUNT(*) AS valor FROM cub3_blog_posts_visualizacao  WHERE bpoCodigo = ".$bpoCodigo;
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function blogListarSliders() {
		$sql = "SELECT * FROM cub3_blog_slider WHERE blsStatus = 'ATIVO'";
		return $this->db->query($sql);
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function sliderPorCodigo($blsCodigo) {
		$sql = "SELECT * FROM cub3_blog_slider WHERE blsStatus = 'ATIVO' AND blsCodigo = ".$blsCodigo;
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Blog_model.php
	* @param 
	* @return 
	*/
	public function adicionarVisualizacao($bpoCodigo) {
		$sql = "UPDATE cub3_blog_posts SET bpoVisualizacoes = bpoVisualizacoes + 1 WHERE bpoCodigo = ".$bpoCodigo;
		return $this->db->query($sql);
	}





}
