<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cub3_usuario_model extends CI_Model {

	public function __construct(){
        parent::__construct();
	}
	public function usuarioPorEmail($usuEmail)
	{
		$sql = "SELECT * FROM cub3_usuario WHERE usuEmail = ?";
		return $this->db->query($sql,array($usuEmail));
	}
	public function usuarioPorCodigo($usuCodigo)
	{
		$sql = "SELECT * FROM cub3_usuario WHERE usuCodigo = ?";
		return $this->db->query($sql,array($usuCodigo));
	}
	public function usuariosAtivos()
	{
		$sql = "SELECT USU.usuCodigo, USU.usuNome, USU.usuSobrenome, USU.usuEmail, USU.usuImagem FROM cub3_usuario USU, cub3_grupo_has_usuario GHU WHERE USU.usuStatus = 'ATIVO' AND USU.usuCodigo = GHU.usuCodigo AND GHU.gruCodigo <> 1 ORDER BY usuCodigo ASC";
		return $this->db->query($sql,array());

	}
	public function usuarioValidarToken($token, $email)
	{
			$sql =  "SELECT * FROM reset_senha RES INNER JOIN cub3_usuario USU ON USU.usuEmail = '{$email}' WHERE RES.urtToken = '".$token."' AND RES.urtStatus = 'Ativo' AND USU.usuCodigo = RES.id LIMIT 0,1";
		return $this->db->query($sql);		
	}	
}

/* End of file Usuario_model.php */
/* Location: ./application/controllers/Usuario_model.php */