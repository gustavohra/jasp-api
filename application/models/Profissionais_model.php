<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 24/08/2016 às 11:19:21
	*
	* @author gustavoaguiar
	* @package None
	*/
class Profissionais_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* [Método]: profissionaisListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function listarProfissionais($listaOrganizarCampo = "proCodigo", $listaOrganizarOrdem = "ASC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM pro_profissional WHERE proStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM pro_profissional WHERE proStatus = 'ATIVO'  ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
	/*
	* [Método]: profissionaisListarSelect
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function listarProfissionaisSelect($listaOrganizarCampo = "proCodigo", $listaOrganizarOrdem = "ASC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT proCodigo as value, proNome as name FROM pro_profissional WHERE proStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT proCodigo as value, proNome as name FROM pro_profissional WHERE proStatus = 'ATIVO'  ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
		/*
	* [Método]: profissionalPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/models/Sintomas_model.php
	* @param 
	* @return 
	*/
	public function profissionalPorCodigo($proCodigo)
	{
			$sql =  "SELECT * FROM pro_profissional WHERE proCodigo = ".$proCodigo." LIMIT 0,1";
		return $this->db->query($sql);		
	}

		/*
	* [Método]: profissionalValidarLogin
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Applications/MAMP/htdocs/jasp/application/models/Sintomas_model.php
	* @param 
	* @return 
	*/
	public function profissionalValidarLogin($proEmail)
	{
		$sql =  "SELECT * FROM pro_profissional WHERE proStatus = 'ATIVO' AND proEmail = '".$proEmail."' LIMIT 0,1";
		return $this->db->query($sql);		
	}
		/*
	* [Método]: profissionalLocaisAtendimento
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalLocaisAtendimento($proCodigo)
	{
		$sql =  "SELECT (palCodigo) AS value, (palRotulo) AS name FROM pro_profissionalAtendimentoLocal WHERE proCodigo = ?";
		return $this->db->query($sql,array($proCodigo));	
	}
	public function profissionalLocaisAtendimentoCompleto($proCodigo)
	{
		$sql =  "SELECT * FROM pro_profissionalAtendimentoLocal WHERE proCodigo = ?";
		return $this->db->query($sql,array($proCodigo));	
	}
	/*
	* [Método]: profissionalDadosCompletos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalDadosCompletos($areCodigo = null, $areTipo = null)
	{
		if(!is_null($areCodigo)):
			$sql = "SELECT * FROM pro_profissional PRO, pro_profissional_has_pro_areaAtuacao PRA, pro_areaAtuacao ARE, pro_profissionalAtendimentoLocal PAL WHERE PRA.pro_areaAtuacao_areCodigo = '".$areCodigo."' AND PRA.pro_profissional_proCodigo = PRO.proCodigo AND ARE.areCodigo = PRA.pro_areaAtuacao_areCodigo AND PAL.proCodigo = PRO.proCodigo AND PRO.proStatus = 'ATIVO'";
		else:
			$sql = "SELECT * FROM pro_profissional PRO, pro_profissionalAtendimentoLocal PAL, pro_profissional_has_pro_areaAtuacao PRA, pro_areaAtuacao ARE WHERE PAL.proCodigo = PRO.proCodigo AND  PRA.pro_profissional_proCodigo	 = PRO.proCodigo AND ARE.areCodigo = PRA.pro_areaAtuacao_areCodigo AND PRO.proStatus = 'ATIVO' AND PAL.palTipo = '".$areTipo."'";
		endif;
		
		if(!is_null($areTipo))
			$sql .=  "AND PAL.palTipo = '".$areTipo."'";

		return $this->db->query($sql,array());
	}
	/*
	* [Método]: profissionalDadosPacotesCompletos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalDadosPacotesCompletos()
	{
		$sql = "SELECT 
					* 
				FROM 
					pro_profissional PRO, pro_areaAtuacao ARE, pro_profissionalAtendimentoLocal PAL, pro_pacote PAC 
				WHERE 
					ARE.areCodigo = PAC.areCodigo AND 
					PAL.proCodigo = PRO.proCodigo AND 
					PRO.proStatus = ? AND 
					PAC.pctStatus = ? AND 
					PAC.proCodigo = PRO.proCodigo AND 
					PAC.palCodigo = PAL.palCodigo";
		return $this->db->query($sql,array("ATIVO", "ATIVO"));
	}
	/*
	* [Método]: profissionalDadosServicosCompletos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalDadosServicosCompletos()
	{
		$sql = "SELECT * FROM pro_profissional PRO, pro_profissional_has_pro_areaAtuacao PRA, pro_areaAtuacao ARE, pro_profissionalAtendimentoLocal PAL, pro_servico SER WHERE PRA.pro_profissional_proCodigo = PRO.proCodigo AND ARE.areCodigo = PRA.pro_areaAtuacao_areCodigo AND PAL.proCodigo = PRO.proCodigo AND PRO.proStatus = ? AND SER.serStatus = ? AND SER.proCodigo = PRO.proCodigo AND SER.palCodigo = PAL.palCodigo";
		return $this->db->query($sql,array("ATIVO", "ATIVO"));
	}
	/*
	* [Método]: profissionalNota
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Pacientes_model.php
	* @param 
	* @return 
	*/
	public function profissionalNota($proCodigo)
	{
		$sql = "SELECT AVG(natNota) nota FROM pro_notaAtendimento WHERE natNota IS NOT NULL AND proCodigo = ?";
		return $this->db->query($sql,array($proCodigo));	
	}
	/*
	* [Método]: pacienteImagem
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Pacientes_model.php
	* @param 
	* @return 
	*/
	public function profissionalImagem()
	{
		$sql = "SELECT proImagem FROM pro_profissional";
		return $this->db->query($sql);
	}
	/*
	* [Método]: profissionalLocaisAtendimento
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalLocaisAtendimentoGeral($palCodigo)
	{
		$sql = "SELECT * FROM pro_profissionalAtendimentoLocal WHERE proCodigo = ?";
		return $this->db->query($sql,array($palCodigo));
	}
		/*
	* [Método]: profissionalAtendimentos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalAtendimentos($proCodigo)
	{
		$sql = "SELECT AGE.ageCodigo, AGE.palCodigo, ageHorarioInicial, ageHorarioFinal, ageStatus, AGE.pacCodigo, pacNome, pacImagem FROM pro_agenda AGE, pac_paciente PAC WHERE AGE.proCodigo = ? AND PAC.pacCodigo = AGE.pacCodigo";
		return $this->db->query($sql,array($proCodigo));
	}

	/*
	* [Método]: profissionalHistoricoPorAgenda
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalHistoricoPorAgenda($ageCodigo)
	{
		$sql = "SELECT * FROM pro_agenda_historico WHERE ageCodigo = ?";
		return $this->db->query($sql,array($ageCodigo));
	}

		/*
	* [Método]: profissionalEspecialidades 
	*/
	public function profissionalEspecialidades($proCodigo)
	{
		$sql = "SELECT ESP.espDescricao FROM pro_especialidade ESP, pro_profissional_has_proEspecialidade PPE WHERE PPE.proCodigo = ? AND PPE.espCodigo = ESP.espCodigo";
		return $this->db->query($sql,array($proCodigo));
	}


		/*
	* [Método]: profissionalAtendimentos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalVerificarDisponibilidade($proCodigo, $data)
	{  
		$sql = "SELECT ageHorarioInicial, ageHorarioFinal, ageStatus FROM pro_agenda WHERE proCodigo = ".$proCodigo." AND '".$data."' = DATE(ageHorarioInicial) AND ageStatus = 'LIBERADO'"; 
		return $this->db->query($sql);
	}
	/*
	* [Método]: profissionalPorEmail
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Projetos/jasp/application/models/Profissionais_model.php
	* @param 
	* @return 
	*/
	public function profissionalPorEmail($proEmail)
	{
		$sql = "SELECT * FROM pro_profissional WHERE proEmail = ?";
		return $this->db->query($sql,array($proEmail));
	}
	/*
	* [Método]: pacoteAssociacaoPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarAreasProProfissional($proCodigo)
	{
		$sql = "SELECT * FROM pro_profissional_has_pro_areaAtuacao PRA, pro_areaAtuacao ARE WHERE PRA.pro_profissional_proCodigo = ? AND ARE.areCodigo = PRA.pro_areaAtuacao_areCodigo";
		return $this->db->query($sql, array($proCodigo));
	}
	/*
	* [Método]: listarDadosBancariosPorProfissional
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarDadosBancariosPorProfissional($proCodigo)
	{
		$sql = "SELECT * FROM pro_profissional_dadosbanco WHERE proCodigo = ?";
		return $this->db->query($sql, array($proCodigo));
	}
	/*
	* [Método]: listarDispositivoPorProfissional
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarDispositivoPorProfissional($proCodigo)
	{
		$sql = "SELECT * FROM pro_profissional_dispositivo WHERE proCodigo = ?";
		return $this->db->query($sql, array($proCodigo));
	}
	/*
	* [Método]: listarProfissionaisPorStatus
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarProfissionaisPorStatus($proStatus)
	{
		$sql = "SELECT * FROM pro_profissional WHERE proStatus = ? ORDER BY proNome ASC";
		return $this->db->query($sql,array($proStatus));
	}
	/*
	* [Método]: listarProfissionaisComLocalAtendimento
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarProfissionaisComLocalAtendimento()
	{
		$sql = "SELECT * FROM pro_profissional WHERE proStatus = ? AND proCodigo IN (SELECT proCodigo FROM pro_profissionalAtendimentoLocal) ORDER BY proNome ASC";
		return $this->db->query($sql,array("ATIVO"));
	}
	/*
	* [Método]: listarDadosLocalAtendimento
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarDadosLocalAtendimento($palCodigo)
	{
		$sql = "SELECT * FROM pro_profissionalAtendimentoLocal WHERE palCodigo = ?";
		return $this->db->query($sql,array($palCodigo));
	}
	/*
	* [Método]: listarPacientesPorProfissional
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarPacientesPorProfissional($proCodigo)
	{
		$sql = "SELECT PAC.* FROM pro_agenda AGE, pac_paciente PAC WHERE AGE.proCodigo = ? AND PAC.pacCodigo = AGE.pacCodigo GROUP BY PAC.pacCodigo";
		return $this->db->query($sql,array($proCodigo));
	}
	/*
	* [Método]: verificarDocumentos
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function verificarDocumentos($dados)
	{
		$sql = "SELECT proCodigo FROM pro_profissional WHERE proCpf = ? OR proDocumentoIdentidade = ? OR proEmail = ?";
		return $this->db->query($sql,array($dados['proCpf'],$dados['proDocumentoIdentidade'],$dados['proEmail']));
	}
	/*
	* [Método]: profissionalAgenda
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function profissionalAgenda($proCodigo)
	{
		$sql = "SELECT * FROM pro_agenda AGE, pro_profissionalAtendimentoLocal PAL WHERE AGE.proCodigo = ? AND AGE.palCodigo = PAL.palCodigo";
		return $this->db->query($sql,array($proCodigo));
	}
 }
