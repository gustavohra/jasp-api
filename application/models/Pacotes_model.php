<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 20/12/2016 às 19:57:10
	*
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Pacotes_model.php
	*/
class Pacotes_model extends CI_Model {

		/*
	* [Método]: pacotesPorArea
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Servicos_model.php
	* @param 
	* @return 
	*/
	public function pacotesPorArea($areCodigo)
	{
		$sql = "SELECT * FROM pro_pacote PCT, pro_profissional PRO, pro_profissionalAtendimentoLocal PAL WHERE PCT.areCodigo = ? AND PCT.proCodigo = PRO.proCodigo AND PAL.palCodigo = PCT.palCodigo";
		return $this->db->query($sql, array($areCodigo));
	}
		/*
	* [Método]: pacotePorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Pacotes_model.php
	* @param 
	* @return 
	*/
	public function pacotePorCodigo($pctCodigo)
	{
		$sql = "SELECT * FROM pro_pacote WHERE pctCodigo = ?";
		return $this->db->query($sql, array($pctCodigo));
	}
		/*
	* [Método]: pacoteAssociacaoPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarPacotesPorProfissional($proCodigo)
	{
		$sql = "SELECT * FROM pro_pacote PPA, pro_profissional PRO, pro_profissionalAtendimentoLocal PAL WHERE PRO.proCodigo = ? AND PRO.proCodigo = PPA.proCodigo AND PPA.palCodigo = PAL.palCodigo";
		return $this->db->query($sql, array($proCodigo));
	}
	/*
	* [Método]: pacoteAssociacaoPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarPacotesPorCliente($pacCodigo)
	{
		$sql = "SELECT * FROM pac_paciente_pacote PAC, pro_pacote PPA, pro_profissional PRO, pro_profissionalAtendimentoLocal PAL WHERE PAC.pacCodigo = ? AND PAC.pctCodigo = PPA.pctCodigo AND PRO.proCodigo = PPA.proCodigo AND PPA.palCodigo = PAL.palCodigo";
		return $this->db->query($sql, array($pacCodigo));
	}
	public function listarUtilizacoesPorPacote($pacCodigo)
	{
		$sql = "SELECT * FROM pro_agenda AGE WHERE AGE.pacCodigo IS NOT NULL AND AGE.pacCOdigo = {$pacCodigo}";
		return $this->db->query($sql, array($pacCodigo));
	}


	public function listarUtilizacoesPorPacoteContratado($pacCodigo, $pctCodigo)
	{
		$sql = "SELECT * FROM pro_agenda AGE WHERE AGE.pacCodigo IS NOT NULL AND AGE.pacCOdigo = {$pacCodigo} AND AGE.pctCodigo = {$pctCodigo}";
		return $this->db->query($sql, array($pacCodigo));
	}
	/*
	* [Método]: listarPacotes
	* [Descrição]: 
	* [Comentários]: 
	*/
	public function listarPacotes()
	{
		$sql = "SELECT * FROM pro_pacote PPA, pro_profissional PRO, pro_profissionalAtendimentoLocal PAL WHERE PRO.proCodigo = PPA.proCodigo AND PPA.palCodigo = PAL.palCodigo ORDER BY PPA.pctCodigo DESC";
		return $this->db->query($sql);
	}

 } 
