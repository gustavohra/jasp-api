<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class padrao_model extends CI_Model {

	public function inserir($tabela, $dados)
	{
		$this->db->insert($tabela,$dados);
		return $this->db->insert_id();
	}
	public function excluir($tabela, $condicao)
	{
		foreach ($condicao as $key => $value) {
			$this->db->where($key,$value);
		}
		return $this->db->delete($tabela);
	}
	public function alterar($tabela, $dados, $condicao){
		foreach ($condicao as $key => $value) {
			$this->db->where($key,$value);
		}
		return $this->db->update($tabela,$dados);
	}
	public function buscar($sql){
		return $this->db->query($sql);
	}

}

/* End of file padrao_model.php */
/* Location: ./application/models/padrao_model.php */