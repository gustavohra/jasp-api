<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cub3_menu_model extends CI_Model {
	public function menuListarPorGrupo($gruCodigo){
		$sql = "SELECT * FROM cub3_grupo_has_menu GHM, cub3_menu MEN WHERE GHM.gruCodigo = ? AND MEN.menCodigo = GHM.menCodigo AND MEN.menStatus = 'Ativo'";
		return $this->db->query($sql,array($gruCodigo));
	}
	public function menuListarTodosAjax($limiteInicial,$limiteFinal,$ordenacaoCampo,$ordenacaoOrdem,$palavraChave){
		$palavraChave = (!empty($palavraChave))?" WHERE menDescricao LIKE '%".$palavraChave."%'":" ";
		$sql = "SELECT * FROM cub3_menu".$palavraChave."ORDER BY $ordenacaoCampo $ordenacaoOrdem LIMIT $limiteInicial,$limiteFinal";
		return $this->db->query($sql);
	}
	public function menuSubmenuListarTodosAjax($limiteInicial,$limiteFinal,$ordenacaoCampo,$ordenacaoOrdem,$palavraChave,$menCodigo){
		$palavraChave = (!empty($palavraChave))?" WHERE menCodigo = ".$menCodigo." AND mitDescricao LIKE '%".$palavraChave."%' ":" WHERE menCodigo = ".$menCodigo." ";
		$sql = "SELECT * FROM cub3_menuitem".$palavraChave."ORDER BY $ordenacaoCampo $ordenacaoOrdem LIMIT $limiteInicial,$limiteFinal";
		return $this->db->query($sql);
	}
	
	/*
	* [Método]: menuListarTodos
	* [Descrição]: Busca todos os menus cadastrados
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/models/Menu_model.php
	* @param 
	* @return 
	*/
	public function menuListarTodos(){
		$sql = "SELECT * FROM cub3_menu WHERE menStatus = 'Ativo' ORDER BY menDescricao ASC";
		return $this->db->query($sql);
	}
	/*
	* [Método]: icones
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/models/Menu_model.php
	* @param 
	* @return 
	*/
	public function icones()
	{
		$sql = "SELECT * FROM cub3_menu_icone ORDER BY icone ASC";
		return $this->db->query($sql);
	}
	/*
	* [Método]: menuSelecionarCodigo
	* [Descrição]: Seleciona dados de um menu por seu código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/models/Menu_model.php
	* @param 
	* @return 
	*/
	public function menuSelecionarCodigo($menCodigo){
		$sql = "SELECT * FROM cub3_menu WHERE menCodigo = ?";
		return $this->db->query($sql,array($menCodigo));
	}
	/*
	* [Método]: menuSubmenuSelecionarCodigo
	* [Descrição]: Seleciona um submenu por código
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/models/Menu_model.php
	* @param 
	* @return 
	*/
	public function menuSubmenuSelecionarCodigo($mitCodigo){
		$sql = "SELECT * FROM cub3_menuitem WHERE mitCodigo = ?";
		return $this->db->query($sql,array($mitCodigo));
	}
	/*
	* [Método]: menuSubmenuListarTodos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsd/application/models/Menu_model.php
	* @param 
	* @return 
	*/
	public function menuSubmenuListarTodos($menCodigo){
		$sql = "SELECT * FROM cub3_menuitem WHERE menCodigo = ? ORDER BY mitDescricao ASC";
		return $this->db->query($sql,array($menCodigo));
	}


}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */