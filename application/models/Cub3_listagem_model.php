<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 13/05/2016 às 19:24:57
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_listagem_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

		/*
	* [Método]: listagens
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function listagens($listaOrganizarCampo = "lisCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_listagem WHERE lisStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_listagem WHERE lisStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}

	/*
	* [Método]: listagemPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function listagemPorCodigo($lisCodigo) {
		$sql = "SELECT * FROM cub3_listagem WHERE lisStatus = 'ATIVO' AND lisCodigo = ".$lisCodigo."";
		return $this->db->query($sql);
	}
	/*
	* [Método]: listagemListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function listagemPorCodigoOuSlug($valor) {
		if(is_numeric($valor))
			$sql = "SELECT * FROM cub3_listagem WHERE lisStatus = 'ATIVO' AND lisCodigo = ".$valor." OR lisSlug = '".$valor."' LIMIT 0,1";
		else
			$sql = "SELECT * FROM cub3_listagem WHERE lisStatus = 'ATIVO' AND lisSlug = '".$valor."' LIMIT 0,1";

		return $this->db->query($sql);
	}
 } 
