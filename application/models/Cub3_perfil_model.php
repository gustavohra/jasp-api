<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cub3_perfil_model extends CI_Model {
	public function perfilListarTodos(){
		$sql = "SELECT USU.usuCodigo, USU.usuNome, USU.usuSobrenome, USU.usuEmail, USU.usuImagem FROM cub3_usuario USU, cub3_grupo_has_usuario GHU WHERE USU.usuStatus = 'ATIVO' AND USU.usuCodigo = GHU.usuCodigo AND GHU.gruCodigo <> 1 ORDER BY USU.usuNome ASC";
		return $this->db->query($sql);
	}
	public function perfilListarTodosAjax($limiteInicial,$limiteFinal,$ordenacaoCampo,$ordenacaoOrdem,$palavraChave){
		$palavraChave = (!empty($palavraChave))?" WHERE GHU.gruCodigo <> 1 AND GHU.usuCodigo = USU.usuCodigo AND USU.usuNome LIKE '%".$palavraChave."%'":" WHERE GHU.gruCodigo <> 1 AND GHU.usuCodigo = USU.usuCodigo ";
		$sql = "SELECT * FROM cub3_usuario USU, cub3_grupo_has_usuario GHU".$palavraChave."ORDER BY $ordenacaoCampo $ordenacaoOrdem LIMIT $limiteInicial,$limiteFinal";
		return $this->db->query($sql);
	}
	public function perfilSelecionarEmail($usuEmail){
		$sql = "SELECT * FROM cub3_usuario WHERE usuEmail = ?";
		return $this->db->query($sql,array($usuEmail));
	}	
	public function perfilSelecionarIdFacebook($usuFacebook){
		$sql = "SELECT * FROM cub3_usuario WHERE usuFacebook = ?";
		return $this->db->query($sql,array($usuCodigo));
	}		
	public function perfilSelecionarCodigo($usuCodigo){
		$sql = "SELECT * FROM cub3_usuario WHERE usuCodigo = ?";
		return $this->db->query($sql,array($usuCodigo));
	}
	public function perfilSelecionarToken($urtToken){
		$sql = "SELECT * FROM cub3_usuarioresetsenha WHERE urtToken = ?";
		return $this->db->query($sql,array($urtToken));
	}
	public function perfilSelecionarGruposAssociados($usuCodigo){
		$sql = "SELECT * FROM cub3_grupo_has_usuario GHU, cub3_grupo GRU, cub3_unidade UNI WHERE GHU.usuCodigo = ? AND UNI.uniCodigo = GRU.uniCodigo AND GHU.gruCodigo = GRU.gruCodigo";
		return $this->db->query($sql,array($usuCodigo));
	}

	public function perfilSelecionarPorLoginSvn($usuLoginSubversion){
		$sql = "SELECT * FROM cub3_usuario WHERE usuLoginSubversion = '".$usuLoginSubversion."' LIMIT 0,1";
		return $this->db->query($sql,array($usuLoginSubversion));
	}
	public function perfilSelecionarProdutos($usuCodigo){
		$sql = "SELECT * FROM crm_clientes_produto WHERE usuCodigo = ".$usuCodigo." AND ccpStatus = 'ATIVO'";
		return $this->db->query($sql,array($usuCodigo));
	}
}
 