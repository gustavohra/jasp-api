<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 10/08/2016 às 15:50:44
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_galeria_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function galeriasListar($listaOrganizarCampo = "galCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_galeria WHERE galStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_galeria WHERE galStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}

	public function galeriaPorCodigo($galCodigo) { 
		$sql = "SELECT * FROM cub3_galeria WHERE galStatus = 'ATIVO' AND galCodigo = ".$galCodigo." LIMIT 0,1";  
		return $this->db->query($sql);
	}
 } 
