<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 16/09/2016 às 19:29:48
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_videos_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: listarVideos 
	*/
	public function listarVideos($listaOrganizarCampo = "vidCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_video WHERE vidStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_video WHERE vidStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
 } 
