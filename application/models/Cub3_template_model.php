<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 13/05/2016 às 19:24:57
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_template_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

		/*
	* [Método]: templatesListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function templatesListar($listaOrganizarCampo = "tplCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_template WHERE tplStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_template WHERE tplStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}

	/*
	* [Método]: templateListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function templateListarPorCodigo($tplCodigo) {
		$sql = "SELECT * FROM cub3_template WHERE tplStatus = 'ATIVO' AND tplCodigo = ".$tplCodigo."";
		return $this->db->query($sql);
	}
	/*
	* [Método]: templateListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function templateListarPorCodigoOuSlug($valor) {
		if(is_numeric($valor))
			$sql = "SELECT * FROM cub3_template WHERE tplStatus = 'ATIVO' AND tplCodigo = ".$valor." OR tplSlug = '".$valor."'";
		else
			$sql = "SELECT * FROM cub3_template WHERE tplStatus = 'ATIVO' AND tplSlug = '".$valor."'";

		return $this->db->query($sql);
	}
 } 
