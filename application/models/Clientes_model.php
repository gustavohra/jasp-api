<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 04/10/2016 às 15:11:32
	*
	* @author gustavoaguiar
	* @usukage None
	*/
class Clientes_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function listarClientes($listaOrganizarCampo = "id", $listaOrganizarOrdem = "ASC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cliente WHERE status != 'EXCLUIDO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cliente WHERE status != 'EXCLUIDO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
	public function clientePorCodigo($id)
	{
			$sql =  "SELECT * FROM cliente WHERE status = 'ATIVO' AND id = '".$id."' LIMIT 0,1";
		return $this->db->query($sql);		
	}	

	public function clientePorEmail($email)
	{
			$sql =  "SELECT id, email, nome FROM cliente WHERE status = 'ATIVO' AND email = '".$email."' LIMIT 0,1";
		return $this->db->query($sql);		
	}	
	public function clienteValidarToken($token)
	{
			$sql =  "SELECT * FROM cliente_reset_senha WHERE urtToken = '".$token."' AND urtStatus = 'Ativo' LIMIT 0,1";
		return $this->db->query($sql);		
	}	
	public function clienteValidarLogin($email)
	{
			$sql =  "SELECT * FROM cliente WHERE status = 'ATIVO' AND email = '".$email."' LIMIT 0,1";
		return $this->db->query($sql);		
	}	
	public function clienteValidarLoginFacebook($cliente)
	{
			$sql =  "SELECT * FROM cliente WHERE status = 'ATIVO' AND usuIdFacebook = '".$cliente["usuIdFacebook"]."' OR email = '".$cliente["email"]."' LIMIT 0,1";
		return $this->db->query($sql);		
	}
		/*
	* [Método]: clienteNota
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function clienteNota($id)
	{
		$sql = "SELECT AVG(natNota) nota FROM usu_notaAtendimento WHERE id = ?";
		return $this->db->query($sql,array($id));	
	}
	/*
	* [Método]: clienteNotasPendentes
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function clienteNotasPendentes($id)
	{
		$sql = "SELECT * FROM pro_notaAtendimento WHERE natNota IS NULL AND id = ?";
		return $this->db->query($sql,array($id));
	}
	/*
	* [Método]: clienteImagem
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function clienteImagem()
	{
		$sql = "SELECT usuImagem FROM cliente";
		return $this->db->query($sql);
	}
	/*
	* [Método]: cuponsPorcliente
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function cuponsPorcliente($id)
	{
		$sql = "SELECT * FROM cliente_desconto WHERE id = ?";
		return $this->db->query($sql,array($id));
	}
	/*
	* [Método]: historicoUtilizacaousuotePorCliente
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function historicoUtilizacaousuotePorCliente($id,$pctCodigo)
	{
		$sql = "SELECT * FROM cliente_usuote WHERE papStatus = ? AND id = ? AND pctCodigo = ?";
		return $this->db->query($sql,array("ATIVO",$id, $pctCodigo));
	}
		/*
	* [Método]: usuotesPorcliente
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function usuotesPorcliente($id)
	{
		$sql = "SELECT PCT.*, PAP.*, PRO.proNome, PRO.proApelido, PRO.proImagem FROM pro_usuote PCT, cliente_usuote PAP, pro_profissional PRO  WHERE PAP.id = ? AND PAP.pctCodigo = PCT.pctCodigo AND PCT.proCodigo = PRO.proCodigo"; 
		return $this->db->query($sql,array($id));
	}
		/*
	* [Método]: servicosPorcliente
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function servicosPorcliente($id)
	{
		$sql = "select * from pro_profissional PRO, cliente_servico PAS WHERE PAS.id = ? AND PAS.proCodigo = PRO.proCodigo"; 
		return $this->db->query($sql,array($id));
	}
		/*
	* [Método]: clienteTransacoes
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function clienteTransacoes($id)
	{
		$sql = "SELECT * FROM log_transacao WHERE id = ? ORDER BY traHorario DESC"; 
		return $this->db->query($sql,array($id));
	}
		/*
	* [Método]: transacaoPorToken
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @usukage /Volumes/Arquivos/Dev/www/beok/application/models/Clientes_model.php
	* @param 
	* @return 
	*/
	public function transacaoPorToken($token)
	{
		$sql = "SELECT * FROM log_transacao WHERE traTokenTransacao = ?"; 
		return $this->db->query($sql,array($token));
	}
	/*
	* [Método]: isClienteAssinante
	* [Descrição]: 
	* [Comentários]: 
	* 
	*/
	public function isClienteAssinante($id,$palCodigo)
	{
		$sql = "SELECT * FROM cliente_servico WHERE id = ? AND palCodigo = ?"; 
		return $this->db->query($sql,array($id,$palCodigo));
	}
	
	/*
	* [Método]: getHistoricoTransacoesAgenda
	* [Descrição]: 
	* [Comentários]: 
	* 
	*/
	public function getHistoricoTransacoesAgenda($id)
	{
		$sql = "SELECT * FROM pro_agenda AGE, pro_profissional PRO, pro_profissionalAtendimentoLocal PAL WHERE AGE.id = ? AND AGE.proCodigo = PRO.proCodigo AND PAL.proCodigo = PRO.proCodigo LIMIT 1"; 
		return $this->db->query($sql,array($id));
	}
	/*
	* [Método]: isClienteAssinante
	* [Descrição]: 
	* [Comentários]: 
	* 
	*/
	public function getHistoricoTransacoesExperiencias($id)
	{
		$sql = "SELECT * FROM pro_experiencia_has_cliente PEP, pro_experiencia EXP, pro_profissional PRO WHERE PEP.cliente_id = ? AND EXP.id = PEP.pro_experiencia_id AND PRO.proCodigo = EXP.proCodigo"; 
		return $this->db->query($sql,array($id));
	}
 } 
