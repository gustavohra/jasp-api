<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_permissoes_model extends CI_Model {
	public function permissoesPorMenu($menCodigo,$gruCodigo){
		$sql = "SELECT gruCodigo FROM cub3_grupo_has_menu WHERE menCodigo = ? AND gruCodigo = ?";
		return $this->db->query($sql,array($menCodigo,$gruCodigo));
	}
	public function permissoesPorUsuario($usuCodigo){
		$sql = "SELECT * FROM cub3_grupo_has_menu GHM, cub3_menuitem MIT, cub3_menu MEN, cub3_grupo_has_usuario GHU WHERE GHU.usuCodigo = ? AND GHU.gruCodigo = GHM.gruCodigo AND MIT.menCodigo = MEN.menCodigo AND MIT.menCodigo = GHM.menCodigo";
		return $this->db->query($sql,array($usuCodigo));
	}
}

/* End of file permissoes_model.php */
/* Location: ./application/models/permissoes_model.php */