<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 26/05/2016 às 17:18:40
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_timeline_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
		/*
	* [Método]: timelineListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/bellolar/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function timelineListar($listaOrganizarCampo = "timCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT *, (SELECT usuImagem FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline.usuCodigo) AS usuImagem, (SELECT usuNome FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline.usuCodigo) AS usuNome FROM cub3_timeline ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT *, (SELECT usuImagem FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline.usuCodigo) AS usuImagem, (SELECT usuNome FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline.usuCodigo) AS usuNome FROM cub3_timeline ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
	public function timelineListarComentarios($listaOrganizarCampo = "timCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT *, (SELECT usuImagem FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline_comentario.usuCodigo) AS usuImagem, (SELECT usuNome FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline_comentario.usuCodigo) AS usuNome FROM cub3_timeline_comentario WHERE timCodigo = ".$listaOrganizarCampo." ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT *, (SELECT usuImagem FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline_comentario.usuCodigo) AS usuImagem, (SELECT usuNome FROM cub3_usuario WHERE cub3_usuario.usuCodigo = cub3_timeline_comentario.usuCodigo) AS usuNome FROM cub3_timeline_comentario WHERE timCodigo = ".$listaOrganizarCampo." ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
 } 
