<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cub3_unidade_model extends CI_Model {
	/*
	* [Método]: unidadeListarTodos
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Unidade_model.php
	* @param 
	* @return 
	*/
	public function unidadeListarTodos(){
		$sql = "SELECT * FROM cub3_unidade WHERE uniCodigo <> 1 ORDER BY uniDescricao ASC";
		return $this->db->query($sql);
	}
	/*
	* [Método]: unidadeSelecionarCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/previna/application/models/Unidade_model.php
	* @param 
	* @return 
	*/
	public function unidadeSelecionarCodigo($uniCodigo){
		$sql = "SELECT * FROM cub3_unidade WHERE uniCodigo = ?";
		return $this->db->query($sql,array($uniCodigo));
	}
}

/* End of file unidade_model.php */
/* Location: ./application/models/unidade_model.php */