<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 13/05/2016 às 19:24:57
	*
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_email_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

		/*
	* [Método]: emailsListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Cub3_formularios_model.php
	* @param 
	* @return 
	*/
	public function emailsListar($listaOrganizarCampo = "emtCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0)
	{
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_email WHERE emtStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_email WHERE emtStatus = 'ATIVO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}

	/*
	* [Método]: emailListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function emailListarPorCodigo($emtCodigo) {
		$sql = "SELECT * FROM cub3_email WHERE emtStatus = 'ATIVO' AND emtCodigo = ".$emtCodigo."";
		return $this->db->query($sql);
	}
	/*
	* [Método]: emailListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function emailListarPorCodigoOuSlug($valor) {
		if(is_numeric($valor))
			$sql = "SELECT * FROM cub3_email WHERE emtStatus = 'ATIVO' AND emtCodigo = ".$valor." OR emtSlug = '".$valor."'";
		else
			$sql = "SELECT * FROM cub3_email WHERE emtStatus = 'ATIVO' AND emtSlug = '".$valor."'";

		return $this->db->query($sql);
	}
 } 
