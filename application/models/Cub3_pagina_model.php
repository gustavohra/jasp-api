<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: Pagina_model
	* [Criação]: 29/04/2016 às 01:09:18
	*
		CREATE TABLE IF NOT EXISTS `cub3_pagina` (
	  	`pagCodigo` int(11) NOT NULL AUTO_INCREMENT,
	  	`pagTitulo` varchar(255) NOT NULL,
	  	`pagPagina` text,
	  	`pagHorario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  	`pagSlug` varchar(255) NOT NULL,
	  	`pagStatus` enum('ATIVO','EXCLUIDO') NOT NULL DEFAULT 'ATIVO',
	  	`usuCodigo` int(11) DEFAULT NULL,
	  	PRIMARY KEY (`pagCodigo`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;	
	* @author gustavoaguiar
	* @package None
	*/
class Cub3_pagina_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* [Método]: paginaListar
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function paginaListar($listaOrganizarCampo = "pagCodigo", $listaOrganizarOrdem = "DESC", $limitInicio = 0, $limitTermino = 0) {
		switch ($limitTermino) {
			case 0:
					$sql = "SELECT * FROM cub3_pagina WHERE pagStatus != 'EXCLUIDO' ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem;
				break;
			default:
					$sql =  "SELECT * FROM cub3_pagina WHERE pagStatus != 'EXCLUIDO'  ORDER BY ".$listaOrganizarCampo." ".$listaOrganizarOrdem." LIMIT ".$limitInicio.", ".$limitTermino;
				break;
		}

		return $this->db->query($sql);
	}
	/*
	* [Método]: paginaListarPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function paginaListarPorCodigo($pagCodigo) {
		$sql = "SELECT *, pagImagem AS anexo FROM cub3_pagina WHERE pagCodigo = ".$pagCodigo."";
		return $this->db->query($sql);
	}

	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavoaguiar
	* @package /Volumes/C/wamp/www/telmaadvincula/application/models/Pagina_model.php
	* @param 
	* @return 
	*/
	public function paginaListarPorSlug($pagSlug) {
		$sql = "SELECT *, pagImagem AS anexo FROM cub3_pagina WHERE pagStatus = 'ATIVO' AND pagSlug = '".$pagSlug."'";
		return $this->db->query($sql);
	}
 } 
