<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 28/12/2016 às 17:37:11
	*
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Financeiro_model.php
	*/
class Financeiro_model extends CI_Model {

		/*
	* [Método]: descontoPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gustavo.aguiar
	* @package /Volumes/Arquivos/Dev/www/jasp/application/models/Financeiro_model.php
	* @param 
	* @return 
	*/
	public function descontoPorCodigo($desCodigo)
	{
		$sql = "SELECT * FROM fin_desconto WHERE desCodigo = ?";
		return $this->db->query($sql,array($desCodigo));
	}
 } 
