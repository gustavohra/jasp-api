<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 06/05/2016 às 13:30:00
	*
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsdbh55/application/models/Regionais.php
	*/
class Cub3_agenda_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [Método]: agenda
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsdbh55/application/models/Regionais.php
	* @param 
	* @return 
	*/
	public function agenda()
	{
		$sql = "SELECT * FROM cam_agenda";
		return $this->db->query($sql);
	}
	/*
	* [Método]: agendaEventoPorCodigo
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/ouvidoriapsdbh55/application/models/Regionais_model.php
	* @param 
	* @return 
	*/
	public function agendaEventoPorCodigo($ageCodigo)
	{
		$sql = "SELECT * FROM cam_agenda WHERE ageCodigo = ?";
		return $this->db->query($sql,array($ageCodigo));
	}


 } 
