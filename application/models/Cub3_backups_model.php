<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/*
	* CUB3 / Classes
	*
	* [Descrição]: 
	* [Criação]: 22/04/2016 às 22:19:55
	*
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/models/Backup_model.php.php
	*/
class Cub3_backups_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/*
	* [Método]: 
	* [Descrição]: 
	* [Comentários]: 
	* 
	* @author gladsonroberto
	* @package /Users/gladsonroberto/Documents/www/carlaandrea/application/models/Backup_model.php.php
	* @param 
	* @return 
	*/
	public function backups()
	{
		$sql = "SELECT * FROM cub3_backup";
		return $this->db->query($sql);
	}


 } 
